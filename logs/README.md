First 2 build attempts failed. Made between 9:30 and 10:15 Sunday 16th march

1. Failed, didn't know what happened because didn't want to read
2. I added a test.txt file to desktop-overlay/usr/share to see if it made it through
- failed for same reason as 1 I thnk, but have to check later

## Tuesday 30th March 2021, ~20:50
3. Found the reason - galculator-gtk2 was listed as "extra gralculator-gtk2" and pacman took it as "extragalculator-gtk2" in Packages-Desktop.... but I forgot to edit it so this build failed too
- funnily enough, the issue of not being able to acces the internet for the mirrorlist persists, even thought the VM has internet and connected to it *DURING THE BUILD*

## Tuesday 30th March 2021, 21:56
4. Took a really long time, going to give it a few more cores next time and leave brave closed in bg if I can..
- build failed, it installed a LOT more stuff after galculator worked....
- I think the issue was it ran out of space: everything's being dumped in /var/lib/manjaro-tools/buildiso. The folder was over 6 Gb
- Wiped that folder, going to try again. I hope that the var folder was full from the last two builds and that I won't have to increase the virtual disk or add a new one.. That'll be a pain
- If 5 doesn't work, I'll try a minimal ISO instead of a full one. I've been doing full so I can get an idea of the upper limit of the final size.
- After going thru log:
	- There were many dependency cycles detected, and also a lot of programs that I didn't ask for seemed to be coming in?
	- rsync said it could not transfer some files (point to space issue?)
	- mhwd_image() failed (what's that??)

## Wednesday 31st March 2021, 09:58
5. Took a much shorter time with 6 cores :p but still failed with same error as 4(at least at the end, I haven't gone through the rest of it)
- I will now try to build a minimal image
- if that doesn't work, I'll make a new virtual disk, put it on a bigger drive, and then mount that to /var/lib/manjaro/iso-profiles

## Wednesday 31st March 2021, 11:43
6. Failed the minimal build
7. Slow internet, failed the build after increasing space

## Wednesday 31st March 2021, 12:23
8. Tried to only one part wiith -x bbuut same prob...  Time to make a new big vdisk
- also the -v option for verbose uutput to a file doesn't seem to work.

## Wednesday 31st March 2021, 13:37
- Mounted new vDisk going to try again
9. Build success, now to figure out how to get the iso file, or  flash it directly to a USB

## Wednesday 31st March 2021, 19:14
- Found the ISO file but lost it when trying to move it over....
10. Tried to build it again, got a segfault

## Wednesday 31st March 2021, 20:43
- The 10th logfile is missing?
11. OK no it might be the 11th one
- anyways this one failed too, after a bunch just died because of a lack of space
- not sure what killed this one, it said something to do with security?
- the security thing might be due to me using time to see how long it took


## Wednesday 31st March 2021, 22:07
- after more storage issues finnaly sorted all out
- or so I thought
12. This build went so far so fast
- I don't think it all got coppied to the log file....
- REEEEEE ok but at the end it said something about running out of space
- so I will find a way to expand linuxcommn and then make it a bigger vDisk
- now I sleep...

## Thursday 1st April 2021 07:53
13. Build failed, I think there is not enough space somewhere
- First vDisk has 2.5G available according to df, other has 9.5G available...
- It said that 12*** is not enough for the *image* and that it needs 13*** so maybe it's the first one
- Going to expand both and try again

## Thursday 1st April 2021 09:09
14. BUILD COMPLLEETOO
- now to see what the iso image is like
- iT WORKED AND SO DID THE SHARED FOLDER
- TIME TO ETCH
- ~0920-24 Ddone etching, going boot VM with it first

## Thursday 1st April 2021 12:22
- It boots, but lightdm says session failed
![Lighdm Session Fail](Pictures/IMG_20210401_095134_NR.jpg)
- It appears  that  there are missing driver config files
![MHWD output](Pictures/IMG_20210401_095225_NR.jpg)
- now to add the driver files to the desktop-overlay config section 
 
