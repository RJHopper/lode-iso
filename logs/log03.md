[manjaro@manjaro LODE-alpha]$ sudo buildiso -f -p LODE-alpha
[sudo] password for manjaro:
 --> Profile: [LODE-alpha]
==> Start building [LODE-alpha]
==> Cleaning up ...
  -> Deleting chroot [rootfs] (x86_64) ...
  -> Deleting isoroot [iso] ...
 --> Loading Packages: [Packages-Root] ...
==> Prepare [Base installation] (rootfs)
 --> mirror: https://manjaro.moson.eu/stable/$repo/$arch
==> Creating install root at /var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/rootfs
  -> Installing packages to /var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/rootfs
:: Synchronizing package databases...
 core                    164.9 KiB   217 KiB/s 00:01 [############################] 100%
 extra                                          2000.5 KiB   193 KiB/s 00:10 [############################################] 100%
 community                                         6.6 MiB   396 KiB/s 00:17 [############################################] 100%
 multilib                                        180.5 KiB   267 KiB/s 00:01 [############################################] 100%
resolving dependencies...
looking for conflicting packages...
warning: dependency cycle detected:
warning: bashrc-manjaro will be installed before its bash dependency
warning: dependency cycle detected:
warning: libglvnd will be installed before its mesa dependency

Packages (269) acl-2.3.1-1  archlinux-keyring-20210110-1  argon2-20190702-3  asciidoc-9.1.0-1  attr-2.5.1-1  audit-3.0.1-1
               bash-5.1.0-2  bashrc-manjaro-5.1.0-2  ca-certificates-20181109-4  ca-certificates-mozilla-3.62-1
               ca-certificates-utils-20181109-4  curl-7.75.0-1  db-5.3.28-5  ding-libs-0.6.1-3  docbook-xml-4.5-9
               docbook-xsl-1.79.2-7  efivar-37-4  expat-2.2.10-2  fuse-common-3.10.2-1  fuse2-2.9.9-4  gdbm-1.19-2
               gdk-pixbuf2-2.42.2-1  glib2-2.66.7-1  gmp-6.2.1-1  gnupg-2.2.27-1  gnutls-3.7.1-1  gpgme-1.15.1-1
               groff-1.22.4-4  gssproxy-0.8.4-1  hdparm-9.60-1  hwids-20201207-1  hwinfo-21.72-1  iana-etc-20210202-1
               icu-68.2-1  iw-5.9-1  json-c-0.15-1  kbd-2.4.0-2  keyutils-1.6.3-1  kmod-28-1  krb5-1.18.3-1  libaio-0.3.112-2
               libarchive-3.5.1-1  libassuan-2.5.4-2  libcap-2.49-1  libcap-ng-0.8.2-1  libcroco-0.6.13-2  libdrm-2.4.104-1
               libedit-20210216_3.1-1  libelf-0.183-3  libevent-2.1.12-1  libffi-3.3-4  libgcrypt-1.9.2-1  libglvnd-1.3.2-1
               libgpg-error-1.41-1  libidn2-2.3.0-1  libinih-52-2  libjpeg-turbo-2.0.6-1  libksba-1.4.0-2  libldap-2.4.57-1
               libmnl-1.0.4-3  libnetfilter_conntrack-1.0.8-1  libnfnetlink-1.0.1-4  libnftnl-1.1.9-1  libnghttp2-1.43.0-1
               libnl-3.5.0-2  libnotify-0.7.9-1  libnsl-1.3.0-1  libomxil-bellagio-0.9.3-3  libp11-kit-0.23.22-1
               libpcap-1.10.0-1  libpciaccess-0.16-2  libpipeline-1.5.3-1  libpng-1.6.37-3  libpsl-0.21.1-1  libsasl-2.1.27-3
               libseccomp-2.5.1-2  libsecret-0.20.4-1  libssh2-1.9.0-2  libtasn1-4.16.0-1  libtiff-4.2.0-1  libtirpc-1.3.1-1
               libunistring-0.9.10-3  libunwind-1.5.0-1  libusb-1.0.24-2  libx11-1.7.0-4  libx86emu-3.1-1  libxau-1.0.9-3
               libxcb-1.14-1  libxcrypt-4.4.18-1  libxdamage-1.1.5-3  libxdmcp-1.1.3-3  libxext-1.3.4-3  libxfixes-5.0.3-4
               libxml2-2.9.10-8  libxshmfence-1.3-2  libxslt-1.1.34-5  libxxf86vm-1.1.4-4  linux-api-headers-5.10.13-1
               llvm-libs-11.1.0-1  lm_sensors-1:3.6.0.r41.g31d1f125-1  lz4-1:1.9.3-1  lzo-2.10-3  manjaro-keyring-20201216-1
               mesa-20.3.4-3  mhwd-amdgpu-19.1.0-1  mhwd-ati-19.1.0-1  mhwd-nvidia-460.56-2  mhwd-nvidia-390xx-390.141-1
               mkinitcpio-29-1.0  mkinitcpio-busybox-1.32.1-2  mpfr-4.1.0-1  ncurses-6.2-1  nettle-3.7.1-1  nfsidmap-2.5.3-1
               npth-1.6-3  nspr-4.29-1  nss-3.62-1  openssl-1.1.1.j-1  p11-kit-0.23.22-1  pacman-mirrors-4.19.5-2  pam-1.5.1-1
               pambase-20200721.1-2  pcre-8.44-1  pcre2-10.36-1  perl-clone-0.45-2  perl-encode-locale-1.05-7
               perl-file-listing-6.14-1  perl-html-parser-3.76-1  perl-html-tagset-3.20-10  perl-http-cookies-6.10-1
               perl-http-daemon-6.06-2  perl-http-date-6.05-3  perl-http-message-6.29-1  perl-http-negotiate-6.01-8
               perl-io-html-1.004-1  perl-libwww-6.53-1  perl-lwp-mediatypes-6.02-8  perl-net-http-6.20-1  perl-timedate-2.33-2
               perl-try-tiny-0.30-5  perl-uri-5.09-1  perl-www-robotrules-6.02-8  perl-xml-parser-2.46-2
               perl-xml-writer-0.625-6  pinentry-1.1.0-5  popt-1.18-1  python-3.9.2-1  python-appdirs-1.4.4-3
               python-chardet-3.0.4-7  python-idna-2.10-3  python-npyscreen-4.10.5-5  python-ordered-set-4.0.2-3
               python-packaging-20.9-1  python-pyparsing-2.4.7-3  python-requests-2.25.1-1  python-setuptools-1:54.1.1-1
               python-six-1.15.0-3  python-urllib3-1.26.3-1  readline-8.1.0-2  rpcbind-1.2.5-3  run-parts-4.8.6.1-2
               shared-mime-info-2.0+57+gc1d1c70-1  sqlite-3.35.1-2  systemd-247.4-1  systemd-libs-247.4-1
               thin-provisioning-tools-0.9.0-1  tzdata-2021a-1  util-linux-libs-2.36.2-1  v86d-0.1.10-5.1
               vulkan-icd-loader-1.2.172-1  wayland-1.19.0-1  wireless-regdb-2020.11.20-1  xcb-proto-1.14.1-3
               xorgproto-2021.3-1  xxhash-0.8.0-1  xz-5.2.5-1  zlib-1:1.2.11-4  zstd-1.4.9-1  acpi-1.7-3  acpid-2.0.32-2
               amd-ucode-20210315.r1846.3568f96-1  b43-fwcutter-019-3  bash-completion-2.11-1  btrfs-progs-5.11-1
               bzip2-1.0.8-4  coreutils-8.32-1  cpupower-5.11-1  crda-4.14-3  cronie-1.5.5-2  cryptsetup-2.3.5-1
               dbus-x11-1.12.20-1  device-mapper-2.03.11-5  dhclient-4.4.2-2  dhcpcd-9.4.0-1  diffutils-3.7-3
               dmraid-1.0.0.rc16.3-13  dnsmasq-2.84-1  dosfstools-4.2-1  e2fsprogs-1.46.2-1  ecryptfs-utils-111-4
               efibootmgr-17-2  exfat-utils-1.3.0-2  f2fs-tools-1.14.0-1  file-5.39-1  filesystem-2021.01.19-1
               findutils-4.8.0-1  gawk-5.1.0-1  gcc-libs-10.2.0-6  gettext-0.21-1  glibc-2.33-4  grep-3.6-1  grub-2.04-22
               gzip-1.10-3  haveged-1.9.14-1  inetutils-1.9.4-8  intel-ucode-20210216-1  iproute2-5.11.0-1  iptables-1:1.8.7-1
               iputils-20200821-1  ipw2100-fw-1.3-10  ipw2200-fw-3.1-8  jfsutils-1.1.15-7  less-563-1  licenses-20200427-1
               linux-firmware-20210315.r1846.3568f96-1  linux54-5.4.105-1  logrotate-3.18.0-1  lsb-release-1.4-13
               lvm2-2.03.11-5  man-db-2.9.4-1  man-pages-5.10-1  manjaro-firmware-20160419-1  manjaro-release-21.0-1
               manjaro-system-20210321-2  mdadm-4.1-2  memtest86+-5.01-4  mhwd-0.6.5-2  mhwd-db-0.6.5-7
               mkinitcpio-openswap-0.1.0-3  nano-5.6.1-1  nfs-utils-2.5.3-1  ntfs-3g-2017.3.23-5  os-prober-1.78-0.1
               pacman-5.2.2-4  pciutils-3.7.0-1  perl-5.32.1-1  procps-ng-3.3.17-1  psmisc-23.4-1  reiserfsprogs-3.6.27-3
               rsync-3.2.3-3  s-nail-14.9.22-1  sed-4.8-1  shadow-4.8.1-4  spectre-meltdown-checker-0.43-1  sudo-1.9.6.p1-1
               sysfsutils-2.1.1-1  systemd-fsck-silent-239-1  systemd-sysvcompat-247.4-1  tar-1.34-1  texinfo-6.7-3
               tlp-1.3.1-2  usbutils-013-1  util-linux-2.36.2-1  vi-1:070224-4  wget-1.21.1-1  which-2.21-5
               wpa_supplicant-2:2.9-8  xfsprogs-5.10.0-2  zsh-5.8-1

Total Installed Size:  1836.52 MiB

:: Proceed with installation? [Y/n]
(269/269) checking keys in keyring                                           [############################################] 100%
(269/269) checking package integrity                                         [############################################] 100%
(269/269) loading package files                                              [############################################] 100%
(269/269) checking for file conflicts                                        [############################################] 100%
:: Processing package changes...
(  1/269) installing lsb-release                                             [############################################] 100%
call to execv failed (No such file or directory)
error: command failed to execute correctly
(  2/269) installing manjaro-release                                         [############################################] 100%
call to execv failed (No such file or directory)
error: command failed to execute correctly
(  3/269) installing linux-api-headers                                       [############################################] 100%
(  4/269) installing tzdata                                                  [############################################] 100%
(  5/269) installing iana-etc                                                [############################################] 100%
(  6/269) installing filesystem                                              [############################################] 100%
(  7/269) installing glibc                                                   [############################################] 100%
Optional dependencies for glibc
    gd: for memusagestat
(  8/269) installing gcc-libs                                                [############################################] 100%
(  9/269) installing acpi                                                    [############################################] 100%
( 10/269) installing ncurses                                                 [############################################] 100%
( 11/269) installing readline                                                [############################################] 100%
( 12/269) installing bashrc-manjaro                                          [############################################] 100%
( 13/269) installing bash                                                    [############################################] 100%
Optional dependencies for bash
    bash-completion: for tab completion [pending]
( 14/269) installing acpid                                                   [############################################] 100%
Optional dependencies for acpid
    perl: use perl based examples [pending]
( 15/269) installing amd-ucode                                               [############################################] 100%
( 16/269) installing b43-fwcutter                                            [############################################] 100%
( 17/269) installing bash-completion                                         [############################################] 100%
( 18/269) installing util-linux-libs                                         [############################################] 100%
( 19/269) installing lzo                                                     [############################################] 100%
( 20/269) installing zlib                                                    [############################################] 100%
( 21/269) installing xz                                                      [############################################] 100%
( 22/269) installing lz4                                                     [############################################] 100%
( 23/269) installing zstd                                                    [############################################] 100%
( 24/269) installing libgpg-error                                            [############################################] 100%
( 25/269) installing libgcrypt                                               [############################################] 100%
( 26/269) installing btrfs-progs                                             [############################################] 100%
Optional dependencies for btrfs-progs
    python: libbtrfsutil python bindings [pending]
    e2fsprogs: btrfs-convert [pending]
    reiserfsprogs: btrfs-convert [pending]
( 27/269) installing bzip2                                                   [############################################] 100%
( 28/269) installing attr                                                    [############################################] 100%
( 29/269) installing acl                                                     [############################################] 100%
( 30/269) installing gmp                                                     [############################################] 100%
( 31/269) installing e2fsprogs                                               [############################################] 100%
( 32/269) installing openssl                                                 [############################################] 100%
Optional dependencies for openssl
    ca-certificates [pending]
    perl [pending]
( 33/269) installing libsasl                                                 [############################################] 100%
( 34/269) installing libldap                                                 [############################################] 100%
( 35/269) installing keyutils                                                [############################################] 100%
( 36/269) installing krb5                                                    [############################################] 100%
( 37/269) installing libtirpc                                                [############################################] 100%
( 38/269) installing pambase                                                 [############################################] 100%
( 39/269) installing libcap-ng                                               [############################################] 100%
( 40/269) installing audit                                                   [############################################] 100%
( 41/269) installing libxcrypt                                               [############################################] 100%
( 42/269) installing pam                                                     [############################################] 100%
( 43/269) installing libcap                                                  [############################################] 100%
( 44/269) installing coreutils                                               [############################################] 100%
( 45/269) installing hwids                                                   [############################################] 100%
( 46/269) installing kmod                                                    [############################################] 100%
( 47/269) installing pciutils                                                [############################################] 100%
( 48/269) installing cpupower                                                [############################################] 100%
( 49/269) installing wireless-regdb                                          [############################################] 100%
( 50/269) installing libnl                                                   [############################################] 100%
( 51/269) installing systemd-libs                                            [############################################] 100%
( 52/269) installing device-mapper                                           [############################################] 100%
( 53/269) installing popt                                                    [############################################] 100%
( 54/269) installing json-c                                                  [############################################] 100%
( 55/269) installing argon2                                                  [############################################] 100%
( 56/269) installing cryptsetup                                              [############################################] 100%
( 57/269) installing expat                                                   [############################################] 100%
( 58/269) installing dbus-x11                                                [############################################] 100%
( 59/269) installing libmnl                                                  [############################################] 100%
( 60/269) installing libnftnl                                                [############################################] 100%
( 61/269) installing libpcap                                                 [############################################] 100%
( 62/269) installing libnfnetlink                                            [############################################] 100%
( 63/269) installing libnetfilter_conntrack                                  [############################################] 100%
( 64/269) installing iptables                                                [############################################] 100%
( 65/269) installing kbd                                                     [############################################] 100%
( 66/269) installing libunistring                                            [############################################] 100%
( 67/269) installing libidn2                                                 [############################################] 100%
( 68/269) installing findutils                                               [############################################] 100%
( 69/269) installing libtasn1                                                [############################################] 100%
( 70/269) installing libffi                                                  [############################################] 100%
( 71/269) installing libp11-kit                                              [############################################] 100%
( 72/269) installing p11-kit                                                 [############################################] 100%
( 73/269) installing ca-certificates-utils                                   [############################################] 100%
( 74/269) installing ca-certificates-mozilla                                 [############################################] 100%
( 75/269) installing ca-certificates                                         [############################################] 100%
( 76/269) installing libssh2                                                 [############################################] 100%
( 77/269) installing libpsl                                                  [############################################] 100%
( 78/269) installing libnghttp2                                              [############################################] 100%
( 79/269) installing curl                                                    [############################################] 100%
( 80/269) installing libelf                                                  [############################################] 100%
( 81/269) installing libseccomp                                              [############################################] 100%
( 82/269) installing shadow                                                  [############################################] 100%
( 83/269) installing file                                                    [############################################] 100%
( 84/269) installing util-linux                                              [############################################] 100%
Optional dependencies for util-linux
    python: python bindings to libmount [pending]
    words: default dictionary for look
( 85/269) installing pcre2                                                   [############################################] 100%
( 86/269) installing systemd                                                 [############################################] 100%
Initializing machine ID from random generator.
Creating group adm with gid 999.
Creating group wheel with gid 998.
Creating group kmem with gid 997.
Creating group tty with gid 5.
Creating group utmp with gid 996.
Creating group audio with gid 995.
Creating group disk with gid 994.
Creating group input with gid 993.
Creating group kvm with gid 992.
Creating group lp with gid 991.
Creating group optical with gid 990.
Creating group render with gid 989.
Creating group storage with gid 988.
Creating group uucp with gid 987.
Creating group video with gid 986.
Creating group users with gid 985.
Creating group sys with gid 3.
Creating group mem with gid 8.
Creating group ftp with gid 11.
Creating group mail with gid 12.
Creating group log with gid 19.
Creating group smmsp with gid 25.
Creating group proc with gid 26.
Creating group games with gid 50.
Creating group lock with gid 54.
Creating group network with gid 90.
Creating group floppy with gid 94.
Creating group scanner with gid 96.
Creating group power with gid 98.
Creating group systemd-journal with gid 984.
Creating group rfkill with gid 983.
Creating group nobody with gid 65534.
Creating user nobody (Nobody) with uid 65534 and gid 65534.
Creating group dbus with gid 81.
Creating user dbus (System Message Bus) with uid 81 and gid 81.
Creating group bin with gid 1.
Creating user bin (n/a) with uid 1 and gid 1.
Creating group daemon with gid 2.
Creating user daemon (n/a) with uid 2 and gid 2.
Creating user mail (n/a) with uid 8 and gid 12.
Creating user ftp (n/a) with uid 14 and gid 11.
Creating group http with gid 33.
Creating user http (n/a) with uid 33 and gid 33.
Creating group systemd-journal-remote with gid 982.
Creating user systemd-journal-remote (systemd Journal Remote) with uid 982 and gid 982.
Creating group systemd-network with gid 981.
Creating user systemd-network (systemd Network Management) with uid 981 and gid 981.
Creating group systemd-resolve with gid 980.
Creating user systemd-resolve (systemd Resolver) with uid 980 and gid 980.
Creating group systemd-timesync with gid 979.
Creating user systemd-timesync (systemd Time Synchronization) with uid 979 and gid 979.
Creating group systemd-coredump with gid 978.
Creating user systemd-coredump (systemd Core Dumper) with uid 978 and gid 978.
Creating group uuidd with gid 68.
Creating user uuidd (n/a) with uid 68 and gid 68.
Created symlink /etc/systemd/system/getty.target.wants/getty@tty1.service → /usr/lib/systemd/system/getty@.service.
Created symlink /etc/systemd/system/multi-user.target.wants/remote-fs.target → /usr/lib/systemd/system/remote-fs.target.
:: Append 'init=/usr/lib/systemd/systemd' to your kernel command line in your
   bootloader to replace sysvinit with systemd, or install systemd-sysvcompat
Optional dependencies for systemd
    libmicrohttpd: remote journald capabilities
    quota-tools: kernel-level quota management
    systemd-sysvcompat: symlink package to provide sysvinit binaries [pending]
    polkit: allow administration as unprivileged user
    curl: machinectl pull-tar and pull-raw [installed]
( 87/269) installing iw                                                      [############################################] 100%
( 88/269) installing crda                                                    [############################################] 100%
Uncomment the right regulatory domain in /etc/conf.d/wireless-regdom.
It will automatically be set on boot.
( 89/269) installing run-parts                                               [############################################] 100%
( 90/269) installing cronie                                                  [############################################] 100%
Optional dependencies for cronie
    smtp-server: send job output via email
    smtp-forwarder: forward job output to email server
( 91/269) installing iproute2                                                [############################################] 100%
Optional dependencies for iproute2
    db: userspace arp daemon [pending]
    libcap: tipc [installed]
    linux-atm: ATM support
( 92/269) installing dhclient                                                [############################################] 100%
( 93/269) installing dhcpcd                                                  [############################################] 100%
Optional dependencies for dhcpcd
    openresolv: resolvconf support
( 94/269) installing diffutils                                               [############################################] 100%
( 95/269) installing dmraid                                                  [############################################] 100%
( 96/269) installing nettle                                                  [############################################] 100%
( 97/269) installing dnsmasq                                                 [############################################] 100%
( 98/269) installing dosfstools                                              [############################################] 100%
( 99/269) installing nspr                                                    [############################################] 100%
(100/269) installing sqlite                                                  [############################################] 100%
(101/269) installing nss                                                     [############################################] 100%
(102/269) installing ecryptfs-utils                                          [############################################] 100%
Optional dependencies for ecryptfs-utils
    python2: for python module
(103/269) installing efivar                                                  [############################################] 100%
(104/269) installing efibootmgr                                              [############################################] 100%
(105/269) installing fuse-common                                             [############################################] 100%
(106/269) installing fuse2                                                   [############################################] 100%
(107/269) installing exfat-utils                                             [############################################] 100%
(108/269) installing f2fs-tools                                              [############################################] 100%
(109/269) installing mpfr                                                    [############################################] 100%
(110/269) installing gawk                                                    [############################################] 100%
(111/269) installing pcre                                                    [############################################] 100%
(112/269) installing glib2                                                   [############################################] 100%
Optional dependencies for glib2
    python: gdbus-codegen, glib-genmarshal, glib-mkenums, gtester-report [pending]
    libelf: gresource inspection tool [installed]
(113/269) installing icu                                                     [############################################] 100%
(114/269) installing libxml2                                                 [############################################] 100%
(115/269) installing libcroco                                                [############################################] 100%
(116/269) installing gettext                                                 [############################################] 100%
Optional dependencies for gettext
    git: for autopoint infrastructure updates
(117/269) installing grep                                                    [############################################] 100%
(118/269) installing grub                                                    [############################################] 100%
Generating grub.cfg.example config file...
This may fail on some machines running a custom kernel.
done.
Optional dependencies for grub
    freetype2: For grub-mkfont usage
    fuse2: For grub-mount usage [installed]
    dosfstools: For grub-mkrescue FAT FS and EFI support [installed]
    efibootmgr: For grub-install EFI support [installed]
    libisoburn: Provides xorriso for generating grub rescue iso using grub-mkrescue
    os-prober: To detect other OSes when generating grub.cfg in BIOS systems [pending]
    mtools: For grub-mkrescue FAT FS support
    libusb: For grub-emu USB support [pending]
    sdl: For grub-emu SDL support
(119/269) installing less                                                    [############################################] 100%
(120/269) installing gzip                                                    [############################################] 100%
(121/269) installing haveged                                                 [############################################] 100%
(122/269) installing inetutils                                               [############################################] 100%
(123/269) installing intel-ucode                                             [############################################] 100%
(124/269) installing iputils                                                 [############################################] 100%
(125/269) installing ipw2100-fw                                              [############################################] 100%
(126/269) installing ipw2200-fw                                              [############################################] 100%
(127/269) installing jfsutils                                                [############################################] 100%
(128/269) installing linux-firmware                                          [############################################] 100%
(129/269) installing mkinitcpio-busybox                                      [############################################] 100%
(130/269) installing libarchive                                              [############################################] 100%
(131/269) installing gdbm                                                    [############################################] 100%
(132/269) installing libnsl                                                  [############################################] 100%
(133/269) installing python                                                  [############################################] 100%
Optional dependencies for python
    python-setuptools [pending]
    python-pip
    sqlite [installed]
    mpdecimal: for decimal
    xz: for lzma [installed]
    tk: for tkinter
(134/269) installing libxslt                                                 [############################################] 100%
(135/269) installing docbook-xml                                             [############################################] 100%
(136/269) installing docbook-xsl                                             [############################################] 100%
(137/269) installing asciidoc                                                [############################################] 100%
Optional dependencies for asciidoc
    graphviz: graphviz-filter
    lilypond: music-filter
    imagemagick: music-filter
    source-highlight: source-highlight-filter
    dblatex: pdf generation
    fop: alternative pdf generation
    w3m: text generation
    lynx: alternative text generation
(138/269) installing mkinitcpio                                              [############################################] 100%
Optional dependencies for mkinitcpio
    xz: Use lzma or xz compression for the initramfs image [installed]
    bzip2: Use bzip2 compression for the initramfs image [installed]
    lzop: Use lzo compression for the initramfs image
    lz4: Use lz4 compression for the initramfs image [installed]
    mkinitcpio-nfs-utils: Support for root filesystem on NFS
(139/269) installing linux54                                                 [############################################] 100%
Optional dependencies for linux54
    crda: to set the correct wireless channels of your country [installed]
(140/269) installing licenses                                                [############################################] 100%
(141/269) installing logrotate                                               [############################################] 100%
(142/269) installing libaio                                                  [############################################] 100%
(143/269) installing thin-provisioning-tools                                 [############################################] 100%
(144/269) installing lvm2                                                    [############################################] 100%
(145/269) installing db                                                      [############################################] 100%
(146/269) installing perl                                                    [############################################] 100%
(147/269) installing groff                                                   [############################################] 100%
Optional dependencies for groff
    netpbm: for use together with man -H command interaction in browsers
    psutils: for use together with man -H command interaction in browsers
    libxaw: for gxditview
    perl-file-homedir: for use with glilypond
(148/269) installing libpipeline                                             [############################################] 100%
(149/269) installing man-db                                                  [############################################] 100%
Optional dependencies for man-db
    gzip [installed]
(150/269) installing man-pages                                               [############################################] 100%
(151/269) installing manjaro-firmware                                        [############################################] 100%
(152/269) installing npth                                                    [############################################] 100%
(153/269) installing libksba                                                 [############################################] 100%
(154/269) installing libassuan                                               [############################################] 100%
(155/269) installing libsecret                                               [############################################] 100%
Optional dependencies for libsecret
    org.freedesktop.secrets: secret storage backend
(156/269) installing pinentry                                                [############################################] 100%
Optional dependencies for pinentry
    gtk2: gtk2 backend
    qt5-base: qt backend
    gcr: gnome3 backend
(157/269) installing gnutls                                                  [############################################] 100%
Optional dependencies for gnutls
    guile: for use with Guile bindings
(158/269) installing gnupg                                                   [############################################] 100%
Optional dependencies for gnupg
    libldap: gpg2keys_ldap [installed]
    libusb-compat: scdaemon
    pcsclite: scdaemon
(159/269) installing gpgme                                                   [############################################] 100%
(160/269) installing archlinux-keyring                                       [############################################] 100%
(161/269) installing manjaro-keyring                                         [############################################] 100%
(162/269) installing python-npyscreen                                        [############################################] 100%
(163/269) installing python-urllib3                                          [############################################] 100%
Optional dependencies for python-urllib3
    python-pysocks: SOCKS support
    python-brotli: Brotli support
    python-pyopenssl: security support
(164/269) installing python-appdirs                                          [############################################] 100%
(165/269) installing python-pyparsing                                        [############################################] 100%
(166/269) installing python-six                                              [############################################] 100%
(167/269) installing python-packaging                                        [############################################] 100%
(168/269) installing python-ordered-set                                      [############################################] 100%
(169/269) installing python-setuptools                                       [############################################] 100%
(170/269) installing python-chardet                                          [############################################] 100%
(171/269) installing python-idna                                             [############################################] 100%
(172/269) installing python-requests                                         [############################################] 100%
Optional dependencies for python-requests
    python-pysocks: SOCKS proxy support
(173/269) installing pacman-mirrors                                          [############################################] 100%
Optional dependencies for pacman-mirrors
    gtk3: for interactive mode (GUI)
    python-gobject: for interactive mode (GUI)
(174/269) installing pacman                                                  [############################################] 100%
==> To import the data required by pacman for package verification run:
==> `pacman-key --init; pacman-key --populate archlinux manjaro`
==> See: https://www.archlinux.org/news/having-pacman-verify-packages
Optional dependencies for pacman
    haveged: for pacman-init.service [installed]
    perl-locale-gettext: translation support in makepkg-template
    findutils: for pacdiff --find [installed]
    mlocate: for pacdiff --locate
    sudo: privilege elevation for several scripts [pending]
    vim: default merge program for pacdiff
(175/269) installing sed                                                     [############################################] 100%
(176/269) installing manjaro-system                                          [############################################] 100%
(177/269) installing mdadm                                                   [############################################] 100%
(178/269) installing memtest86+                                              [############################################] 100%
(179/269) installing libx86emu                                               [############################################] 100%
(180/269) installing perl-encode-locale                                      [############################################] 100%
(181/269) installing perl-timedate                                           [############################################] 100%
(182/269) installing perl-http-date                                          [############################################] 100%
(183/269) installing perl-file-listing                                       [############################################] 100%
(184/269) installing perl-html-tagset                                        [############################################] 100%
(185/269) installing perl-clone                                              [############################################] 100%
(186/269) installing perl-io-html                                            [############################################] 100%
(187/269) installing perl-lwp-mediatypes                                     [############################################] 100%
(188/269) installing perl-uri                                                [############################################] 100%
(189/269) installing perl-http-message                                       [############################################] 100%
(190/269) installing perl-html-parser                                        [############################################] 100%
(191/269) installing perl-http-cookies                                       [############################################] 100%
(192/269) installing perl-http-daemon                                        [############################################] 100%
(193/269) installing perl-http-negotiate                                     [############################################] 100%
(194/269) installing perl-net-http                                           [############################################] 100%
(195/269) installing perl-try-tiny                                           [############################################] 100%
(196/269) installing perl-www-robotrules                                     [############################################] 100%
(197/269) installing perl-libwww                                             [############################################] 100%
Optional dependencies for perl-libwww
    perl-lwp-protocol-https: for https:// url schemes
(198/269) installing perl-xml-parser                                         [############################################] 100%
(199/269) installing perl-xml-writer                                         [############################################] 100%
(200/269) installing hwinfo                                                  [############################################] 100%
(201/269) installing libpciaccess                                            [############################################] 100%
(202/269) installing libdrm                                                  [############################################] 100%
(203/269) installing wayland                                                 [############################################] 100%
(204/269) installing xcb-proto                                               [############################################] 100%
(205/269) installing libxdmcp                                                [############################################] 100%
(206/269) installing libxau                                                  [############################################] 100%
(207/269) installing libxcb                                                  [############################################] 100%
(208/269) installing xorgproto                                               [############################################] 100%
(209/269) installing libx11                                                  [############################################] 100%
(210/269) installing libxext                                                 [############################################] 100%
(211/269) installing libxxf86vm                                              [############################################] 100%
(212/269) installing libxfixes                                               [############################################] 100%
(213/269) installing libxdamage                                              [############################################] 100%
(214/269) installing libxshmfence                                            [############################################] 100%
(215/269) installing libomxil-bellagio                                       [############################################] 100%
(216/269) installing libunwind                                               [############################################] 100%
(217/269) installing libedit                                                 [############################################] 100%
(218/269) installing llvm-libs                                               [############################################] 100%
(219/269) installing lm_sensors                                              [############################################] 100%
Optional dependencies for lm_sensors
    rrdtool: for logging with sensord
    perl: for sensor detection and configuration convert [installed]
(220/269) installing libglvnd                                                [############################################] 100%
(221/269) installing vulkan-icd-loader                                       [############################################] 100%
Optional dependencies for vulkan-icd-loader
    vulkan-driver: packaged vulkan driver
(222/269) installing mesa                                                    [############################################] 100%
Optional dependencies for mesa
    opengl-man-pages: for the OpenGL API man pages
    mesa-vdpau: for accelerated video playback
    libva-mesa-driver: for accelerated video playback
(223/269) installing libpng                                                  [############################################] 100%
(224/269) installing libjpeg-turbo                                           [############################################] 100%
Optional dependencies for libjpeg-turbo
    java-runtime>11: for TurboJPEG Java wrapper
(225/269) installing libtiff                                                 [############################################] 100%
Optional dependencies for libtiff
    freeglut: for using tiffgt
(226/269) installing shared-mime-info                                        [############################################] 100%
(227/269) installing gdk-pixbuf2                                             [############################################] 100%
(228/269) installing libnotify                                               [############################################] 100%
(229/269) installing mhwd-nvidia-390xx                                       [############################################] 100%
(230/269) installing mhwd-amdgpu                                             [############################################] 100%
(231/269) installing mhwd-ati                                                [############################################] 100%
(232/269) installing mhwd-nvidia                                             [############################################] 100%
(233/269) installing mhwd-db                                                 [############################################] 100%
(234/269) installing v86d                                                    [############################################] 100%
(235/269) installing mhwd                                                    [############################################] 100%
Optional dependencies for mhwd
    lib32-mesa: for 32bit libgl support
(236/269) installing mkinitcpio-openswap                                     [############################################] 100%

Alter /etc/openswap.conf file for your swap device name, keyfiles, etc...

For more information see: https://wiki.archlinux.org/index.php/Dm-crypt/Swap_encryption#mkinitcpio_hook

Don't forget to add the openswap hook after encrypt and before resume in your /etc/mkinitcpio.conf and run mkinitcpio -p linux...

(237/269) installing nano                                                    [############################################] 100%
(238/269) installing rpcbind                                                 [############################################] 100%
(239/269) installing nfsidmap                                                [############################################] 100%
(240/269) installing ding-libs                                               [############################################] 100%
(241/269) installing gssproxy                                                [############################################] 100%
(242/269) installing libevent                                                [############################################] 100%
Optional dependencies for libevent
    python: to use event_rpcgen.py [installed]
(243/269) installing nfs-utils                                               [############################################] 100%
Optional dependencies for nfs-utils
    sqlite: for nfsdcltrack usage [installed]
    python: for nfsiostat, nfsdclnts and mountstats usage [installed]
(244/269) installing ntfs-3g                                                 [############################################] 100%
(245/269) installing os-prober                                               [############################################] 100%
(246/269) installing procps-ng                                               [############################################] 100%
(247/269) installing psmisc                                                  [############################################] 100%
(248/269) installing reiserfsprogs                                           [############################################] 100%
(249/269) installing xxhash                                                  [############################################] 100%
(250/269) installing rsync                                                   [############################################] 100%
(251/269) installing s-nail                                                  [############################################] 100%
Optional dependencies for s-nail
    smtp-forwarder: for sending mail
(252/269) installing spectre-meltdown-checker                                [############################################] 100%
(253/269) installing sudo                                                    [############################################] 100%
(254/269) installing sysfsutils                                              [############################################] 100%
(255/269) installing systemd-fsck-silent                                     [############################################] 100%
(256/269) installing systemd-sysvcompat                                      [############################################] 100%
(257/269) installing tar                                                     [############################################] 100%
(258/269) installing texinfo                                                 [############################################] 100%
(259/269) installing hdparm                                                  [############################################] 100%
Optional dependencies for hdparm
    bash: for wiper.sh script [installed]
(260/269) installing libusb                                                  [############################################] 100%
(261/269) installing usbutils                                                [############################################] 100%
Optional dependencies for usbutils
    python: for lsusb.py usage [installed]
    coreutils: for lsusb.py usage [installed]
(262/269) installing tlp                                                     [############################################] 100%
Optional dependencies for tlp
    acpi_call: ThinkPad battery functions, Sandy Bridge and newer
    bash-completion: Bash completion [installed]
    ethtool: Disable Wake On Lan
    lsb-release: Display LSB release version in tlp-stat [installed]
    smartmontools: Display S.M.A.R.T. data in tlp-stat
    tp_smapi: ThinkPad battery functions
    x86_energy_perf_policy: Set energy versus performance policy on x86 processors
(263/269) installing vi                                                      [############################################] 100%
Optional dependencies for vi
    s-nail: used by the preserve command for notification [installed]
(264/269) installing wget                                                    [############################################] 100%
Optional dependencies for wget
    ca-certificates: HTTPS downloads [installed]
(265/269) installing which                                                   [############################################] 100%
(266/269) installing wpa_supplicant                                          [############################################] 100%
(267/269) installing libinih                                                 [############################################] 100%
(268/269) installing xfsprogs                                                [############################################] 100%
Optional dependencies for xfsprogs
    python: for xfs_scrub_all script [installed]
    smtp-forwarder: for xfs_scrub_fail script
(269/269) installing zsh                                                     [############################################] 100%
:: Running post-transaction hooks...
( 1/18) Creating system user accounts...
Creating group dhcpcd with gid 977.
Creating user dhcpcd (dhcpcd privilege separation) with uid 977 and gid 977.
Creating group dnsmasq with gid 976.
Creating user dnsmasq (dnsmasq daemon) with uid 976 and gid 976.
Creating group rpc with gid 32.
Creating user rpc (Rpcbind Daemon) with uid 32 and gid 32.
( 2/18) Updating journal message catalog...
( 3/18) Reloading system manager configuration...
  Skipped: Current root is not booted.
( 4/18) Updating udev hardware database...
( 5/18) Applying kernel sysctl settings...
  Skipped: Current root is not booted.
( 6/18) Creating temporary files...
( 7/18) Reloading device manager configuration...
  Skipped: Device manager is not running.
( 8/18) Arming ConditionNeedsUpdate...
( 9/18) Rebuilding certificate stores...
(10/18) Updating module dependencies...
(11/18) Updating linux initcpios...
==> Building image from preset: /etc/mkinitcpio.d/linux54.preset: 'default'
  -> -k /boot/vmlinuz-5.4-x86_64 -c /etc/mkinitcpio.conf -g /boot/initramfs-5.4-x86_64.img
==> Starting build: 5.4.105-1-MANJARO
  -> Running build hook: [base]
  -> Running build hook: [udev]
  -> Running build hook: [autodetect]
  -> Running build hook: [modconf]
  -> Running build hook: [block]
  -> Running build hook: [filesystems]
  -> Running build hook: [keyboard]
  -> Running build hook: [fsck]
==> Generating module dependencies
==> Creating gzip-compressed initcpio image: /boot/initramfs-5.4-x86_64.img
bsdtar: bsdtar: Failed to set default localeFailed to set default locale

==> Image generation successful
==> Building image from preset: /etc/mkinitcpio.d/linux54.preset: 'fallback'
  -> -k /boot/vmlinuz-5.4-x86_64 -c /etc/mkinitcpio.conf -g /boot/initramfs-5.4-x86_64-fallback.img -S autodetect
==> Starting build: 5.4.105-1-MANJARO
  -> Running build hook: [base]
  -> Running build hook: [udev]
  -> Running build hook: [modconf]
  -> Running build hook: [block]
  -> Running build hook: [filesystems]
  -> Running build hook: [keyboard]
  -> Running build hook: [fsck]
==> Generating module dependencies
==> Creating gzip-compressed initcpio image: /boot/initramfs-5.4-x86_64-fallback.img
bsdtar: bsdtar: Failed to set default locale
Failed to set default locale
==> Image generation successful
(12/18) Updating Grub-Bootmenu
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-5.4-x86_64
Found initrd image: /boot/intel-ucode.img /boot/amd-ucode.img /boot/initramfs-5.4-x86_64.img
Found initrd fallback image: /boot/initramfs-5.4-x86_64-fallback.img
Warning: os-prober will not be executed to detect other bootable partitions.
Systems on them will not be added to the GRUB boot configuration.
Check GRUB_DISABLE_OS_PROBER documentation entry.
Found memtest86+ image: /boot/memtest86+/memtest.bin
done
(13/18) Reloading system bus configuration...
  Skipped: Current root is not booted.
(14/18) Warn about old perl modules
perl: warning: Setting locale failed.
perl: warning: Please check that your locale settings:
	LANGUAGE = (unset),
	LC_ALL = (unset),
	LC_ADDRESS = "en_IN",
	LC_MONETARY = "en_IN",
	LC_PAPER = "en_IN",
	LC_MESSAGES = "C",
	LC_MEASUREMENT = "en_IN",
	LC_TIME = "en_IN",
	LANG = "C"
    are supported and installed on your system.
perl: warning: Falling back to the standard locale ("C").
(15/18) Probing GDK-Pixbuf loader modules...
(16/18) Configuring pacman-mirrors ...
::WARNING https://gitlab.manjaro.org '<urlopen error [Errno -2] Name or service not known>'
::WARNING https://wikipedia.org '<urlopen error [Errno -2] Name or service not known>'
::WARNING https://bitbucket.org '<urlopen error [Errno -2] Name or service not known>'
::INFO Internet connection appears to be down
::INFO Mirror ranking is not available
::INFO Mirror list is generated using random method
::INFO Writing mirror list
::Brazil          : https://manjaro.c3sl.ufpr.br/stable
::Ecuador         : https://mirror.cedia.org.ec/manjaro/stable
::Australia       : http://mirror.ventraip.net.au/Manjaro/stable
::Romania         : http://mirrors.serverhost.ro/manjaro/packages/stable
::Bulgaria        : http://manjaro.telecoms.bg/stable
::France          : http://kibo.remi.lu/stable
::Russia          : http://mirror.truenetwork.ru/manjaro/stable
::Poland          : http://mirror.chmuri.net/manjaro/stable
::Italy           : https://ct.mirror.garr.it/mirrors/manjaro/stable
::Philippines     : http://mirror.rise.ph/manjaro/stable
::Austria         : http://mirror.inode.at/manjaro/stable
::Hong_Kong       : http://ftp.cuhk.edu.hk/pub/Linux/manjaro/stable
::United_Kingdom  : http://manjaro.mirrors.uk2.net/stable
::Italy           : https://ba.mirror.garr.it/mirrors/manjaro/stable
::Turkey          : http://ftp.linux.org.tr/manjaro/stable
::Iran            : https://repo.sadjad.ac.ir/manjaro/stable
::China           : https://mirrors.zju.edu.cn/manjaro/stable
::Ecuador         : https://mirror.espoch.edu.ec/manjaro/stable
::Brazil          : http://linorg.usp.br/manjaro/stable
::Greece          : https://ftp.cc.uoc.gr/mirrors/linux/manjaro/stable
::Germany         : https://ftp.halifax.rwth-aachen.de/manjaro/stable
::Denmark         : https://mirrors.dotsrc.org/manjaro/stable
::China           : https://mirrors.shuosc.org/manjaro/stable
::Germany         : http://ftp.rz.tu-bs.de/pub/mirror/manjaro.org/repos/stable
::Germany         : http://ftp.tu-chemnitz.de/pub/linux/manjaro/stable
::Sweden          : https://ftp.lysator.liu.se/pub/manjaro/stable
::Bulgaria        : https://mirrors.netix.net/manjaro/stable
::Hungary         : http://mirror.infotronik.hu/mirrors/pub/manjaro/stable
::Spain           : http://ftp.caliu.cat/manjaro/stable
::Netherlands     : https://ftp.nluug.nl/pub/os/Linux/distr/manjaro/stable
::Portugal        : http://manjaro.barata.pt/stable
::Belgium         : https://manjaro.cu.be/stable
::China           : http://mirrors.tuna.tsinghua.edu.cn/manjaro/stable
::Australia       : http://manjaro.melbourneitmirror.net/stable
::United_Kingdom  : http://repo.manjaro.org.uk/stable
::Singapore       : https://download.nus.edu.sg/mirror/manjaro/stable
::United_States   : https://mirror.math.princeton.edu/pub/manjaro/stable
::United_Kingdom  : https://www.mirrorservice.org/sites/repo.manjaro.org/repos/sta
::United_States   : http://distro.ibiblio.org/manjaro/stable
::Japan           : http://ftp.riken.jp/Linux/manjaro/stable
::South_Africa    : http://manjaro.mirror.ac.za/stable
::Sweden          : https://mirror.zetup.net/manjaro/stable
::Germany         : http://mirror.ragenetwork.de/manjaro/stable
::Germany         : https://mirror.netcologne.de/manjaro/stable
::United_States   : https://mirrors.ocf.berkeley.edu/manjaro/stable
::China           : https://mirrors.shu.edu.cn/manjaro/stable
::Taiwan          : http://free.nchc.org.tw/manjaro/stable
::Indonesia       : http://kambing.ui.ac.id/manjaro/stable
::Costa_Rica      : https://mirrors.ucr.ac.cr/manjaro/stable
::Netherlands     : http://ftp.snt.utwente.nl/pub/linux/manjaro/stable
::Colombia        : http://mirror.upb.edu.co/manjarostable
::United_States   : https://mirror.clarkson.edu/manjaro/stable
::Bulgaria        : https://manjaro.ipacct.com/manjaro/stable
::Bangladesh      : http://mirror.xeonbd.com/manjaro/stable
::Canada          : https://osmirror.org/manjaro/stable
::Russia          : https://mirror.yandex.ru/mirrors/manjaro/stable
::France          : http://ftp.free.org/mirrors/repo.manjaro.org/repos/stable
::Brazil          : http://pet.inf.ufsc.br/mirrors/manjarolinux/stable
::Germany         : https://repo.rhindon.net/manjaro/stable
::Netherlands     : https://mirror.koddos.net/manjaro/stable
::Denmark         : https://www.uex.dk/public/manjaro/stable
::Germany         : https://manjaro.moson.eu/stable
::United_Kingdom  : http://mirror.catn.com/pub/manjaro/stable
::Germany         : https://mirror.philpot.de/manjaro/stable
::Belarus         : http://mirror.datacenter.by/pub/mirrors/manjaro/stable
::Japan           : http://ftp.tsukuba.wide.ad.jp/Linux/manjaro/stable
::Chile           : http://manjaro.dcc.uchile.cl/stable
::South_Africa    : http://mirror.is.co.za/mirrors/manjaro.org/stable
::China           : https://mirrors.ustc.edu.cn/manjaro/stable
::Czech           : https://mirror.dkm.cz/manjaro/stable
::Indonesia       : http://kartolo.sby.datautama.net.id/manjaro/stable
::China           : https://mirrors.sjtug.sjtu.edu.cn/manjarostable
::United_States   : http://mirror.dacentec.com/manjaro/stable
::Poland          : https://mirror.tuchola-dc.pl/manjaro/stable
::Italy           : https://manjaro.mirror.garr.it/mirrors/manjaro/stable
::Belgium         : http://ftp.belnet.be/mirrors/manjaro/stable
::Brazil          : http://mirror.ufam.edu.br/manjaro/stable
::Germany         : https://mirror.netzspielplatz.de/manjaro/packages/stable
::INFO Mirror list generated and saved to: /etc/pacman.d/mirrorlist
hint: use `pacman-mirrors` to generate and update your pacman mirrorlist.
(17/18) Updating the info directory file...
(18/18) Updating the MIME type database...
 --> Using build locales ...
 --> Setting mirrorlist branch: stable
Generating locales...
  en_US.UTF-8... done
Generation complete.
 --> Restoring [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/rootfs/etc/pacman.conf] ...
  -> Configuring lsb-release
  -> Cleaning [rootfs]
==> Done [Base installation] (rootfs)
 --> Loading Packages: [Packages-Desktop] ...
==> Prepare [Desktop installation] (desktopfs)
 --> overlayfs mount: [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/desktopfs]
 --> mirror: https://manjaro.moson.eu/stable/$repo/$arch
==> Creating install root at /var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/desktopfs
  -> Installing packages to /var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/desktopfs
:: Synchronizing package databases...
 core                                            164.9 KiB   434 KiB/s 00:00 [############################################] 100%
 extra                                          2000.5 KiB  1493 KiB/s 00:01 [############################################] 100%
 community                                         6.6 MiB  1156 KiB/s 00:06 [############################################] 100%
 multilib                                        180.5 KiB  1062 KiB/s 00:00 [############################################] 100%
error: target not found: extragalculator-gtk2
==> ERROR: Failed to install packages to new root
==> ERROR: Failed to install all packages
==> ERROR: A failure occurred in make_image_desktop().
    Aborting...
 --> overlayfs umount: [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/desktopfs]
