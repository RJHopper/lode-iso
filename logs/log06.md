[manjaro@manjaro LODE-alpha]$  sudo buildiso -p LODE-alpha
 --> Profile: [LODE-alpha]
==> Start building [LODE-alpha]
==> Cleaning up ...
  -> Deleting isoroot [iso] ...
 --> Loading Packages: [Packages-Root] ...
==> Prepare [Base installation] (rootfs)
 --> mirror: https://manjaro.moson.eu/stable/$repo/$arch
==> Creating install root at /var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/rootfs
  -> Installing packages to /var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/rootfs
:: Synchronizing package databases...
 core                           164.9 KiB   237 KiB/s 00:01 [################################] 100%
 extra                         2000.5 KiB   281 KiB/s 00:07 [################################] 100%
 community                        6.6 MiB   414 KiB/s 00:16 [################################] 100%
 multilib                       180.5 KiB   311 KiB/s 00:01 [################################] 100%
resolving dependencies...
looking for conflicting packages...
warning: dependency cycle detected:
warning: bashrc-manjaro will be installed before its bash dependency
warning: dependency cycle detected:
warning: libglvnd will be installed before its mesa dependency

Packages (269) acl-2.3.1-1  archlinux-keyring-20210110-1  argon2-20190702-3  asciidoc-9.1.0-1
               attr-2.5.1-1  audit-3.0.1-1  bash-5.1.0-2  bashrc-manjaro-5.1.0-2
               ca-certificates-20181109-4  ca-certificates-mozilla-3.62-1
               ca-certificates-utils-20181109-4  curl-7.75.0-1  db-5.3.28-5  ding-libs-0.6.1-3
               docbook-xml-4.5-9  docbook-xsl-1.79.2-7  efivar-37-4  expat-2.2.10-2
               fuse-common-3.10.2-1  fuse2-2.9.9-4  gdbm-1.19-2  gdk-pixbuf2-2.42.2-1
               glib2-2.66.7-1  gmp-6.2.1-1  gnupg-2.2.27-1  gnutls-3.7.1-1  gpgme-1.15.1-1
               groff-1.22.4-4  gssproxy-0.8.4-1  hdparm-9.60-1  hwids-20201207-1  hwinfo-21.72-1
               iana-etc-20210202-1  icu-68.2-1  iw-5.9-1  json-c-0.15-1  kbd-2.4.0-2
               keyutils-1.6.3-1  kmod-28-1  krb5-1.18.3-1  libaio-0.3.112-2  libarchive-3.5.1-1
               libassuan-2.5.4-2  libcap-2.49-1  libcap-ng-0.8.2-1  libcroco-0.6.13-2
               libdrm-2.4.104-1  libedit-20210216_3.1-1  libelf-0.183-3  libevent-2.1.12-1
               libffi-3.3-4  libgcrypt-1.9.2-1  libglvnd-1.3.2-1  libgpg-error-1.41-1
               libidn2-2.3.0-1  libinih-52-2  libjpeg-turbo-2.0.6-1  libksba-1.4.0-2
               libldap-2.4.57-1  libmnl-1.0.4-3  libnetfilter_conntrack-1.0.8-1
               libnfnetlink-1.0.1-4  libnftnl-1.1.9-1  libnghttp2-1.43.0-1  libnl-3.5.0-2
               libnotify-0.7.9-1  libnsl-1.3.0-1  libomxil-bellagio-0.9.3-3  libp11-kit-0.23.22-1
               libpcap-1.10.0-1  libpciaccess-0.16-2  libpipeline-1.5.3-1  libpng-1.6.37-3
               libpsl-0.21.1-1  libsasl-2.1.27-3  libseccomp-2.5.1-2  libsecret-0.20.4-1
               libssh2-1.9.0-2  libtasn1-4.16.0-1  libtiff-4.2.0-1  libtirpc-1.3.1-1
               libunistring-0.9.10-3  libunwind-1.5.0-1  libusb-1.0.24-2  libx11-1.7.0-4
               libx86emu-3.1-1  libxau-1.0.9-3  libxcb-1.14-1  libxcrypt-4.4.18-1
               libxdamage-1.1.5-3  libxdmcp-1.1.3-3  libxext-1.3.4-3  libxfixes-5.0.3-4
               libxml2-2.9.10-8  libxshmfence-1.3-2  libxslt-1.1.34-5  libxxf86vm-1.1.4-4
               linux-api-headers-5.10.13-1  llvm-libs-11.1.0-1  lm_sensors-1:3.6.0.r41.g31d1f125-1
               lz4-1:1.9.3-1  lzo-2.10-3  manjaro-keyring-20201216-1  mesa-20.3.4-3
               mhwd-amdgpu-19.1.0-1  mhwd-ati-19.1.0-1  mhwd-nvidia-460.56-2
               mhwd-nvidia-390xx-390.141-1  mkinitcpio-29-1.0  mkinitcpio-busybox-1.32.1-2
               mpfr-4.1.0-1  ncurses-6.2-1  nettle-3.7.1-1  nfsidmap-2.5.3-1  npth-1.6-3
               nspr-4.29-1  nss-3.62-1  openssl-1.1.1.j-1  p11-kit-0.23.22-1
               pacman-mirrors-4.19.5-2  pam-1.5.1-1  pambase-20200721.1-2  pcre-8.44-1
               pcre2-10.36-1  perl-clone-0.45-2  perl-encode-locale-1.05-7
               perl-file-listing-6.14-1  perl-html-parser-3.76-1  perl-html-tagset-3.20-10
               perl-http-cookies-6.10-1  perl-http-daemon-6.06-2  perl-http-date-6.05-3
               perl-http-message-6.29-1  perl-http-negotiate-6.01-8  perl-io-html-1.004-1
               perl-libwww-6.53-1  perl-lwp-mediatypes-6.02-8  perl-net-http-6.20-1
               perl-timedate-2.33-2  perl-try-tiny-0.30-5  perl-uri-5.09-1
               perl-www-robotrules-6.02-8  perl-xml-parser-2.46-2  perl-xml-writer-0.625-6
               pinentry-1.1.0-5  popt-1.18-1  python-3.9.2-1  python-appdirs-1.4.4-3
               python-chardet-3.0.4-7  python-idna-2.10-3  python-npyscreen-4.10.5-5
               python-ordered-set-4.0.2-3  python-packaging-20.9-1  python-pyparsing-2.4.7-3
               python-requests-2.25.1-1  python-setuptools-1:54.1.1-1  python-six-1.15.0-3
               python-urllib3-1.26.3-1  readline-8.1.0-2  rpcbind-1.2.5-3  run-parts-4.8.6.1-2
               shared-mime-info-2.0+57+gc1d1c70-1  sqlite-3.35.1-2  systemd-247.4-1
               systemd-libs-247.4-1  thin-provisioning-tools-0.9.0-1  tzdata-2021a-1
               util-linux-libs-2.36.2-1  v86d-0.1.10-5.1  vulkan-icd-loader-1.2.172-1
               wayland-1.19.0-1  wireless-regdb-2020.11.20-1  xcb-proto-1.14.1-3
               xorgproto-2021.3-1  xxhash-0.8.0-1  xz-5.2.5-1  zlib-1:1.2.11-4  zstd-1.4.9-1
               acpi-1.7-3  acpid-2.0.32-2  amd-ucode-20210315.r1846.3568f96-1  b43-fwcutter-019-3
               bash-completion-2.11-1  btrfs-progs-5.11-1  bzip2-1.0.8-4  coreutils-8.32-1
               cpupower-5.11-1  crda-4.14-3  cronie-1.5.5-2  cryptsetup-2.3.5-1
               dbus-x11-1.12.20-1  device-mapper-2.03.11-5  dhclient-4.4.2-2  dhcpcd-9.4.0-1
               diffutils-3.7-3  dmraid-1.0.0.rc16.3-13  dnsmasq-2.84-1  dosfstools-4.2-1
               e2fsprogs-1.46.2-1  ecryptfs-utils-111-4  efibootmgr-17-2  exfat-utils-1.3.0-2
               f2fs-tools-1.14.0-1  file-5.39-1  filesystem-2021.01.19-1  findutils-4.8.0-1
               gawk-5.1.0-1  gcc-libs-10.2.0-6  gettext-0.21-1  glibc-2.33-4  grep-3.6-1
               grub-2.04-22  gzip-1.10-3  haveged-1.9.14-1  inetutils-1.9.4-8
               intel-ucode-20210216-1  iproute2-5.11.0-1  iptables-1:1.8.7-1  iputils-20200821-1
               ipw2100-fw-1.3-10  ipw2200-fw-3.1-8  jfsutils-1.1.15-7  less-563-1
               licenses-20200427-1  linux-firmware-20210315.r1846.3568f96-1  linux54-5.4.105-1
               logrotate-3.18.0-1  lsb-release-1.4-13  lvm2-2.03.11-5  man-db-2.9.4-1
               man-pages-5.10-1  manjaro-firmware-20160419-1  manjaro-release-21.0-1
               manjaro-system-20210321-2  mdadm-4.1-2  memtest86+-5.01-4  mhwd-0.6.5-2
               mhwd-db-0.6.5-7  mkinitcpio-openswap-0.1.0-3  nano-5.6.1-1  nfs-utils-2.5.3-1
               ntfs-3g-2017.3.23-5  os-prober-1.78-0.1  pacman-5.2.2-4  pciutils-3.7.0-1
               perl-5.32.1-1  procps-ng-3.3.17-1  psmisc-23.4-1  reiserfsprogs-3.6.27-3
               rsync-3.2.3-3  s-nail-14.9.22-1  sed-4.8-1  shadow-4.8.1-4
               spectre-meltdown-checker-0.43-1  sudo-1.9.6.p1-1  sysfsutils-2.1.1-1
               systemd-fsck-silent-239-1  systemd-sysvcompat-247.4-1  tar-1.34-1  texinfo-6.7-3
               tlp-1.3.1-2  usbutils-013-1  util-linux-2.36.2-1  vi-1:070224-4  wget-1.21.1-1
               which-2.21-5  wpa_supplicant-2:2.9-8  xfsprogs-5.10.0-2  zsh-5.8-1

Total Installed Size:  1836.52 MiB

:: Proceed with installation? [Y/n]
(269/269) checking keys in keyring                          [################################] 100%
(269/269) checking package integrity                        [################################] 100%
(269/269) loading package files                             [################################] 100%
(269/269) checking for file conflicts                       [################################] 100%
:: Processing package changes...
(  1/269) installing lsb-release                            [################################] 100%
call to execv failed (No such file or directory)
error: command failed to execute correctly
(  2/269) installing manjaro-release                        [################################] 100%
call to execv failed (No such file or directory)
error: command failed to execute correctly
(  3/269) installing linux-api-headers                      [################################] 100%
(  4/269) installing tzdata                                 [################################] 100%
(  5/269) installing iana-etc                               [################################] 100%
(  6/269) installing filesystem                             [################################] 100%
(  7/269) installing glibc                                  [################################] 100%
Optional dependencies for glibc
    gd: for memusagestat
(  8/269) installing gcc-libs                               [################################] 100%
(  9/269) installing acpi                                   [################################] 100%
( 10/269) installing ncurses                                [################################] 100%
( 11/269) installing readline                               [################################] 100%
( 12/269) installing bashrc-manjaro                         [################################] 100%
( 13/269) installing bash                                   [################################] 100%
Optional dependencies for bash
    bash-completion: for tab completion [pending]
( 14/269) installing acpid                                  [################################] 100%
Optional dependencies for acpid
    perl: use perl based examples [pending]
( 15/269) installing amd-ucode                              [################################] 100%
( 16/269) installing b43-fwcutter                           [################################] 100%
( 17/269) installing bash-completion                        [################################] 100%
( 18/269) installing util-linux-libs                        [################################] 100%
( 19/269) installing lzo                                    [################################] 100%
( 20/269) installing zlib                                   [################################] 100%
( 21/269) installing xz                                     [################################] 100%
( 22/269) installing lz4                                    [################################] 100%
( 23/269) installing zstd                                   [################################] 100%
( 24/269) installing libgpg-error                           [################################] 100%
( 25/269) installing libgcrypt                              [################################] 100%
( 26/269) installing btrfs-progs                            [################################] 100%
Optional dependencies for btrfs-progs
    python: libbtrfsutil python bindings [pending]
    e2fsprogs: btrfs-convert [pending]
    reiserfsprogs: btrfs-convert [pending]
( 27/269) installing bzip2                                  [################################] 100%
( 28/269) installing attr                                   [################################] 100%
( 29/269) installing acl                                    [################################] 100%
( 30/269) installing gmp                                    [################################] 100%
( 31/269) installing e2fsprogs                              [################################] 100%
( 32/269) installing openssl                                [################################] 100%
Optional dependencies for openssl
    ca-certificates [pending]
    perl [pending]
( 33/269) installing libsasl                                [################################] 100%
( 34/269) installing libldap                                [################################] 100%
( 35/269) installing keyutils                               [################################] 100%
( 36/269) installing krb5                                   [################################] 100%
( 37/269) installing libtirpc                               [################################] 100%
( 38/269) installing pambase                                [################################] 100%
( 39/269) installing libcap-ng                              [################################] 100%
( 40/269) installing audit                                  [################################] 100%
( 41/269) installing libxcrypt                              [################################] 100%
( 42/269) installing pam                                    [################################] 100%
( 43/269) installing libcap                                 [################################] 100%
( 44/269) installing coreutils                              [################################] 100%
( 45/269) installing hwids                                  [################################] 100%
( 46/269) installing kmod                                   [################################] 100%
( 47/269) installing pciutils                               [################################] 100%
( 48/269) installing cpupower                               [################################] 100%
( 49/269) installing wireless-regdb                         [################################] 100%
( 50/269) installing libnl                                  [################################] 100%
( 51/269) installing systemd-libs                           [################################] 100%
( 52/269) installing device-mapper                          [################################] 100%
( 53/269) installing popt                                   [################################] 100%
( 54/269) installing json-c                                 [################################] 100%
( 55/269) installing argon2                                 [################################] 100%
( 56/269) installing cryptsetup                             [################################] 100%
( 57/269) installing expat                                  [################################] 100%
( 58/269) installing dbus-x11                               [################################] 100%
( 59/269) installing libmnl                                 [################################] 100%
( 60/269) installing libnftnl                               [################################] 100%
( 61/269) installing libpcap                                [################################] 100%
( 62/269) installing libnfnetlink                           [################################] 100%
( 63/269) installing libnetfilter_conntrack                 [################################] 100%
( 64/269) installing iptables                               [################################] 100%
( 65/269) installing kbd                                    [################################] 100%
( 66/269) installing libunistring                           [################################] 100%
( 67/269) installing libidn2                                [################################] 100%
( 68/269) installing findutils                              [################################] 100%
( 69/269) installing libtasn1                               [################################] 100%
( 70/269) installing libffi                                 [################################] 100%
( 71/269) installing libp11-kit                             [################################] 100%
( 72/269) installing p11-kit                                [################################] 100%
( 73/269) installing ca-certificates-utils                  [################################] 100%
( 74/269) installing ca-certificates-mozilla                [################################] 100%
( 75/269) installing ca-certificates                        [################################] 100%
( 76/269) installing libssh2                                [################################] 100%
( 77/269) installing libpsl                                 [################################] 100%
( 78/269) installing libnghttp2                             [################################] 100%
( 79/269) installing curl                                   [################################] 100%
( 80/269) installing libelf                                 [################################] 100%
( 81/269) installing libseccomp                             [################################] 100%
( 82/269) installing shadow                                 [################################] 100%
( 83/269) installing file                                   [################################] 100%
( 84/269) installing util-linux                             [################################] 100%
Optional dependencies for util-linux
    python: python bindings to libmount [pending]
    words: default dictionary for look
( 85/269) installing pcre2                                  [################################] 100%
( 86/269) installing systemd                                [################################] 100%
Initializing machine ID from random generator.
Creating group adm with gid 999.
Creating group wheel with gid 998.
Creating group kmem with gid 997.
Creating group tty with gid 5.
Creating group utmp with gid 996.
Creating group audio with gid 995.
Creating group disk with gid 994.
Creating group input with gid 993.
Creating group kvm with gid 992.
Creating group lp with gid 991.
Creating group optical with gid 990.
Creating group render with gid 989.
Creating group storage with gid 988.
Creating group uucp with gid 987.
Creating group video with gid 986.
Creating group users with gid 985.
Creating group sys with gid 3.
Creating group mem with gid 8.
Creating group ftp with gid 11.
Creating group mail with gid 12.
Creating group log with gid 19.
Creating group smmsp with gid 25.
Creating group proc with gid 26.
Creating group games with gid 50.
Creating group lock with gid 54.
Creating group network with gid 90.
Creating group floppy with gid 94.
Creating group scanner with gid 96.
Creating group power with gid 98.
Creating group systemd-journal with gid 984.
Creating group rfkill with gid 983.
Creating group nobody with gid 65534.
Creating user nobody (Nobody) with uid 65534 and gid 65534.
Creating group dbus with gid 81.
Creating user dbus (System Message Bus) with uid 81 and gid 81.
Creating group bin with gid 1.
Creating user bin (n/a) with uid 1 and gid 1.
Creating group daemon with gid 2.
Creating user daemon (n/a) with uid 2 and gid 2.
Creating user mail (n/a) with uid 8 and gid 12.
Creating user ftp (n/a) with uid 14 and gid 11.
Creating group http with gid 33.
Creating user http (n/a) with uid 33 and gid 33.
Creating group systemd-journal-remote with gid 982.
Creating user systemd-journal-remote (systemd Journal Remote) with uid 982 and gid 982.
Creating group systemd-network with gid 981.
Creating user systemd-network (systemd Network Management) with uid 981 and gid 981.
Creating group systemd-resolve with gid 980.
Creating user systemd-resolve (systemd Resolver) with uid 980 and gid 980.
Creating group systemd-timesync with gid 979.
Creating user systemd-timesync (systemd Time Synchronization) with uid 979 and gid 979.
Creating group systemd-coredump with gid 978.
Creating user systemd-coredump (systemd Core Dumper) with uid 978 and gid 978.
Creating group uuidd with gid 68.
Creating user uuidd (n/a) with uid 68 and gid 68.
Created symlink /etc/systemd/system/getty.target.wants/getty@tty1.service → /usr/lib/systemd/system/getty@.service.
Created symlink /etc/systemd/system/multi-user.target.wants/remote-fs.target → /usr/lib/systemd/system/remote-fs.target.
:: Append 'init=/usr/lib/systemd/systemd' to your kernel command line in your
   bootloader to replace sysvinit with systemd, or install systemd-sysvcompat
Optional dependencies for systemd
    libmicrohttpd: remote journald capabilities
    quota-tools: kernel-level quota management
    systemd-sysvcompat: symlink package to provide sysvinit binaries [pending]
    polkit: allow administration as unprivileged user
    curl: machinectl pull-tar and pull-raw [installed]
( 87/269) installing iw                                     [################################] 100%
( 88/269) installing crda                                   [################################] 100%
Uncomment the right regulatory domain in /etc/conf.d/wireless-regdom.
It will automatically be set on boot.
( 89/269) installing run-parts                              [################################] 100%
( 90/269) installing cronie                                 [################################] 100%
Optional dependencies for cronie
    smtp-server: send job output via email
    smtp-forwarder: forward job output to email server
( 91/269) installing iproute2                               [################################] 100%
Optional dependencies for iproute2
    db: userspace arp daemon [pending]
    libcap: tipc [installed]
    linux-atm: ATM support
( 92/269) installing dhclient                               [################################] 100%
( 93/269) installing dhcpcd                                 [################################] 100%
Optional dependencies for dhcpcd
    openresolv: resolvconf support
( 94/269) installing diffutils                              [################################] 100%
( 95/269) installing dmraid                                 [################################] 100%
( 96/269) installing nettle                                 [################################] 100%
( 97/269) installing dnsmasq                                [################################] 100%
( 98/269) installing dosfstools                             [################################] 100%
( 99/269) installing nspr                                   [################################] 100%
(100/269) installing sqlite                                 [################################] 100%
(101/269) installing nss                                    [################################] 100%
(102/269) installing ecryptfs-utils                         [################################] 100%
Optional dependencies for ecryptfs-utils
    python2: for python module
(103/269) installing efivar                                 [################################] 100%
(104/269) installing efibootmgr                             [################################] 100%
(105/269) installing fuse-common                            [################################] 100%
(106/269) installing fuse2                                  [################################] 100%
(107/269) installing exfat-utils                            [################################] 100%
(108/269) installing f2fs-tools                             [################################] 100%
(109/269) installing mpfr                                   [################################] 100%
(110/269) installing gawk                                   [################################] 100%
(111/269) installing pcre                                   [################################] 100%
(112/269) installing glib2                                  [################################] 100%
Optional dependencies for glib2
    python: gdbus-codegen, glib-genmarshal, glib-mkenums, gtester-report [pending]
    libelf: gresource inspection tool [installed]
(113/269) installing icu                                    [################################] 100%
(114/269) installing libxml2                                [################################] 100%
(115/269) installing libcroco                               [################################] 100%
(116/269) installing gettext                                [################################] 100%
Optional dependencies for gettext
    git: for autopoint infrastructure updates
(117/269) installing grep                                   [################################] 100%
(118/269) installing grub                                   [################################] 100%
Generating grub.cfg.example config file...
This may fail on some machines running a custom kernel.
done.
Optional dependencies for grub
    freetype2: For grub-mkfont usage
    fuse2: For grub-mount usage [installed]
    dosfstools: For grub-mkrescue FAT FS and EFI support [installed]
    efibootmgr: For grub-install EFI support [installed]
    libisoburn: Provides xorriso for generating grub rescue iso using grub-mkrescue
    os-prober: To detect other OSes when generating grub.cfg in BIOS systems [pending]
    mtools: For grub-mkrescue FAT FS support
    libusb: For grub-emu USB support [pending]
    sdl: For grub-emu SDL support
(119/269) installing less                                   [################################] 100%
(120/269) installing gzip                                   [################################] 100%
(121/269) installing haveged                                [################################] 100%
(122/269) installing inetutils                              [################################] 100%
(123/269) installing intel-ucode                            [################################] 100%
(124/269) installing iputils                                [################################] 100%
(125/269) installing ipw2100-fw                             [################################] 100%
(126/269) installing ipw2200-fw                             [################################] 100%
(127/269) installing jfsutils                               [################################] 100%
(128/269) installing linux-firmware                         [################################] 100%
(129/269) installing mkinitcpio-busybox                     [################################] 100%
(130/269) installing libarchive                             [################################] 100%
(131/269) installing gdbm                                   [################################] 100%
(132/269) installing libnsl                                 [################################] 100%
(133/269) installing python                                 [################################] 100%
Optional dependencies for python
    python-setuptools [pending]
    python-pip
    sqlite [installed]
    mpdecimal: for decimal
    xz: for lzma [installed]
    tk: for tkinter
(134/269) installing libxslt                                [################################] 100%
(135/269) installing docbook-xml                            [################################] 100%
(136/269) installing docbook-xsl                            [################################] 100%
(137/269) installing asciidoc                               [################################] 100%
Optional dependencies for asciidoc
    graphviz: graphviz-filter
    lilypond: music-filter
    imagemagick: music-filter
    source-highlight: source-highlight-filter
    dblatex: pdf generation
    fop: alternative pdf generation
    w3m: text generation
    lynx: alternative text generation
(138/269) installing mkinitcpio                             [################################] 100%
Optional dependencies for mkinitcpio
    xz: Use lzma or xz compression for the initramfs image [installed]
    bzip2: Use bzip2 compression for the initramfs image [installed]
    lzop: Use lzo compression for the initramfs image
    lz4: Use lz4 compression for the initramfs image [installed]
    mkinitcpio-nfs-utils: Support for root filesystem on NFS
(139/269) installing linux54                                [################################] 100%
Optional dependencies for linux54
    crda: to set the correct wireless channels of your country [installed]
(140/269) installing licenses                               [################################] 100%
(141/269) installing logrotate                              [################################] 100%
(142/269) installing libaio                                 [################################] 100%
(143/269) installing thin-provisioning-tools                [################################] 100%
(144/269) installing lvm2                                   [################################] 100%
(145/269) installing db                                     [################################] 100%
(146/269) installing perl                                   [################################] 100%
(147/269) installing groff                                  [################################] 100%
Optional dependencies for groff
    netpbm: for use together with man -H command interaction in browsers
    psutils: for use together with man -H command interaction in browsers
    libxaw: for gxditview
    perl-file-homedir: for use with glilypond
(148/269) installing libpipeline                            [################################] 100%
(149/269) installing man-db                                 [################################] 100%
Optional dependencies for man-db
    gzip [installed]
(150/269) installing man-pages                              [################################] 100%
(151/269) installing manjaro-firmware                       [################################] 100%
(152/269) installing npth                                   [################################] 100%
(153/269) installing libksba                                [################################] 100%
(154/269) installing libassuan                              [################################] 100%
(155/269) installing libsecret                              [################################] 100%
Optional dependencies for libsecret
    org.freedesktop.secrets: secret storage backend
(156/269) installing pinentry                               [################################] 100%
Optional dependencies for pinentry
    gtk2: gtk2 backend
    qt5-base: qt backend
    gcr: gnome3 backend
(157/269) installing gnutls                                 [################################] 100%
Optional dependencies for gnutls
    guile: for use with Guile bindings
(158/269) installing gnupg                                  [################################] 100%
Optional dependencies for gnupg
    libldap: gpg2keys_ldap [installed]
    libusb-compat: scdaemon
    pcsclite: scdaemon
(159/269) installing gpgme                                  [################################] 100%
(160/269) installing archlinux-keyring                      [################################] 100%
(161/269) installing manjaro-keyring                        [################################] 100%
(162/269) installing python-npyscreen                       [################################] 100%
(163/269) installing python-urllib3                         [################################] 100%
Optional dependencies for python-urllib3
    python-pysocks: SOCKS support
    python-brotli: Brotli support
    python-pyopenssl: security support
(164/269) installing python-appdirs                         [################################] 100%
(165/269) installing python-pyparsing                       [################################] 100%
(166/269) installing python-six                             [################################] 100%
(167/269) installing python-packaging                       [################################] 100%
(168/269) installing python-ordered-set                     [################################] 100%
(169/269) installing python-setuptools                      [################################] 100%
(170/269) installing python-chardet                         [################################] 100%
(171/269) installing python-idna                            [################################] 100%
(172/269) installing python-requests                        [################################] 100%
Optional dependencies for python-requests
    python-pysocks: SOCKS proxy support
(173/269) installing pacman-mirrors                         [################################] 100%
Optional dependencies for pacman-mirrors
    gtk3: for interactive mode (GUI)
    python-gobject: for interactive mode (GUI)
(174/269) installing pacman                                 [################################] 100%
==> To import the data required by pacman for package verification run:
==> `pacman-key --init; pacman-key --populate archlinux manjaro`
==> See: https://www.archlinux.org/news/having-pacman-verify-packages
Optional dependencies for pacman
    haveged: for pacman-init.service [installed]
    perl-locale-gettext: translation support in makepkg-template
    findutils: for pacdiff --find [installed]
    mlocate: for pacdiff --locate
    sudo: privilege elevation for several scripts [pending]
    vim: default merge program for pacdiff
(175/269) installing sed                                    [################################] 100%
(176/269) installing manjaro-system                         [################################] 100%
(177/269) installing mdadm                                  [################################] 100%
(178/269) installing memtest86+                             [################################] 100%
(179/269) installing libx86emu                              [################################] 100%
(180/269) installing perl-encode-locale                     [################################] 100%
(181/269) installing perl-timedate                          [################################] 100%
(182/269) installing perl-http-date                         [################################] 100%
(183/269) installing perl-file-listing                      [################################] 100%
(184/269) installing perl-html-tagset                       [################################] 100%
(185/269) installing perl-clone                             [################################] 100%
(186/269) installing perl-io-html                           [################################] 100%
(187/269) installing perl-lwp-mediatypes                    [################################] 100%
(188/269) installing perl-uri                               [################################] 100%
(189/269) installing perl-http-message                      [################################] 100%
(190/269) installing perl-html-parser                       [################################] 100%
(191/269) installing perl-http-cookies                      [################################] 100%
(192/269) installing perl-http-daemon                       [################################] 100%
(193/269) installing perl-http-negotiate                    [################################] 100%
(194/269) installing perl-net-http                          [################################] 100%
(195/269) installing perl-try-tiny                          [################################] 100%
(196/269) installing perl-www-robotrules                    [################################] 100%
(197/269) installing perl-libwww                            [################################] 100%
Optional dependencies for perl-libwww
    perl-lwp-protocol-https: for https:// url schemes
(198/269) installing perl-xml-parser                        [################################] 100%
(199/269) installing perl-xml-writer                        [################################] 100%
(200/269) installing hwinfo                                 [################################] 100%
(201/269) installing libpciaccess                           [################################] 100%
(202/269) installing libdrm                                 [################################] 100%
(203/269) installing wayland                                [################################] 100%
(204/269) installing xcb-proto                              [################################] 100%
(205/269) installing libxdmcp                               [################################] 100%
(206/269) installing libxau                                 [################################] 100%
(207/269) installing libxcb                                 [################################] 100%
(208/269) installing xorgproto                              [################################] 100%
(209/269) installing libx11                                 [################################] 100%
(210/269) installing libxext                                [################################] 100%
(211/269) installing libxxf86vm                             [################################] 100%
(212/269) installing libxfixes                              [################################] 100%
(213/269) installing libxdamage                             [################################] 100%
(214/269) installing libxshmfence                           [################################] 100%
(215/269) installing libomxil-bellagio                      [################################] 100%
(216/269) installing libunwind                              [################################] 100%
(217/269) installing libedit                                [################################] 100%
(218/269) installing llvm-libs                              [################################] 100%
(219/269) installing lm_sensors                             [################################] 100%
Optional dependencies for lm_sensors
    rrdtool: for logging with sensord
    perl: for sensor detection and configuration convert [installed]
(220/269) installing libglvnd                               [################################] 100%
(221/269) installing vulkan-icd-loader                      [################################] 100%
Optional dependencies for vulkan-icd-loader
    vulkan-driver: packaged vulkan driver
(222/269) installing mesa                                   [################################] 100%
Optional dependencies for mesa
    opengl-man-pages: for the OpenGL API man pages
    mesa-vdpau: for accelerated video playback
    libva-mesa-driver: for accelerated video playback
(223/269) installing libpng                                 [################################] 100%
(224/269) installing libjpeg-turbo                          [################################] 100%
Optional dependencies for libjpeg-turbo
    java-runtime>11: for TurboJPEG Java wrapper
(225/269) installing libtiff                                [################################] 100%
Optional dependencies for libtiff
    freeglut: for using tiffgt
(226/269) installing shared-mime-info                       [################################] 100%
(227/269) installing gdk-pixbuf2                            [################################] 100%
(228/269) installing libnotify                              [################################] 100%
(229/269) installing mhwd-nvidia-390xx                      [################################] 100%
(230/269) installing mhwd-amdgpu                            [################################] 100%
(231/269) installing mhwd-ati                               [################################] 100%
(232/269) installing mhwd-nvidia                            [################################] 100%
(233/269) installing mhwd-db                                [################################] 100%
(234/269) installing v86d                                   [################################] 100%
(235/269) installing mhwd                                   [################################] 100%
Optional dependencies for mhwd
    lib32-mesa: for 32bit libgl support
(236/269) installing mkinitcpio-openswap                    [################################] 100%

Alter /etc/openswap.conf file for your swap device name, keyfiles, etc...

For more information see: https://wiki.archlinux.org/index.php/Dm-crypt/Swap_encryption#mkinitcpio_hook

Don't forget to add the openswap hook after encrypt and before resume in your /etc/mkinitcpio.conf and run mkinitcpio -p linux...

(237/269) installing nano                                   [################################] 100%
(238/269) installing rpcbind                                [################################] 100%
(239/269) installing nfsidmap                               [################################] 100%
(240/269) installing ding-libs                              [################################] 100%
(241/269) installing gssproxy                               [################################] 100%
(242/269) installing libevent                               [################################] 100%
Optional dependencies for libevent
    python: to use event_rpcgen.py [installed]
(243/269) installing nfs-utils                              [################################] 100%
Optional dependencies for nfs-utils
    sqlite: for nfsdcltrack usage [installed]
    python: for nfsiostat, nfsdclnts and mountstats usage [installed]
(244/269) installing ntfs-3g                                [################################] 100%
(245/269) installing os-prober                              [################################] 100%
(246/269) installing procps-ng                              [################################] 100%
(247/269) installing psmisc                                 [################################] 100%
(248/269) installing reiserfsprogs                          [################################] 100%
(249/269) installing xxhash                                 [################################] 100%
(250/269) installing rsync                                  [################################] 100%
(251/269) installing s-nail                                 [################################] 100%
Optional dependencies for s-nail
    smtp-forwarder: for sending mail
(252/269) installing spectre-meltdown-checker               [################################] 100%
(253/269) installing sudo                                   [################################] 100%
(254/269) installing sysfsutils                             [################################] 100%
(255/269) installing systemd-fsck-silent                    [################################] 100%
(256/269) installing systemd-sysvcompat                     [################################] 100%
(257/269) installing tar                                    [################################] 100%
(258/269) installing texinfo                                [################################] 100%
(259/269) installing hdparm                                 [################################] 100%
Optional dependencies for hdparm
    bash: for wiper.sh script [installed]
(260/269) installing libusb                                 [################################] 100%
(261/269) installing usbutils                               [################################] 100%
Optional dependencies for usbutils
    python: for lsusb.py usage [installed]
    coreutils: for lsusb.py usage [installed]
(262/269) installing tlp                                    [################################] 100%
Optional dependencies for tlp
    acpi_call: ThinkPad battery functions, Sandy Bridge and newer
    bash-completion: Bash completion [installed]
    ethtool: Disable Wake On Lan
    lsb-release: Display LSB release version in tlp-stat [installed]
    smartmontools: Display S.M.A.R.T. data in tlp-stat
    tp_smapi: ThinkPad battery functions
    x86_energy_perf_policy: Set energy versus performance policy on x86 processors
(263/269) installing vi                                     [################################] 100%
Optional dependencies for vi
    s-nail: used by the preserve command for notification [installed]
(264/269) installing wget                                   [################################] 100%
Optional dependencies for wget
    ca-certificates: HTTPS downloads [installed]
(265/269) installing which                                  [################################] 100%
(266/269) installing wpa_supplicant                         [################################] 100%
(267/269) installing libinih                                [################################] 100%
(268/269) installing xfsprogs                               [################################] 100%
Optional dependencies for xfsprogs
    python: for xfs_scrub_all script [installed]
    smtp-forwarder: for xfs_scrub_fail script
(269/269) installing zsh                                    [################################] 100%
:: Running post-transaction hooks...
( 1/18) Creating system user accounts...
Creating group dhcpcd with gid 977.
Creating user dhcpcd (dhcpcd privilege separation) with uid 977 and gid 977.
Creating group dnsmasq with gid 976.
Creating user dnsmasq (dnsmasq daemon) with uid 976 and gid 976.
Creating group rpc with gid 32.
Creating user rpc (Rpcbind Daemon) with uid 32 and gid 32.
( 2/18) Updating journal message catalog...
( 3/18) Reloading system manager configuration...
  Skipped: Current root is not booted.
( 4/18) Updating udev hardware database...
( 5/18) Applying kernel sysctl settings...
  Skipped: Current root is not booted.
( 6/18) Creating temporary files...
( 7/18) Reloading device manager configuration...
  Skipped: Device manager is not running.
( 8/18) Arming ConditionNeedsUpdate...
( 9/18) Rebuilding certificate stores...
(10/18) Updating module dependencies...
(11/18) Updating linux initcpios...
==> Building image from preset: /etc/mkinitcpio.d/linux54.preset: 'default'
  -> -k /boot/vmlinuz-5.4-x86_64 -c /etc/mkinitcpio.conf -g /boot/initramfs-5.4-x86_64.img
==> Starting build: 5.4.105-1-MANJARO
  -> Running build hook: [base]
  -> Running build hook: [udev]
  -> Running build hook: [autodetect]
  -> Running build hook: [modconf]
  -> Running build hook: [block]
  -> Running build hook: [filesystems]
  -> Running build hook: [keyboard]
  -> Running build hook: [fsck]
==> Generating module dependencies
==> Creating gzip-compressed initcpio image: /boot/initramfs-5.4-x86_64.img
bsdtar: Failed to set default locale
bsdtar: Failed to set default locale
==> Image generation successful
==> Building image from preset: /etc/mkinitcpio.d/linux54.preset: 'fallback'
  -> -k /boot/vmlinuz-5.4-x86_64 -c /etc/mkinitcpio.conf -g /boot/initramfs-5.4-x86_64-fallback.img -S autodetect
==> Starting build: 5.4.105-1-MANJARO
  -> Running build hook: [base]
  -> Running build hook: [udev]
  -> Running build hook: [modconf]
  -> Running build hook: [block]
  -> Running build hook: [filesystems]
  -> Running build hook: [keyboard]
  -> Running build hook: [fsck]
==> Generating module dependencies
==> Creating gzip-compressed initcpio image: /boot/initramfs-5.4-x86_64-fallback.img
bsdtar: Failed to set default locale
bsdtar: Failed to set default locale
==> Image generation successful
(12/18) Updating Grub-Bootmenu
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-5.4-x86_64
Found initrd image: /boot/intel-ucode.img /boot/amd-ucode.img /boot/initramfs-5.4-x86_64.img
Found initrd fallback image: /boot/initramfs-5.4-x86_64-fallback.img
Warning: os-prober will not be executed to detect other bootable partitions.
Systems on them will not be added to the GRUB boot configuration.
Check GRUB_DISABLE_OS_PROBER documentation entry.
Found memtest86+ image: /boot/memtest86+/memtest.bin
done
(13/18) Reloading system bus configuration...
  Skipped: Current root is not booted.
(14/18) Warn about old perl modules
perl: warning: Setting locale failed.
perl: warning: Please check that your locale settings:
	LANGUAGE = (unset),
	LC_ALL = (unset),
	LC_ADDRESS = "en_IN",
	LC_MONETARY = "en_IN",
	LC_PAPER = "en_IN",
	LC_MESSAGES = "C",
	LC_MEASUREMENT = "en_IN",
	LC_TIME = "en_IN",
	LANG = "C"
    are supported and installed on your system.
perl: warning: Falling back to the standard locale ("C").
(15/18) Probing GDK-Pixbuf loader modules...
(16/18) Configuring pacman-mirrors ...
::WARNING https://gitlab.manjaro.org '<urlopen error [Errno -2] Name or service not known>'
::WARNING https://wikipedia.org '<urlopen error [Errno -2] Name or service not known>'
::WARNING https://bitbucket.org '<urlopen error [Errno -2] Name or service not known>'
::INFO Internet connection appears to be down
::INFO Mirror ranking is not available
::INFO Mirror list is generated using random method
::INFO Writing mirror list
::Ecuador         : https://mirror.espoch.edu.ec/manjaro/stable
::Belgium         : http://ftp.belnet.be/mirrors/manjaro/stable
::Brazil          : http://pet.inf.ufsc.br/mirrors/manjarolinux/stable
::Australia       : http://mirror.ventraip.net.au/Manjaro/stable
::Belgium         : https://manjaro.cu.be/stable
::Japan           : http://ftp.riken.jp/Linux/manjaro/stable
::Germany         : https://manjaro.moson.eu/stable
::United_States   : http://distro.ibiblio.org/manjaro/stable
::Sweden          : https://ftp.lysator.liu.se/pub/manjaro/stable
::France          : http://ftp.free.org/mirrors/repo.manjaro.org/repos/stable
::Greece          : https://ftp.cc.uoc.gr/mirrors/linux/manjaro/stable
::Denmark         : https://mirrors.dotsrc.org/manjaro/stable
::United_States   : https://mirrors.ocf.berkeley.edu/manjaro/stable
::Poland          : http://mirror.chmuri.net/manjaro/stable
::Bulgaria        : https://mirrors.netix.net/manjaro/stable
::Netherlands     : https://ftp.nluug.nl/pub/os/Linux/distr/manjaro/stable
::Germany         : https://mirror.netcologne.de/manjaro/stable
::Hungary         : http://mirror.infotronik.hu/mirrors/pub/manjaro/stable
::Bulgaria        : https://manjaro.ipacct.com/manjaro/stable
::China           : https://mirrors.sjtug.sjtu.edu.cn/manjarostable
::Denmark         : https://www.uex.dk/public/manjaro/stable
::Brazil          : http://linorg.usp.br/manjaro/stable
::Ecuador         : https://mirror.cedia.org.ec/manjaro/stable
::Brazil          : http://mirror.ufam.edu.br/manjaro/stable
::Germany         : http://mirror.ragenetwork.de/manjaro/stable
::China           : https://mirrors.shu.edu.cn/manjaro/stable
::China           : http://mirrors.tuna.tsinghua.edu.cn/manjaro/stable
::Netherlands     : https://mirror.koddos.net/manjaro/stable
::United_Kingdom  : https://www.mirrorservice.org/sites/repo.manjaro.org/repos/sta
::Germany         : https://mirror.philpot.de/manjaro/stable
::Colombia        : http://mirror.upb.edu.co/manjarostable
::Bulgaria        : http://manjaro.telecoms.bg/stable
::Australia       : http://manjaro.melbourneitmirror.net/stable
::United_Kingdom  : http://repo.manjaro.org.uk/stable
::Singapore       : https://download.nus.edu.sg/mirror/manjaro/stable
::Philippines     : http://mirror.rise.ph/manjaro/stable
::Italy           : https://ba.mirror.garr.it/mirrors/manjaro/stable
::Germany         : http://ftp.tu-chemnitz.de/pub/linux/manjaro/stable
::Italy           : https://manjaro.mirror.garr.it/mirrors/manjaro/stable
::China           : https://mirrors.ustc.edu.cn/manjaro/stable
::Japan           : http://ftp.tsukuba.wide.ad.jp/Linux/manjaro/stable
::Austria         : http://mirror.inode.at/manjaro/stable
::Germany         : https://mirror.netzspielplatz.de/manjaro/packages/stable
::Costa_Rica      : https://mirrors.ucr.ac.cr/manjaro/stable
::Italy           : https://ct.mirror.garr.it/mirrors/manjaro/stable
::Indonesia       : http://kartolo.sby.datautama.net.id/manjaro/stable
::China           : https://mirrors.zju.edu.cn/manjaro/stable
::Chile           : http://manjaro.dcc.uchile.cl/stable
::France          : http://kibo.remi.lu/stable
::Russia          : https://mirror.yandex.ru/mirrors/manjaro/stable
::United_Kingdom  : http://manjaro.mirrors.uk2.net/stable
::Brazil          : https://manjaro.c3sl.ufpr.br/stable
::China           : https://mirrors.shuosc.org/manjaro/stable
::Germany         : https://repo.rhindon.net/manjaro/stable
::Belarus         : http://mirror.datacenter.by/pub/mirrors/manjaro/stable
::Romania         : http://mirrors.serverhost.ro/manjaro/packages/stable
::South_Africa    : http://mirror.is.co.za/mirrors/manjaro.org/stable
::United_States   : https://mirror.math.princeton.edu/pub/manjaro/stable
::Spain           : http://ftp.caliu.cat/manjaro/stable
::United_States   : http://mirror.dacentec.com/manjaro/stable
::United_States   : https://mirror.clarkson.edu/manjaro/stable
::Portugal        : http://manjaro.barata.pt/stable
::Poland          : https://mirror.tuchola-dc.pl/manjaro/stable
::Russia          : http://mirror.truenetwork.ru/manjaro/stable
::Hong_Kong       : http://ftp.cuhk.edu.hk/pub/Linux/manjaro/stable
::Czech           : https://mirror.dkm.cz/manjaro/stable
::United_Kingdom  : http://mirror.catn.com/pub/manjaro/stable
::Netherlands     : http://ftp.snt.utwente.nl/pub/linux/manjaro/stable
::South_Africa    : http://manjaro.mirror.ac.za/stable
::Sweden          : https://mirror.zetup.net/manjaro/stable
::Bangladesh      : http://mirror.xeonbd.com/manjaro/stable
::Indonesia       : http://kambing.ui.ac.id/manjaro/stable
::Canada          : https://osmirror.org/manjaro/stable
::Germany         : http://ftp.rz.tu-bs.de/pub/mirror/manjaro.org/repos/stable
::Turkey          : http://ftp.linux.org.tr/manjaro/stable
::Taiwan          : http://free.nchc.org.tw/manjaro/stable
::Iran            : https://repo.sadjad.ac.ir/manjaro/stable
::Germany         : https://ftp.halifax.rwth-aachen.de/manjaro/stable
::INFO Mirror list generated and saved to: /etc/pacman.d/mirrorlist
hint: use `pacman-mirrors` to generate and update your pacman mirrorlist.
(17/18) Updating the info directory file...
(18/18) Updating the MIME type database...
 --> Using build locales ...
 --> Setting mirrorlist branch: stable
Generating locales...
  en_US.UTF-8... done
Generation complete.
 --> Restoring [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/rootfs/etc/pacman.conf] ...
  -> Configuring lsb-release
  -> Cleaning [rootfs]
==> Done [Base installation] (rootfs)
 --> Loading Packages: [Packages-Desktop] ...
==> Prepare [Desktop installation] (desktopfs)
 --> overlayfs mount: [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/desktopfs]
 --> mirror: https://manjaro.moson.eu/stable/$repo/$arch
==> Creating install root at /var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/desktopfs
  -> Installing packages to /var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/desktopfs
:: Synchronizing package databases...
 core                           164.9 KiB   227 KiB/s 00:01 [################################] 100%
 extra                         2000.5 KiB   424 KiB/s 00:05 [################################] 100%
 community                        6.6 MiB   662 KiB/s 00:10 [################################] 100%
 multilib                       180.5 KiB  2005 KiB/s 00:00 [################################] 100%
resolving dependencies...
:: There are 3 providers available for xdg-desktop-portal-impl:
:: Repository extra
   1) xdg-desktop-portal-gtk  2) xdg-desktop-portal-kde
:: Repository community
   3) xdg-desktop-portal-wlr

Enter a number (default=1):
looking for conflicting packages...
warning: dependency cycle detected:
warning: cifs-utils will be installed before its smbclient dependency
warning: dependency cycle detected:
warning: harfbuzz will be installed before its freetype2 dependency
warning: dependency cycle detected:
warning: usbmuxd will be installed before its libimobiledevice dependency
warning: dependency cycle detected:
warning: lib32-mesa will be installed before its lib32-libglvnd dependency
warning: dependency cycle detected:
warning: lib32-keyutils will be installed before its lib32-krb5 dependency

Packages (695) a52dec-0.7.4-11  aalib-1.4rc5-14
               adobe-source-code-pro-fonts-2.038ro+1.058it+1.018var-1  adwaita-icon-theme-3.38.0-1
               alsa-card-profiles-1:0.3.23-1  alsa-lib-1.2.4-3  alsa-topology-conf-1.2.4-2
               alsa-ucm-conf-1.2.4-2  aom-2.0.2-1  appstream-glib-0.7.18-2
               archlinux-appstream-data-20210218-1  at-spi2-atk-2.38.0-1  at-spi2-core-2.38.0-1
               atk-2.36.0-1  atkmm-2.28.1-1  audacious-plugins-4.1-3  babl-0.1.84-1  bc-1.07.1-4
               blas-3.9.0-3  bluez-5.56-1  bluez-libs-5.56-1  boost-libs-1.75.0-2  botan-2.17.3-1
               brotli-1.0.9-4  bubblewrap-0.4.1-1  cairo-1.17.4-5  cairomm-1.12.2-4
               cantarell-fonts-1:0.301-1  cdparanoia-10.2-8  ceph-libs-15.2.8-2
               chromaprint-1.5.0-3  cifs-utils-6.12-1  ckbcomp-1.194-1  colord-1.4.5-1
               colord-sane-1.4.5-1  confuse-3.3-3  cups-1:2.3.3op2-1  cups-filters-1.28.7-1
               cups-pdf-3.0.1-5  cups-pk-helper-0.2.6-4  dav1d-0.8.2-1  dbus-glib-0.110-2
               dconf-0.40.0-1  desktop-file-utils-0.26-1  dnssec-anchors-20190629-3
               double-conversion-3.1.5-2  enchant-2.2.15-1  exiv2-0.27.3-1  exo-4.16.1-1
               faac-1.30-2  faad2-2.10.0-1  farstream-0.2.9-2  fftw-3.3.9-1  flac-1.3.3-2
               flatpak-1.10.2-1  fluidsynth-2.1.7-1  fmt-7.1.3-1
               fontconfig-2:2.13.91+48+gfcb0420-2  foomatic-db-3:20210222-1
               foomatic-db-engine-4:20200206-1  foomatic-db-gutenprint-ppds-5.3.4-1
               freeglut-3.2.1-2  fribidi-1.0.10-1  fuse3-3.10.2-1  gcab-1.4-1  gcr-3.38.1-1
               gd-2.3.2-2  gegl-0.4.28-2  geoclue-2.5.7-2  geocode-glib-3.26.2-1
               ghostscript-9.53.3-3  giflib-5.2.1-2  git-2.31.0-1  glew-2.2.0-2
               glib-networking-2.66.0-1  glibmm-2.66.0-1  glslang-11.2.0-3  glu-9.0.1-2
               gnome-desktop-1:3.38.4-1  gnome-icon-theme-symbolic-3.12.0-6  gnome-menus-3.36.0-1
               gobject-introspection-runtime-1.66.1-3  gpm-1.20.7.r38.ge82d1a6-3  gptfdisk-1.0.7-1
               graphene-1.10.4-1  graphite-1:1.3.14-1  graphviz-2.46.1-1
               gsettings-desktop-schemas-3.38.0-1  gsfonts-20200910-1  gsm-1.0.19-1  gssdp-1.2.3-1
               gst-plugins-bad-libs-1.18.4-1  gst-plugins-base-libs-1.18.4-1  gstreamer-1.18.4-1
               gtk-engine-murrine-0.98.2-4  gtk-engines-2.21.0-5  gtk-update-icon-cache-1:4.0.3-1
               gtk2-2.24.33-1  gtk3-1:3.24.27-4  gtkhash-1.4-4  gtkmm3-3.24.4-1
               gtksourceview3-3.24.11+28+g73e57b57-1  gtkspell-2.0.16-8  gts-0.7.6.121130-2
               gupnp-1.2.4-1  gupnp-igd-1.2.0-1  gutenprint-5.3.4-1  harfbuzz-2.8.0-1
               harfbuzz-icu-2.8.0-1  hicolor-icon-theme-0.17-2  hidapi-0.10.1-1  hplip-1:3.21.2-1
               hunspell-1.7.0-3  hwloc-2.4.0-1  hyphen-2.8.8-3  ijs-0.35-3  imagemagick-7.0.11.3-1
               imlib2-1.7.1-1  iniparser-4.1-3  intltool-0.51.0-6  iso-codes-4.6.0-1
               jack-0.125.0-9  jansson-2.13.1-1  jasper-2.0.25-1  java-environment-common-3-3
               java-runtime-common-3-3  jbig2dec-0.19-1  js78-78.8.0-1  json-glib-1.6.2-1
               kauth-5.80.0-1  kcoreaddons-5.80.0-1  kitemmodels-5.80.0-1  l-smash-2.14.5-2
               lame-3.100-3  lapack-3.9.0-3  lcms2-2.12-1  ldb-1:2.2.0-3  ldns-1.7.1-2
               lensfun-0.3.95-4  lib32-bzip2-1.0.8-2  lib32-curl-7.75.0-1
               lib32-e2fsprogs-1.46.2-2  lib32-expat-2.2.10-1  lib32-gcc-libs-10.2.0-6
               lib32-glew-2.2.0-1  lib32-glibc-2.33-4  lib32-glu-9.0.1-1  lib32-icu-68.2-1
               lib32-keyutils-1.6.3-1  lib32-krb5-1.18.3-1  lib32-libdrm-2.4.104-1
               lib32-libelf-0.183-3  lib32-libffi-3.3-2  lib32-libglvnd-1.3.2-1
               lib32-libice-1.0.10-1  lib32-libidn2-2.3.0-1  lib32-libldap-2.4.57-1
               lib32-libpciaccess-0.16-1  lib32-libpsl-0.21.1-1  lib32-libsm-1.2.3-1
               lib32-libssh2-1.9.0-1  lib32-libunistring-0.9.10-1  lib32-libunwind-1.5.0-1
               lib32-libva-2.10.0-1  lib32-libvdpau-1.4-1  lib32-libx11-1.7.0-1
               lib32-libxau-1.0.9-1  lib32-libxcb-1.14-1  lib32-libxcrypt-4.4.18-1
               lib32-libxdamage-1.1.5-1  lib32-libxdmcp-1.1.3-1  lib32-libxext-1.3.4-1
               lib32-libxfixes-5.0.3-2  lib32-libxi-1.7.10-2  lib32-libxml2-2.9.10-3
               lib32-libxmu-1.1.3-1  lib32-libxshmfence-1.3-2  lib32-libxt-1.2.1-1
               lib32-libxxf86vm-1.1.4-2  lib32-llvm-libs-11.1.0-1
               lib32-lm_sensors-1:3.6.0.r41.g31d1f125-1  lib32-mesa-20.3.4-3  lib32-ncurses-6.2-1
               lib32-openssl-1:1.1.1.j-1  lib32-readline-8.1.0-2  lib32-util-linux-2.36.2-1
               lib32-wayland-1.19.0-1  lib32-xz-5.2.5-2  lib32-zlib-1.2.11-3  lib32-zstd-1.4.9-1
               libappindicator-gtk3-12.10.0.r296-1  libass-0.15.0-2  libasyncns-0.8+3+g68cd5af-3
               libatasmart-0.19-5  libavc1394-0.5.4-4  libavif-0.9.0-1  libavtp-0.1.0-2
               libblockdev-2.25-2  libbluray-1.2.1-1  libbs2b-3.1.0-7  libbsd-0.10.0-2
               libburn-1.5.4-1  libbytesize-2.5-1  libcaca-0.99.beta19-3
               libcanberra-0.30+2+gc0620e4-3  libcanberra-pulse-0.30+2+gc0620e4-3  libcddb-1.3.2-6
               libcdio-2.1.0-2  libcdio-paranoia-10.2+2.0.1-2  libcloudproviders-0.3.1-2
               libcue-2.2.1-2  libcups-1:2.3.3op2-1  libdaemon-0.14-5  libdatrie-0.2.13-1
               libdbusmenu-glib-16.04.0-4  libdbusmenu-gtk3-16.04.0-4  libdc1394-2.2.6-2
               libdca-0.0.7-1  libde265-1.0.8-1  libdv-1.0.0-9  libdvbpsi-1:1.3.3-2
               libdvdnav-6.1.0-2  libdvdread-6.1.1-1  libebml-1.4.1-1  libepoxy-1.5.5-1
               libevdev-1.11.0-1  libexif-0.6.22-1  libfdk-aac-2.0.1-2  libfontenc-1.1.4-3
               libftdi-1.5-1  libgadu-1.12.2-13  libgee-0.20.3-2  libgexiv2-0.12.2-1
               libgme-0.6.3-1  libgnomekbd-3.26.1+2+g8d02ebd-2  libgphoto2-2.5.27-1
               libgtop-2.40.0+2+g31db82ef-2  libgudev-236-1  libguess-1.2-3  libgusb-0.3.5-1
               libheif-1.11.0-1  libibus-1.5.24-1  libical-3.0.9-1  libice-1.0.10-3
               libid3tag-0.15.1b-11  libidn-1.36-1  libiec61883-1.2.0-6  libieee1284-0.2.11-11
               libimagequant-2.14.1-1  libimobiledevice-1.3.0-3  libindicator-gtk3-12.10.1-9
               libinput-1.17.0-1  libinstpatch-1.1.6-1  libisofs-1.5.4-1  libkate-0.4.1-7
               libldac-2.0.2.3-1  liblouis-3.17.0-1  liblqr-0.4.2-3  liblrdf-0.6.1-4
               libmad-0.15.1b-9  libmatroska-1.6.2-1  libmbim-1.24.6-1  libmfx-20.5.1-1
               libmicrodns-0.2.0-1  libmm-glib-1.16.2-1  libmms-0.6.4-3  libmng-2.0.3-3
               libmodplug-0.8.9.0-3  libmpcdec-1:0.1+r475-3  libmpeg2-0.5.1-7  libmtp-1.1.18-1
               libmypaint-1.5.1-2  libndp-1.7-2  libnewt-0.52.21-5  libnfs-4.0.0-4
               libnice-0.1.18-1  libnm-1.30.2-1  libnma-1.8.30-1  libofa-0.9.3-9  libogg-1.3.4-2
               libopenaptx-0.2.0-1  libpaper-1.1.28-1  libpgm-5.3.128-1  libplacebo-3.104.0-2
               libplist-2.2.0-3  libproxy-0.4.17-1  libpulse-14.2-2  libpurple-2.14.1-3
               libqmi-1.28.2-1  libqrtr-glib-1.0.0-1  libraqm-0.7.1-1  libraw-0.20.2-1
               libraw1394-2.1.2-3  librsvg-2:2.50.3-1  libsamplerate-0.2.1-1  libshout-1:2.4.5-1
               libsidplay-1.36.59-10  libsidplayfp-2.1.1-1  libsigc++-2.10.6-1  libsm-1.2.3-2
               libsndfile-1.0.31-1  libsodium-1.0.18-2  libsoup-2.72.0+5+g0b094bff-3
               libsoxr-0.1.3-2  libspiro-1:20200505-2  libsrtp-1:2.3.0-1  libssh-0.9.5-1
               libstemmer-0+713-2  libsynctex-2020.54586-11  libtar-1.2.20-5  libteam-1.31-2
               libthai-0.1.28-2  libtheora-1.1.1-5  libtool-2.4.6+42+gb88cebd5-14
               libupnp-1.14.2-1  liburing-0.7-2  libusb-compat-0.1.7-1  libusbmuxd-2.0.2-1
               libva-2.10.0-1  libvdpau-1.4-1  libvisual-0.4.0-8  libvorbis-1.3.7-2
               libvpx-1.9.0-1  libwacom-1.9-1  libwebp-1.2.0-1  libwmf-0.2.12-2  libwpe-1.8.0-1
               libxaw-1.0.13-3  libxcomposite-0.4.5-3  libxcursor-1.2.0-2  libxfce4ui-4.16.0-1
               libxfce4util-4.16.0-1  libxfont2-2.0.4-3  libxft-2.3.3-2  libxi-1.7.10-3
               libxinerama-1.1.4-3  libxkbcommon-1.1.0-2  libxkbcommon-x11-1.1.0-2
               libxkbfile-1.1.0-2  libxklavier-5.4-3  libxmu-1.1.3-2  libxpm-3.5.13-2
               libxrandr-1.5.2-3  libxrender-0.9.10-4  libxss-1.2.3-3  libxt-1.2.1-1
               libxtst-1.2.3-4  libxv-1.0.11-4  libyaml-0.2.5-1  libyuv-r2212+dfaf7534-2
               lilv-0.24.12-1  lirc-1:0.10.1-8  lmdb-0.9.29-1  lua52-5.2.4-5  luajit-2.0.5-3
               mailcap-2.1.49-1  md4c-0.4.7-1  metis-5.1.0.p10-1  mjpegtools-2.2.0beta-1
               mpg123-1.26.3-2  mtdev-1.1.6-1  mypaint-brushes1-1.3.1-1  ndctl-71.1-1
               neon-0.31.2-1  net-snmp-5.9-3  netpbm-10.73.33-1  nm-connection-editor-1.20.0-2
               oath-toolkit-2.6.6-1  openal-1.21.1-1  openconnect-1:8.10-1  opencore-amr-0.1.5-5
               openexr-2.5.5-1  openjpeg2-2.4.0-1  openmpi-4.0.5-2  openvpn-2.5.1-1  opus-1.3.1-2
               orc-0.4.32-1  ostree-2020.8-1  pamac-cli-10.0.5-1  pamac-common-10.0.5-1
               pango-1:1.48.3-1  pangomm-2.46.0-1  parted-3.4-2  pcsclite-1.9.1-1
               perl-alien-build-2.38-1  perl-alien-libxml2-0.17-1  perl-capture-tiny-0.48-4
               perl-dbi-1.643-2  perl-error-0.17029-2  perl-ffi-checklib-0.27-2
               perl-file-basedir-0.08-5  perl-file-chdir-0.1011-4  perl-file-desktopentry-0.22-7
               perl-file-which-1.23-4  perl-ipc-system-simple-1.30-2  perl-mailtools-2.21-4
               perl-parse-yapp-1.21-3  perl-path-tiny-0.118-1  perl-xml-libxml-2.0206-1
               perl-xml-namespacesupport-1.12-4  perl-xml-sax-1.02-1  perl-xml-sax-base-1.09-4
               pipewire-1:0.3.23-1  pixman-0.40.0-1  pkcs11-helper-1.27.0-1  polkit-0.118-1
               polkit-qt5-0.113.0-2  poppler-21.03.0-1  poppler-qt5-21.03.0-1
               portaudio-1:19.6.0-7  ppp-2.4.8-1  pptpclient-1.10.0-2  protobuf-3.15.6-1
               protobuf-c-1.3.3-3  pulseaudio-14.2-2  python-cachecontrol-0.12.6-3
               python-cairo-1.20.0-3  python-cffi-1.14.5-1  python-colorama-0.4.4-3
               python-contextlib2-0.6.0.post1-3  python-cryptography-3.4.6-1  python-dbus-1.2.16-3
               python-dbus-common-1.2.16-3  python-distlib-0.3.1-2  python-distro-1.5.0-3
               python-distutils-extra-2.39-8  python-dnspython-1:1.16.0-3  python-docopt-0.6.2-9
               python-gobject-3.38.0-3  python-html5lib-1.1-6  python-keyutils-0.6-6
               python-markdown-3.3.4-1  python-msgpack-1.0.2-2  python-pep517-0.9.1-3
               python-pexpect-4.8.0-3  python-pillow-8.1.0-1  python-pip-20.3.1-1
               python-ply-3.11-7  python-progress-1.5-5  python-psutil-5.8.0-1
               python-ptyprocess-0.7.0-1  python-pycparser-2.20-3  python-pyopenssl-20.0.1-1
               python-pyqt5-5.15.4-1  python-pyqt5-sip-12.8.1-3  python-pysmbc-1.0.23-2
               python-pyxdg-0.26-8  python-reportlab-3.5.65-1  python-resolvelib-0.5.4-1
               python-retrying-1.3.3-9  python-toml-0.10.2-3  python-webencodings-0.5.1-6
               python-yaml-5.4.1.1-1  qpdf-10.3.1-1  qt5-base-5.15.2-3  qt5-svg-5.15.2-1
               qt5-x11extras-5.15.2-1  raptor-2.0.15-15  rav1e-0.4.0-1  rest-0.8.1-3  rtkit-0.13-1
               rtmpdump-1:2.4.r96.fa8646d-6  sane-1.0.32-3  sbc-1.5-2  sdl2-2.0.14-1
               serd-0.30.10-1  shaderc-2020.5-1  slang-2.3.2-2  smbclient-4.14.0-1  snappy-1.1.8-2
               sord-0.16.8-1  sound-theme-freedesktop-0.8-4  soundtouch-2.2-1  spandsp-0.0.6-3
               speex-1.2.0-3  speexdsp-1.2.0-2  spirv-tools-2020.7-1  splix-2.0.0-15
               squashfs-tools-4.4-2  sratom-0.6.8-1  srt-1.4.2-1  startup-notification-0.12-7
               stoken-0.92-4  suitesparse-5.9.0-1  svt-av1-0.8.6-2  svt-hevc-1.5.0-1
               taglib-1.12-1  talloc-2.3.2-1  tbb-2020.3-1  tcl-8.6.11-1  tdb-1.4.3-4
               tevent-1:0.10.2-3  thunar-4.16.5-1  tslib-1.22-1  twolame-0.4.0-2  ufw-0.36-5
               unzip-6.0-14  usbmuxd-1.1.1-1  v4l-utils-1.20.0-1  vid.stab-1.1-3  vmaf-1.5.3-1
               volume_key-0.3.12-5  vpnc-1:0.5.3.r468.r81-2  vte-common-0.62.3-1  vte3-0.62.3-1
               wavpack-5.4.0-1  wayland-protocols-1.20-1  web-installer-url-handler-1.0.2-1
               webkit2gtk-2.30.5-1  webrtc-audio-processing-0.3.1-2  wildmidi-0.4.3-3
               woff2-1.0.2-3  wpebackend-fdo-1.8.1-1  x264-3:0.160.r3011.cde9a93-1  x265-3.4-1
               xapp-2.0.7-1  xcb-util-0.4.0-3  xcb-util-image-0.4.0-3  xcb-util-keysyms-0.4.0-3
               xcb-util-renderutil-0.3.9-3  xcb-util-wm-0.4.1-3  xdg-dbus-proxy-0.1.2-2
               xdg-desktop-portal-1.8.0-1  xdg-desktop-portal-gtk-1.8.0-1  xfconf-4.16.0-1
               xkeyboard-config-2.32-1  xmlsec-1.2.31-1  xorg-fonts-encodings-1.0.5-2
               xorg-server-common-1.20.10-3  xorg-setxkbmap-1.3.2-2  xorg-xauth-1.1-2
               xorg-xdpyinfo-1.3.2-4  xorg-xhost-1.0.8-2  xorg-xkbcomp-1.4.5-1
               xorg-xmodmap-1.0.10-2  xorg-xprop-1.2.5-1  xorg-xrdb-1.2.0-2  xorg-xset-1.2.4-2
               xvidcore-1.3.7-2  yelp-xsl-3.38.3-1  zbar-0.23.1-6  zeromq-4.3.3-2  zimg-3.0.1-2
               zip-3.0-9  zita-alsa-pcmi-0.3.2-3  zita-resampler-1.8.0-1  zvbi-0.2.35-4
               zxing-cpp-1.1.1-1  accountsservice-0.6.55-3  adobe-source-han-sans-cn-fonts-2.002-1
               adobe-source-han-sans-jp-fonts-2.002-1  adobe-source-han-sans-kr-fonts-2.002-1
               adobe-source-sans-pro-fonts-3.028-1  alsa-firmware-1.2.4-2  alsa-utils-1.2.4-2
               android-tools-30.0.5-3  android-udev-20210302-1  apparmor-3.0.1-1  audacious-4.1-2
               avahi-0.8+15+ge8a3dd0-3  blueman-2.1.4-1  breath2-icon-themes-1.0.11-1
               capitaine-cursors-4-1  catfish-4.16.0-3  dmidecode-3.3-1  engrampa-1.24.1-1
               ffmpeg-2:4.3.2-2  ffmpegthumbnailer-2.2.2-2  firefox-87.0-0.1  freetype2-2.10.4-1
               galculator-gtk2-2.1.4-5  gcolor2-0.4-9  gimp-2.10.22-2  gnome-icon-theme-3.12.0-6
               gnome-keyring-1:3.36.0-3  gnome-themes-extra-3.28-2  gparted-1.2.0-2
               grub-theme-manjaro-20.2-12  gst-libav-1.18.4-1  gst-plugins-bad-1.18.4-1
               gst-plugins-base-1.18.4-1  gst-plugins-good-1.18.4-1  gst-plugins-ugly-1.18.4-1
               gtk-theme-breath-5.9.0-1  gtkhash-thunar-1.4-4  gufw-21.04.0-1  gvfs-1.46.2-1
               gvfs-afc-1.46.2-1  gvfs-gphoto2-1.46.2-1  gvfs-mtp-1.46.2-1  gvfs-nfs-1.46.2-1
               gvfs-smb-1.46.2-1  htop-3.0.5-1  inxi-3.3.03.1-1  jdk8-openjdk-8.u282-1
               jre8-openjdk-8.u282-1  jre8-openjdk-headless-8.u282-1  kernel-alive-0.5-1
               lib32-libva-intel-driver-2.4.1-1  lib32-libva-mesa-driver-20.3.4-3
               lib32-libva-vdpau-driver-0.7.4-6  lib32-mesa-demos-8.4.0-2  libdvdcss-1.4.2-2
               libgsf-1.14.47-1  libopenraw-0.3.0-1  libva-intel-driver-2.4.1-1
               libva-mesa-driver-20.3.4-3  libva-vdpau-driver-0.7.4-5  light-locker-1.9.0-4
               lightdm-1:1.30.0-4  lightdm-gtk-greeter-1:2.0.8-1
               lightdm-gtk-greeter-settings-1.2.2-5  manjaro-application-utility-1.3.3-4.1
               manjaro-documentation-en-20181009-1  manjaro-hello-0.6.6-5
               manjaro-hotfixes-2018.08-6  manjaro-icons-20191015-1  manjaro-printer-20200215-2
               manjaro-settings-manager-0.5.6-11  manjaro-settings-manager-notifier-0.5.6-11
               matcha-gtk-theme-20210204-1  menulibre-2.2.1-3  mesa-demos-8.4.0-4
               mlocate-0.26.git.20170220-6  mobile-broadband-provider-info-20201225-1
               modemmanager-1.16.2-1  mousepad-0.5.3-1  mtpfs-1.1-4  mugshot-0.4.3-1
               network-manager-applet-1.20.0-2  networkmanager-1.30.2-1
               networkmanager-openconnect-1.2.7dev+49+gc512d5a-1  networkmanager-openvpn-1.8.12-1
               networkmanager-pptp-1.2.9dev+10+gb41b0d0-3
               networkmanager-vpnc-1.2.7dev+20+gdca3aea-2  noto-fonts-20201226-1
               noto-fonts-cjk-20201206-1  noto-fonts-emoji-20200916-1  nss-mdns-0.14.1-3
               ntp-4.2.8.p15-1  numlockx-1.2-5  openresolv-3.12.0-1  openssh-8.5p1-1
               p7zip-17.03-2  pamac-flatpak-plugin-10.0.5-1  pamac-gtk-10.0.5-1
               pamac-snap-plugin-10.0.5-1  pavucontrol-1:4.0-2  perl-file-mimeinfo-0.30-1
               pidgin-2.14.1-3  poppler-data-0.4.10-1  poppler-glib-21.03.0-1  powertop-2.13-1
               pulseaudio-bluetooth-14.2-2  pulseaudio-ctl-1.67-1  pulseaudio-zeroconf-14.2-2
               qpdfview-0.4.18-2  samba-4.14.0-1  screenfetch-3.9.1-1  snapd-2.49.1-1
               snapd-glib-1.54-1  terminus-font-4.49.1-1  thunderbird-78.9.0-0.1
               timeshift-20.11.1.r3.g08d0e59-2  ttf-dejavu-2.37+18+g9b5d1b2f-2
               ttf-droid-20121017-9  ttf-inconsolata-1:3.000-2  ttf-indic-otf-0.2-11
               ttf-liberation-2.1.3-1  tty-solitaire-1.3.0-1  udiskie-2.3.3-1  udisks2-2.9.2-1
               unace-2.5-11  unrar-1:6.0.4-1  usb_modeswitch-2.6.1-1  viewnior-1.7-3  vlc-3.0.12-2
               xcursor-maia-20160417-2  xcursor-simpleandsoft-0.2-9
               xcursor-vanilla-dmz-aa-0.4.5-2  xdg-user-dirs-0.17-3  xdg-utils-1.1.3+19+g9816ebb-1
               xf86-input-elographics-1.4.2-2  xf86-input-evdev-2.10.6-2
               xf86-input-libinput-0.30.0-1  xf86-input-void-1.4.1-5  xfburn-0.6.2-1
               xiccd-0.3.0-1  xorg-mkfontscale-1.2.1-2  xorg-server-1.20.10-3  xorg-twm-1.0.11-1
               xorg-xinit-1.4.1-3  xorg-xkill-1.0.5-2  yelp-3.38.3-1  zensu-0.3-1

Total Installed Size:  3864.54 MiB

:: Proceed with installation? [Y/n]
(695/695) checking keys in keyring                          [################################] 100%
(695/695) checking package integrity                        [################################] 100%
(695/695) loading package files                             [################################] 100%
(695/695) checking for file conflicts                       [################################] 100%
:: Processing package changes...
(  1/695) installing libdaemon                              [################################] 100%
(  2/695) installing avahi                                  [################################] 100%
Optional dependencies for avahi
    gtk3: avahi-discover, avahi-discover-standalone, bshell, bssh, bvnc [pending]
    qt5-base: qt5 bindings [pending]
    libevent: libevent bindings [installed]
    nss-mdns: NSS support for mDNS [pending]
    python-twisted: avahi-bookmarks
    python-gobject: avahi-bookmarks, avahi-discover [pending]
    python-dbus: avahi-bookmarks, avahi-discover [pending]
(  3/695) installing jansson                                [################################] 100%
(  4/695) installing libnm                                  [################################] 100%
(  5/695) installing js78                                   [################################] 100%
(  6/695) installing polkit                                 [################################] 100%
(  7/695) installing libmm-glib                             [################################] 100%
(  8/695) installing gpm                                    [################################] 100%
(  9/695) installing slang                                  [################################] 100%
( 10/695) installing libnewt                                [################################] 100%
Optional dependencies for libnewt
    tcl: whiptcl support [pending]
    python: libnewt support with the _snack module [installed]
    python2: libnewt support with the _snack module
( 11/695) installing libndp                                 [################################] 100%
( 12/695) installing libsodium                              [################################] 100%
( 13/695) installing libpgm                                 [################################] 100%
( 14/695) installing zeromq                                 [################################] 100%
( 15/695) installing libteam                                [################################] 100%
( 16/695) installing bluez-libs                             [################################] 100%
( 17/695) installing mobile-broadband-provider-info         [################################] 100%
( 18/695) installing networkmanager                         [################################] 100%
Optional dependencies for networkmanager
    dnsmasq: connection sharing [installed]
    bluez: Bluetooth support [pending]
    ppp: dialup connection support [pending]
    modemmanager: cellular network support [pending]
    iwd: wpa_supplicant alternative
    dhclient: alternative DHCP client [installed]
    openresolv: alternative resolv.conf manager [pending]
    firewalld: Firewall support
( 19/695) installing libproxy                               [################################] 100%
Optional dependencies for libproxy
    networkmanager: NetworkManager configuration module [installed]
    perl: Perl bindings [installed]
    python2: Python 2.x bindings
    python: Python 3.x bindings [installed]
    libproxy-webkit: PAC proxy support (via WebKit)
( 20/695) installing vpnc                                   [################################] 100%
Optional dependencies for vpnc
    openresolv: Let vpnc manage resolv.conf [pending]
( 21/695) installing pcsclite                               [################################] 100%
( 22/695) installing stoken                                 [################################] 100%
Optional dependencies for stoken
    gtk3: required for stoken-gui [pending]
( 23/695) installing openconnect                            [################################] 100%
Optional dependencies for openconnect
    python: tncc-wrapper [installed]
( 24/695) installing networkmanager-openconnect             [################################] 100%
Optional dependencies for networkmanager-openconnect
    libnma: GUI support [pending]
( 25/695) installing pkcs11-helper                          [################################] 100%
( 26/695) installing openvpn                                [################################] 100%
Optional dependencies for openvpn
    easy-rsa: easy CA and certificate handling
    pam: authenticate via PAM [installed]
( 27/695) installing networkmanager-openvpn                 [################################] 100%
Optional dependencies for networkmanager-openvpn
    libnma: GUI support [pending]
( 28/695) installing ppp                                    [################################] 100%
( 29/695) installing pptpclient                             [################################] 100%
( 30/695) installing networkmanager-pptp                    [################################] 100%
Optional dependencies for networkmanager-pptp
    libnma: GUI support [pending]
( 31/695) installing networkmanager-vpnc                    [################################] 100%
Optional dependencies for networkmanager-vpnc
    libnma: GUI support [pending]
( 32/695) installing nss-mdns                               [################################] 100%
( 33/695) installing ntp                                    [################################] 100%
( 34/695) installing libgudev                               [################################] 100%
( 35/695) installing libmbim                                [################################] 100%
( 36/695) installing libqrtr-glib                           [################################] 100%
( 37/695) installing libqmi                                 [################################] 100%
( 38/695) installing modemmanager                           [################################] 100%
Optional dependencies for modemmanager
    usb_modeswitch: install if your modem shows up as a storage drive [pending]
( 39/695) installing openresolv                             [################################] 100%
( 40/695) installing dnssec-anchors                         [################################] 100%
( 41/695) installing ldns                                   [################################] 100%
Optional dependencies for ldns
    libpcap: ldns-dpa tool [installed]
( 42/695) installing openssh                                [################################] 100%
Optional dependencies for openssh
    xorg-xauth: X11 forwarding [pending]
    x11-ssh-askpass: input passphrase in X
    libfido2: FIDO/U2F support
( 43/695) installing libcups                                [################################] 100%
( 44/695) installing talloc                                 [################################] 100%
Optional dependencies for talloc
    python: for python bindings [installed]
( 45/695) installing tevent                                 [################################] 100%
Optional dependencies for tevent
    python: for python bindings [installed]
( 46/695) installing tdb                                    [################################] 100%
Optional dependencies for tdb
    python: for python bindings [installed]
( 47/695) installing lmdb                                   [################################] 100%
( 48/695) installing ldb                                    [################################] 100%
Optional dependencies for ldb
    python: for python bindings [installed]
( 49/695) installing libbsd                                 [################################] 100%
( 50/695) installing iniparser                              [################################] 100%
( 51/695) installing perl-parse-yapp                        [################################] 100%
( 52/695) installing cifs-utils                             [################################] 100%
( 53/695) installing python-markdown                        [################################] 100%
( 54/695) installing python-dnspython                       [################################] 100%
Optional dependencies for python-dnspython
    python-ecdsa: DNSSEC support
    python-pycryptodome: DNSSEC support
    python-idna: support for updated IDNA 2008 [installed]
( 55/695) installing smbclient                              [################################] 100%
Optional dependencies for smbclient
    python-dnspython: samba_dnsupdate and samba_upgradedns in AD setup [installed]
( 56/695) installing boost-libs                             [################################] 100%
Optional dependencies for boost-libs
    openmpi: for mpi support [pending]
( 57/695) installing libtool                                [################################] 100%
( 58/695) installing xmlsec                                 [################################] 100%
( 59/695) installing oath-toolkit                           [################################] 100%
( 60/695) installing snappy                                 [################################] 100%
( 61/695) installing fmt                                    [################################] 100%
( 62/695) installing ceph-libs                              [################################] 100%
( 63/695) installing liburing                               [################################] 100%
( 64/695) installing samba                                  [################################] 100%
Optional dependencies for samba
    python-dnspython: samba_dnsupdate and samba_upgradedns in AD setup [installed]
( 65/695) installing tcl                                    [################################] 100%
( 66/695) installing usb_modeswitch                         [################################] 100%
( 67/695) installing alsa-firmware                          [################################] 100%
( 68/695) installing alsa-topology-conf                     [################################] 100%
( 69/695) installing alsa-ucm-conf                          [################################] 100%
( 70/695) installing alsa-lib                               [################################] 100%
( 71/695) installing libsamplerate                          [################################] 100%
( 72/695) installing alsa-utils                             [################################] 100%
Optional dependencies for alsa-utils
    fftw: for alsabat [pending]
( 73/695) installing aom                                    [################################] 100%
( 74/695) installing graphite                               [################################] 100%
( 75/695) installing harfbuzz                               [################################] 100%
Optional dependencies for harfbuzz
    cairo: hb-view program [pending]
( 76/695) installing freetype2                              [################################] 100%
( 77/695) installing fontconfig                             [################################] 100%
Rebuilding fontconfig cache... done.
( 78/695) installing fribidi                                [################################] 100%
( 79/695) installing gsm                                    [################################] 100%
( 80/695) installing opus                                   [################################] 100%
( 81/695) installing libogg                                 [################################] 100%
( 82/695) installing speexdsp                               [################################] 100%
( 83/695) installing speex                                  [################################] 100%
( 84/695) installing flac                                   [################################] 100%
( 85/695) installing libvorbis                              [################################] 100%
( 86/695) installing libsndfile                             [################################] 100%
Optional dependencies for libsndfile
    alsa-lib: for sndfile-play [installed]
( 87/695) installing zita-alsa-pcmi                         [################################] 100%
( 88/695) installing zita-resampler                         [################################] 100%
Optional dependencies for zita-resampler
    libsndfile: for zresample and zretune [installed]
( 89/695) installing jack                                   [################################] 100%
Optional dependencies for jack
    celt: NetJACK driver
    libffado: FireWire support
    realtime-privileges: Acquire realtime privileges
( 90/695) installing lame                                   [################################] 100%
( 91/695) installing libass                                 [################################] 100%
( 92/695) installing libraw1394                             [################################] 100%
( 93/695) installing libavc1394                             [################################] 100%
( 94/695) installing libbluray                              [################################] 100%
Optional dependencies for libbluray
    java-runtime: BD-J library [pending]
( 95/695) installing dav1d                                  [################################] 100%
Optional dependencies for dav1d
    dav1d-doc: HTML documentation
( 96/695) installing libiec61883                            [################################] 100%
( 97/695) installing libmfx                                 [################################] 100%
( 98/695) installing libmodplug                             [################################] 100%
( 99/695) installing libasyncns                             [################################] 100%
(100/695) installing libice                                 [################################] 100%
(101/695) installing libsm                                  [################################] 100%
(102/695) installing libpulse                               [################################] 100%
Optional dependencies for libpulse
    glib2: mainloop integration [installed]
(103/695) installing rav1e                                  [################################] 100%
(104/695) installing libsoxr                                [################################] 100%
(105/695) installing libssh                                 [################################] 100%
(106/695) installing libtheora                              [################################] 100%
(107/695) installing libva                                  [################################] 100%
Optional dependencies for libva
    intel-media-driver: backend for Intel GPUs (>= Broadwell)
    libva-vdpau-driver: backend for Nvidia and AMD GPUs [pending]
    libva-intel-driver: backend for Intel GPUs (<= Haswell) [pending]
(108/695) installing libvdpau                               [################################] 100%
(109/695) installing vid.stab                               [################################] 100%
(110/695) installing libvpx                                 [################################] 100%
(111/695) installing giflib                                 [################################] 100%
(112/695) installing libwebp                                [################################] 100%
Optional dependencies for libwebp
    freeglut: vwebp viewer [pending]
(113/695) installing l-smash                                [################################] 100%
(114/695) installing x264                                   [################################] 100%
(115/695) installing x265                                   [################################] 100%
(116/695) installing libxv                                  [################################] 100%
(117/695) installing xvidcore                               [################################] 100%
(118/695) installing zimg                                   [################################] 100%
(119/695) installing opencore-amr                           [################################] 100%
(120/695) installing lcms2                                  [################################] 100%
(121/695) installing openjpeg2                              [################################] 100%
(122/695) installing libxrender                             [################################] 100%
(123/695) installing libxcursor                             [################################] 100%
Optional dependencies for libxcursor
    gnome-themes-standard: fallback icon theme [pending]
(124/695) installing libibus                                [################################] 100%
(125/695) installing hidapi                                 [################################] 100%
Optional dependencies for hidapi
    libusb: for the libusb backend -- hidapi-libusb.so [installed]
    libudev.so: for the hidraw backend -- hidapi-hidraw.so [installed]
(126/695) installing sdl2                                   [################################] 100%
Optional dependencies for sdl2
    alsa-lib: ALSA audio driver [installed]
    libpulse: PulseAudio audio driver [installed]
    jack: JACK audio driver [installed]
(127/695) installing srt                                    [################################] 100%
(128/695) installing hicolor-icon-theme                     [################################] 100%
(129/695) installing v4l-utils                              [################################] 100%
Optional dependencies for v4l-utils
    qt5-base: for qv4l2 [pending]
    alsa-lib: for qv4l2 [installed]
(130/695) installing vmaf                                   [################################] 100%
(131/695) installing ffmpeg                                 [################################] 100%
Optional dependencies for ffmpeg
    avisynthplus: AviSynthPlus support
    intel-media-sdk: Intel QuickSync support
    ladspa: LADSPA filters
    nvidia-utils: Nvidia NVDEC/NVENC support
(132/695) installing gstreamer                              [################################] 100%
(133/695) installing orc                                    [################################] 100%
(134/695) installing iso-codes                              [################################] 100%
(135/695) installing gst-plugins-base-libs                  [################################] 100%
(136/695) installing gst-libav                              [################################] 100%
(137/695) installing xkeyboard-config                       [################################] 100%
(138/695) installing libxkbcommon                           [################################] 100%
Optional dependencies for libxkbcommon
    libxkbcommon-x11: xkbcli interactive-x11 [pending]
    wayland: xkbcli interactive-wayland [installed]
(139/695) installing libxkbcommon-x11                       [################################] 100%
(140/695) installing gst-plugins-bad-libs                   [################################] 100%
(141/695) installing dconf                                  [################################] 100%
(142/695) installing cantarell-fonts                        [################################] 100%
(143/695) installing adobe-source-code-pro-fonts            [################################] 100%
(144/695) installing gsettings-desktop-schemas              [################################] 100%
(145/695) installing glib-networking                        [################################] 100%
(146/695) installing brotli                                 [################################] 100%
(147/695) installing libsoup                                [################################] 100%
Optional dependencies for libsoup
    samba: Windows Domain SSO [installed]
(148/695) installing wavpack                                [################################] 100%
(149/695) installing aalib                                  [################################] 100%
(150/695) installing taglib                                 [################################] 100%
(151/695) installing libdv                                  [################################] 100%
(152/695) installing libshout                               [################################] 100%
(153/695) installing libid3tag                              [################################] 100%
(154/695) installing imlib2                                 [################################] 100%
(155/695) installing libcaca                                [################################] 100%
(156/695) installing pixman                                 [################################] 100%
(157/695) installing cairo                                  [################################] 100%
(158/695) installing mpg123                                 [################################] 100%
Optional dependencies for mpg123
    sdl: for sdl audio support
    jack: for jack audio support [installed]
    libpulse: for pulse audio support [installed]
    perl: for conplay [installed]
(159/695) installing twolame                                [################################] 100%
(160/695) installing gst-plugins-good                       [################################] 100%
(161/695) installing libbs2b                                [################################] 100%
(162/695) installing chromaprint                            [################################] 100%
(163/695) installing libdatrie                              [################################] 100%
(164/695) installing libthai                                [################################] 100%
(165/695) installing libxft                                 [################################] 100%
(166/695) installing pango                                  [################################] 100%
(167/695) installing libdc1394                              [################################] 100%
(168/695) installing libde265                               [################################] 100%
Optional dependencies for libde265
    ffmpeg: for sherlock265 [installed]
    qt5-base: for sherlock265 [pending]
    sdl: dec265 YUV overlay output
(169/695) installing libdca                                 [################################] 100%
(170/695) installing faac                                   [################################] 100%
(171/695) installing faad2                                  [################################] 100%
(172/695) installing libfdk-aac                             [################################] 100%
(173/695) installing libinstpatch                           [################################] 100%
(174/695) installing portaudio                              [################################] 100%
(175/695) installing fluidsynth                             [################################] 100%
(176/695) installing libgme                                 [################################] 100%
(177/695) installing libkate                                [################################] 100%
(178/695) installing raptor                                 [################################] 100%
(179/695) installing liblrdf                                [################################] 100%
(180/695) installing serd                                   [################################] 100%
(181/695) installing sord                                   [################################] 100%
(182/695) installing sratom                                 [################################] 100%
(183/695) installing lilv                                   [################################] 100%
Optional dependencies for lilv
    bash-completion: completion for bash [installed]
    libsndfile: for lv2apply [installed]
    python: for Python bindings [installed]
(184/695) installing libmms                                 [################################] 100%
(185/695) installing mjpegtools                             [################################] 100%
(186/695) installing libmpcdec                              [################################] 100%
(187/695) installing neon                                   [################################] 100%
(188/695) installing hwloc                                  [################################] 100%
Optional dependencies for hwloc
    cairo [installed]
    libxml2 [installed]
    pciutils [installed]
    libx11 [installed]
(189/695) installing openmpi                                [################################] 100%
Optional dependencies for openmpi
    gcc-fortran: fortran support
(190/695) installing fftw                                   [################################] 100%
(191/695) installing libofa                                 [################################] 100%
(192/695) installing openal                                 [################################] 100%
Optional dependencies for openal
    qt5-base: alsoft-config GUI Configurator [pending]
    fluidsynth: MIDI rendering [installed]
    libmysofa: makemhr tool
(193/695) installing openexr                                [################################] 100%
Optional dependencies for openexr
    boost-libs: python support [installed]
    python: python support [installed]
(194/695) installing libdvdread                             [################################] 100%
Optional dependencies for libdvdread
    libdvdcss: for decoding encrypted DVDs [pending]
(195/695) installing libdvdnav                              [################################] 100%
(196/695) installing librsvg                                [################################] 100%
(197/695) installing rtmpdump                               [################################] 100%
(198/695) installing sbc                                    [################################] 100%
(199/695) installing soundtouch                             [################################] 100%
(200/695) installing spandsp                                [################################] 100%
(201/695) installing libsrtp                                [################################] 100%
(202/695) installing zvbi                                   [################################] 100%
(203/695) installing gssdp                                  [################################] 100%
Optional dependencies for gssdp
    gtk3: gssdp-device-sniffer [pending]
(204/695) installing gupnp                                  [################################] 100%
Optional dependencies for gupnp
    python: gupnp-binding-tool [installed]
(205/695) installing gupnp-igd                              [################################] 100%
(206/695) installing libnice                                [################################] 100%
Optional dependencies for libnice
    gstreamer: "nice" GStreamer plugin [installed]
(207/695) installing webrtc-audio-processing                [################################] 100%
(208/695) installing wildmidi                               [################################] 100%
(209/695) installing liblqr                                 [################################] 100%
(210/695) installing libraqm                                [################################] 100%
(211/695) installing imagemagick                            [################################] 100%
Optional dependencies for imagemagick
    ghostscript: PS/PDF support [pending]
    libheif: HEIF support [pending]
    libraw: DNG support [pending]
    librsvg: SVG support [installed]
    libwebp: WEBP support [installed]
    libwmf: WMF support [pending]
    libxml2: Magick Scripting Language [installed]
    ocl-icd: OpenCL support
    openexr: OpenEXR support [installed]
    openjpeg2: JPEG2000 support [installed]
    djvulibre: DJVU support
    pango: Text rendering [installed]
    imagemagick-doc: manual and API docs
(212/695) installing zbar                                   [################################] 100%
Optional dependencies for zbar
    gtk3: for zbar-gtk [pending]
    qt5-x11extras: for zbar-qt [pending]
    python: for zbar python bindings [installed]
(213/695) installing libavtp                                [################################] 100%
(214/695) installing svt-hevc                               [################################] 100%
(215/695) installing libmicrodns                            [################################] 100%
(216/695) installing zxing-cpp                              [################################] 100%
(217/695) installing gst-plugins-bad                        [################################] 100%
Optional dependencies for gst-plugins-bad
    nvidia-utils: nvcodec plugin
(218/695) installing cdparanoia                             [################################] 100%
(219/695) installing libvisual                              [################################] 100%
(220/695) installing graphene                               [################################] 100%
(221/695) installing gst-plugins-base                       [################################] 100%
(222/695) installing libmpeg2                               [################################] 100%
Optional dependencies for libmpeg2
    sdl: required for mpeg2dec
    libxv: required for mpeg2dec [installed]
(223/695) installing a52dec                                 [################################] 100%
(224/695) installing libsidplay                             [################################] 100%
(225/695) installing libcddb                                [################################] 100%
(226/695) installing libcdio                                [################################] 100%
(227/695) installing gst-plugins-ugly                       [################################] 100%
(228/695) installing libdvdcss                              [################################] 100%
(229/695) installing rtkit                                  [################################] 100%
(230/695) installing libxi                                  [################################] 100%
(231/695) installing libxtst                                [################################] 100%
(232/695) installing pulseaudio                             [################################] 100%
Created symlink /etc/systemd/user/sockets.target.wants/pulseaudio.socket → /usr/lib/systemd/user/pulseaudio.socket.
Optional dependencies for pulseaudio
    pulseaudio-alsa: ALSA configuration (recommended)
    pulseaudio-zeroconf: Zeroconf support [pending]
    pulseaudio-lirc: IR (lirc) support
    pulseaudio-jack: Jack support
    pulseaudio-bluetooth: Bluetooth support [pending]
    pulseaudio-equalizer: Graphical equalizer
    pulseaudio-rtp: RTP and RAOP support
(233/695) installing libical                                [################################] 100%
(234/695) installing bluez                                  [################################] 100%
(235/695) installing pulseaudio-bluetooth                   [################################] 100%
(236/695) installing bc                                     [################################] 100%
(237/695) installing pulseaudio-ctl                         [################################] 100%
--------------------------------------------------------------------------
                           INSTRUCTIONS FOR USE

Simply map the following scripts to keyboard shortcuts in your DE or WM.
Xfce4 allows for this under Settings > Keyboard > Application Shortcuts.

/usr/bin/pulseaudio-ctl mute ==>  Toggle status of mute
/usr/bin/pulseaudio-ctl mute-input ==>  Toggle status of mute for mic
/usr/bin/pulseaudio-ctl up   ==>  Increase vol by 5 %
/usr/bin/pulseaudio-ctl down ==>  Decrease vol by 5 %
--------------------------------------------------------------------------
Optional dependencies for pulseaudio-ctl
    libnotify: to display volume and mute status [installed]
(238/695) installing pulseaudio-zeroconf                    [################################] 100%
(239/695) installing protobuf                               [################################] 100%
(240/695) installing android-tools                          [################################] 100%
Optional dependencies for android-tools
    python: for mkbootimg & avbtool scripts [installed]
    python2: for unpack_bootimg script
(241/695) installing libmtp                                 [################################] 100%
(242/695) installing android-udev                           [################################] 100%
(243/695) installing fuse3                                  [################################] 100%
(244/695) installing libcdio-paranoia                       [################################] 100%
(245/695) installing libatasmart                            [################################] 100%
(246/695) installing gptfdisk                               [################################] 100%
(247/695) installing libbytesize                            [################################] 100%
Optional dependencies for libbytesize
    python: for bscalc command [installed]
(248/695) installing ndctl                                  [################################] 100%
(249/695) installing parted                                 [################################] 100%
(250/695) installing volume_key                             [################################] 100%
Optional dependencies for volume_key
    python: for python bindings [installed]
(251/695) installing libyaml                                [################################] 100%
(252/695) installing libblockdev                            [################################] 100%
(253/695) installing udisks2                                [################################] 100%
Optional dependencies for udisks2
    gptfdisk: GUID partition table support [installed]
    ntfs-3g: NTFS filesystem management support [installed]
    dosfstools: VFAT filesystem management support [installed]
(254/695) installing atk                                    [################################] 100%
(255/695) installing libxinerama                            [################################] 100%
(256/695) installing libxrandr                              [################################] 100%
(257/695) installing libepoxy                               [################################] 100%
(258/695) installing libxcomposite                          [################################] 100%
(259/695) installing at-spi2-core                           [################################] 100%
Optional dependencies for at-spi2-core
    dbus-broker: Alternative bus implementation
(260/695) installing at-spi2-atk                            [################################] 100%
(261/695) installing gtk-update-icon-cache                  [################################] 100%
(262/695) installing adwaita-icon-theme                     [################################] 100%
(263/695) installing json-glib                              [################################] 100%
(264/695) installing wayland-protocols                      [################################] 100%
(265/695) installing desktop-file-utils                     [################################] 100%
(266/695) installing libgusb                                [################################] 100%
(267/695) installing colord                                 [################################] 100%
Optional dependencies for colord
    argyllcms: color profiling
    colord-sane: SANE support [pending]
(268/695) installing rest                                   [################################] 100%
(269/695) installing sound-theme-freedesktop                [################################] 100%
(270/695) installing libcanberra                            [################################] 100%
Optional dependencies for libcanberra
    libcanberra-pulse: PulseAudio driver [pending]
    libcanberra-gstreamer: GStreamer driver
(271/695) installing libcloudproviders                      [################################] 100%
(272/695) installing gtk3                                   [################################] 100%
(273/695) installing gcr                                    [################################] 100%
(274/695) installing gvfs                                   [################################] 100%
Optional dependencies for gvfs
    gvfs-afc: AFC (mobile devices) support [pending]
    gvfs-smb: SMB/CIFS (Windows client) support [pending]
    gvfs-gphoto2: gphoto2 (PTP camera/MTP media player) support [pending]
    gvfs-mtp: MTP device support [pending]
    gvfs-goa: gnome-online-accounts (e.g. OwnCloud) support
    gvfs-nfs: NFS support [pending]
    gvfs-google: Google Drive support
    gtk3: Recent files support [installed]
(275/695) installing libplist                               [################################] 100%
(276/695) installing libusbmuxd                             [################################] 100%
(277/695) installing usbmuxd                                [################################] 100%
(278/695) installing libimobiledevice                       [################################] 100%
(279/695) installing gvfs-afc                               [################################] 100%
(280/695) installing libexif                                [################################] 100%
(281/695) installing libxt                                  [################################] 100%
(282/695) installing libxpm                                 [################################] 100%
(283/695) installing svt-av1                                [################################] 100%
(284/695) installing libyuv                                 [################################] 100%
(285/695) installing libavif                                [################################] 100%
(286/695) installing libheif                                [################################] 100%
Optional dependencies for libheif
    libjpeg: for heif-convert and heif-enc [installed]
    libpng: for heif-convert and heif-enc [installed]
(287/695) installing gd                                     [################################] 100%
Optional dependencies for gd
    perl: bdftogd script [installed]
(288/695) installing libgphoto2                             [################################] 100%
(289/695) installing gvfs-gphoto2                           [################################] 100%
(290/695) installing gvfs-mtp                               [################################] 100%
(291/695) installing libnfs                                 [################################] 100%
(292/695) installing gvfs-nfs                               [################################] 100%
(293/695) installing gvfs-smb                               [################################] 100%
(294/695) installing libmad                                 [################################] 100%
(295/695) installing mtpfs                                  [################################] 100%
(296/695) installing gobject-introspection-runtime          [################################] 100%
(297/695) installing python-gobject                         [################################] 100%
Optional dependencies for python-gobject
    cairo: Cairo bindings [installed]
(298/695) installing python-yaml                            [################################] 100%
(299/695) installing python-docopt                          [################################] 100%
(300/695) installing python-keyutils                        [################################] 100%
(301/695) installing udiskie                                [################################] 100%
Optional dependencies for udiskie
    libappindicator-gtk3: --appindicator support [pending]
(302/695) installing ttf-dejavu                             [################################] 100%
(303/695) installing noto-fonts                             [################################] 100%
Optional dependencies for noto-fonts
    noto-fonts-cjk: CJK characters [pending]
    noto-fonts-emoji: Emoji characters [pending]
    noto-fonts-extra: additional variants (condensed, semi-bold, extra-light)
(304/695) installing noto-fonts-cjk                         [################################] 100%
(305/695) installing adobe-source-sans-pro-fonts            [################################] 100%
(306/695) installing adobe-source-han-sans-jp-fonts         [################################] 100%
(307/695) installing adobe-source-han-sans-kr-fonts         [################################] 100%
(308/695) installing adobe-source-han-sans-cn-fonts         [################################] 100%
(309/695) installing noto-fonts-emoji                       [################################] 100%
(310/695) installing terminus-font                          [################################] 100%
(311/695) installing ttf-inconsolata                        [################################] 100%
(312/695) installing ttf-indic-otf                          [################################] 100%
(313/695) installing ttf-liberation                         [################################] 100%
(314/695) installing ttf-droid                              [################################] 100%
(315/695) installing tty-solitaire                          [################################] 100%
(316/695) installing apparmor                               [################################] 100%
Optional dependencies for apparmor
    perl: for perl bindings [installed]
    python-notify2: for aa-notify
    python-psutil: for aa-notify [pending]
    ruby: for ruby bindings
(317/695) installing dbus-glib                              [################################] 100%
(318/695) installing vte-common                             [################################] 100%
(319/695) installing vte3                                   [################################] 100%
(320/695) installing gcab                                   [################################] 100%
(321/695) installing libstemmer                             [################################] 100%
(322/695) installing appstream-glib                         [################################] 100%
(323/695) installing archlinux-appstream-data               [################################] 100%
(324/695) installing perl-error                             [################################] 100%
(325/695) installing perl-mailtools                         [################################] 100%
(326/695) installing git                                    [################################] 100%
Optional dependencies for git
    tk: gitk and git gui
    perl-libwww: git svn [installed]
    perl-term-readkey: git svn and interactive.singlekey setting
    perl-mime-tools: git send-email
    perl-net-smtp-ssl: git send-email TLS support
    perl-authen-sasl: git send-email TLS support
    perl-mediawiki-api: git mediawiki support
    perl-datetime-format-iso8601: git mediawiki support
    perl-lwp-protocol-https: git mediawiki https support
    perl-cgi: gitweb (web interface) support
    python: git svn & git p4 [installed]
    subversion: git svn
    org.freedesktop.secrets: keyring credential helper [pending]
    libsecret: libsecret credential helper [installed]
(327/695) installing pamac-common                           [################################] 100%
==> An authentication agent is required
    Cinnamon, Deepin, GNOME, GNOME Flashback, KDE, LXDE, LXQt, MATE and Xfce
    have an authentication agent already.
    See https://wiki.archlinux.org/index.php/Polkit#Authentication_agents
    for other desktop environments.
Optional dependencies for pamac-common
    pamac-snap-plugin [pending]
    pamac-flatpak-plugin [pending]
(328/695) installing pamac-cli                              [################################] 100%
(329/695) installing pamac-gtk                              [################################] 100%
==> Enable Pamac Updates Indicator via
    Gnome Tweak
Optional dependencies for pamac-gtk
    pamac-gnome-integration
(330/695) installing squashfs-tools                         [################################] 100%
(331/695) installing snapd                                  [################################] 100%
Optional dependencies for snapd
    bash-completion: bash completion support [installed]
    xdg-desktop-portal: desktop integration [pending]
(332/695) installing snapd-glib                             [################################] 100%
(333/695) installing pamac-snap-plugin                      [################################] 100%
(334/695) installing ostree                                 [################################] 100%
(335/695) installing bubblewrap                             [################################] 100%
(336/695) installing xdg-dbus-proxy                         [################################] 100%
(337/695) installing alsa-card-profiles                     [################################] 100%
(338/695) installing libldac                                [################################] 100%
(339/695) installing libopenaptx                            [################################] 100%
(340/695) installing pipewire                               [################################] 100%
Created symlink /etc/systemd/user/sockets.target.wants/pipewire.socket → /usr/lib/systemd/user/pipewire.socket.
Optional dependencies for pipewire
    pipewire-docs: Documentation
    pipewire-media-session: Default session manager
    pipewire-alsa: ALSA configuration
    pipewire-jack: JACK support
    pipewire-pulse: PulseAudio replacement
    gst-plugin-pipewire: GStreamer support
(341/695) installing geocode-glib                           [################################] 100%
(342/695) installing geoclue                                [################################] 100%
Optional dependencies for geoclue
    libnotify: Demo Agent [installed]
(343/695) installing libxkbfile                             [################################] 100%
(344/695) installing gnome-desktop                          [################################] 100%
(345/695) installing gnome-keyring                          [################################] 100%
(346/695) installing xdg-desktop-portal-gtk                 [################################] 100%
Optional dependencies for xdg-desktop-portal-gtk
    evince: Print preview
(347/695) installing xdg-desktop-portal                     [################################] 100%
(348/695) installing flatpak                                [################################] 100%
(349/695) installing pamac-flatpak-plugin                   [################################] 100%

(flatpak remote-add:11260): GLib-GIO-ERROR **: 23:41:40.586: No GSettings schemas are installed on the system
/tmp/alpm_mrXQVq/.INSTALL: line 1: 11260 Trace/breakpoint trap   (core dumped) flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
error: command failed to execute correctly
(350/695) installing java-environment-common                [################################] 100%
(351/695) installing java-runtime-common                    [################################] 100%
For the complete set of Java binaries to be available in your PATH,
you need to re-login or source /etc/profile.d/jre.sh
Please note that this package does not support forcing JAVA_HOME as former package java-common did
(352/695) installing jre8-openjdk-headless                  [################################] 100%
Optional dependencies for jre8-openjdk-headless
    java-rhino: for some JavaScript support
(353/695) installing libxmu                                 [################################] 100%
(354/695) installing xorg-xset                              [################################] 100%
(355/695) installing xorg-xprop                             [################################] 100%
(356/695) installing xdg-utils                              [################################] 100%
Optional dependencies for xdg-utils
    kde-cli-tools: for KDE Plasma5 support in xdg-open
    exo: for Xfce support in xdg-open [pending]
    pcmanfm: for LXDE support in xdg-open
    perl-file-mimeinfo: for generic support in xdg-open [pending]
    perl-net-dbus: Perl extension to dbus used in xdg-screensaver
    perl-x11-protocol: Perl X11 protocol used in xdg-screensaver
(357/695) installing jre8-openjdk                           [################################] 100%
when you use a non-reparenting window manager,
set _JAVA_AWT_WM_NONREPARENTING=1 in /etc/profile.d/jre.sh
Optional dependencies for jre8-openjdk
    icedtea-web: web browser plugin + Java Web Start
    alsa-lib: for basic sound support [installed]
    gtk2: for the Gtk+ look and feel - desktop usage [pending]
    java8-openjfx: for JavaFX GUI components support
(358/695) installing jdk8-openjdk                           [################################] 100%
(359/695) installing poppler                                [################################] 100%
Optional dependencies for poppler
    poppler-data: highly recommended encoding data to display PDF documents with certain encodings
    and characters [pending]
(360/695) installing qpdf                                   [################################] 100%
(361/695) installing liblouis                               [################################] 100%
(362/695) installing ijs                                    [################################] 100%
(363/695) installing cups-filters                           [################################] 100%
Optional dependencies for cups-filters
    ghostscript: for non-PostScript printers to print with CUPS to convert PostScript to raster
    images [pending]
    foomatic-db: drivers use Ghostscript to convert PostScript to a printable form directly
    [pending]
    foomatic-db-engine: drivers use Ghostscript to convert PostScript to a printable form directly
    [pending]
    foomatic-db-nonfree: drivers use Ghostscript to convert PostScript to a printable form directly
    antiword: to convert MS Word documents
    docx2txt: to convert Microsoft OOXML text from DOCX files
(364/695) installing libpaper                               [################################] 100%
(365/695) installing cups                                   [################################] 100%
>> If you use an HTTPS connection to CUPS, the first time you access
>> the interface it may take a very long time before the site comes up.
>> This is because the first request triggers the generation of the CUPS
>> SSL certificates which can be a very time-consuming job.
Optional dependencies for cups
    ipp-usb: allows to send HTTP requests via a USB connection on devices without Ethernet or WiFi
    connections
    xdg-utils: xdg .desktop file support [installed]
    colord: for ICC color profile support [installed]
    logrotate: for logfile rotation support [installed]
(366/695) installing jbig2dec                               [################################] 100%
(367/695) installing libidn                                 [################################] 100%
(368/695) installing ghostscript                            [################################] 100%
Optional dependencies for ghostscript
    texlive-core: needed for dvipdf
    gtk3: needed for gsx [installed]
(369/695) installing cups-pdf                               [################################] 100%
-------------------------------------------------
To use cups-pdf, restart cups and visit the cups
web interface at http://localhost:631/

You can now add a "Virtual Printer (PDF Printer)"
and use the Postscript/Generic postscript color
printer driver.

Note that cups-pdf has a configuration
file in /etc/cups. The default location for
pdf output is /var/spool/cups-pdf/$username.
-------------------------------------------------
(370/695) installing cups-pk-helper                         [################################] 100%
(371/695) installing gsfonts                                [################################] 100%
(372/695) installing gutenprint                             [################################] 100%
>>please run /usr/bin/cups-genppdupdate
>>and restart cups daemon
Optional dependencies for gutenprint
    gimp: adds gutenprint plugin to gimp [pending]
    libusb: required for drivers that depend on gutenprint52usb backend [installed]
    foomatic-db-gutenprint-ppds: prebuilt ppd files [pending]
    perl: to run cups-genppdupdate [installed]
(373/695) installing python-dbus-common                     [################################] 100%
(374/695) installing python-dbus                            [################################] 100%
(375/695) installing python-distro                          [################################] 100%
(376/695) installing net-snmp                               [################################] 100%
Optional dependencies for net-snmp
    perl-term-readkey: for snmpcheck application
    perl-tk: for snmpcheck and tkmib applications
    python: for the python modules [installed]
(377/695) installing perl-capture-tiny                      [################################] 100%
(378/695) installing perl-ffi-checklib                      [################################] 100%
(379/695) installing perl-file-chdir                        [################################] 100%
(380/695) installing perl-file-which                        [################################] 100%
(381/695) installing perl-path-tiny                         [################################] 100%
(382/695) installing perl-alien-build                       [################################] 100%
(383/695) installing perl-alien-libxml2                     [################################] 100%
(384/695) installing perl-xml-sax-base                      [################################] 100%
(385/695) installing perl-xml-namespacesupport              [################################] 100%
(386/695) installing perl-xml-sax                           [################################] 100%
(387/695) installing perl-xml-libxml                        [################################] 100%
:: Installing SAX XML Parsers
perl: warning: Setting locale failed.
perl: warning: Please check that your locale settings:
	LANGUAGE = (unset),
	LC_ALL = (unset),
	LC_ADDRESS = "en_IN",
	LC_MONETARY = "en_IN",
	LC_PAPER = "en_IN",
	LC_MESSAGES = "C",
	LC_MEASUREMENT = "en_IN",
	LC_TIME = "en_IN",
	LANG = "C"
    are supported and installed on your system.
perl: warning: Falling back to the standard locale ("C").
perl: warning: Setting locale failed.
perl: warning: Please check that your locale settings:
	LANGUAGE = (unset),
	LC_ALL = (unset),
	LC_ADDRESS = "en_IN",
	LC_MONETARY = "en_IN",
	LC_PAPER = "en_IN",
	LC_MESSAGES = "C",
	LC_MEASUREMENT = "en_IN",
	LC_TIME = "en_IN",
	LANG = "C"
    are supported and installed on your system.
perl: warning: Falling back to the standard locale ("C").
(388/695) installing perl-dbi                               [################################] 100%
(389/695) installing foomatic-db-engine                     [################################] 100%
Optional dependencies for foomatic-db-engine
    foomatic-db: linuxprinting.org xml files database to create ppd driver files [pending]
(390/695) installing hplip                                  [################################] 100%
Optional dependencies for hplip
    cups: for printing support [installed]
    sane: for scanner support [pending]
    xsane: sane scanner frontend
    python-pillow: for commandline scanning support [pending]
    python-reportlab: for pdf output in hp-scan [pending]
    rpcbind: for network support [installed]
    python-pyqt5: for running GUI and hp-toolbox [pending]
    libusb: for advanced usb support [installed]
    wget: for network support [installed]
(391/695) installing foomatic-db                            [################################] 100%
Optional dependencies for foomatic-db
    foomatic-db-ppds: PostScript PPD files
(392/695) installing foomatic-db-gutenprint-ppds            [################################] 100%
Optional dependencies for foomatic-db-gutenprint-ppds
    cups: to use cups printer spooler(recommended) [installed]
(393/695) installing python-pyqt5-sip                       [################################] 100%
(394/695) installing xcb-util-keysyms                       [################################] 100%
(395/695) installing xcb-util-renderutil                    [################################] 100%
(396/695) installing xcb-util-wm                            [################################] 100%
(397/695) installing xcb-util                               [################################] 100%
(398/695) installing xcb-util-image                         [################################] 100%
(399/695) installing tslib                                  [################################] 100%
(400/695) installing mtdev                                  [################################] 100%
(401/695) installing libevdev                               [################################] 100%
(402/695) installing libwacom                               [################################] 100%
(403/695) installing libinput                               [################################] 100%
Optional dependencies for libinput
    gtk3: libinput debug-gui [installed]
    python-pyudev: libinput measure
    python-libevdev: libinput measure
(404/695) installing double-conversion                      [################################] 100%
(405/695) installing md4c                                   [################################] 100%
(406/695) installing qt5-base                               [################################] 100%
Optional dependencies for qt5-base
    qt5-svg: to use SVG icon themes [pending]
    qt5-wayland: to run Qt applications in a Wayland session
    qt5-translations: for some native UI translations
    postgresql-libs: PostgreSQL driver
    mariadb-libs: MariaDB driver
    unixodbc: ODBC driver
    libfbclient: Firebird/iBase driver
    freetds: MS SQL driver
    gtk3: GTK platform plugin [installed]
    perl: for fixqt4headers and syncqt [installed]
(407/695) installing python-pyqt5                           [################################] 100%
Optional dependencies for python-pyqt5
    python-opengl: enable OpenGL 3D graphics in PyQt applications
    python-dbus: for python-dbus mainloop support [installed]
    qt5-multimedia: QtMultimedia, QtMultimediaWidgets
    qt5-tools: QtHelp, QtDesigner
    qt5-svg: QtSvg [pending]
    qt5-webkit: QtWebKit, QtWebKitWidgets
    qt5-xmlpatterns: QtXmlPatterns
    qt5-declarative: QtQml, qmlplugin
    qt5-serialport: QtSerialPort
    qt5-websockets: QtWebSockets
    qt5-connectivity: QtNfc, QtBluetooth
    qt5-x11extras: QtX11Extras [pending]
    qt5-remoteobjects: QtRemoteObjects
    qt5-speech: QtTextToSpeech
    qt5-quick3d: QtQuick3D
(408/695) installing python-pysmbc                          [################################] 100%
(409/695) installing python-msgpack                         [################################] 100%
(410/695) installing python-cachecontrol                    [################################] 100%
Optional dependencies for python-cachecontrol
    python-lockfile: for the FileCache
(411/695) installing python-colorama                        [################################] 100%
(412/695) installing python-contextlib2                     [################################] 100%
(413/695) installing python-distlib                         [################################] 100%
(414/695) installing python-webencodings                    [################################] 100%
(415/695) installing python-html5lib                        [################################] 100%
Optional dependencies for python-html5lib
    python-lxml: lxml treebuilder
    python-genshi: genshi treewalker
(416/695) installing python-toml                            [################################] 100%
(417/695) installing python-pep517                          [################################] 100%
(418/695) installing python-progress                        [################################] 100%
(419/695) installing python-retrying                        [################################] 100%
(420/695) installing python-resolvelib                      [################################] 100%
(421/695) installing python-ply                             [################################] 100%
(422/695) installing python-pycparser                       [################################] 100%
(423/695) installing python-cffi                            [################################] 100%
(424/695) installing python-cryptography                    [################################] 100%
(425/695) installing python-pyopenssl                       [################################] 100%
(426/695) installing python-pip                             [################################] 100%
(427/695) installing libimagequant                          [################################] 100%
(428/695) installing python-pillow                          [################################] 100%
Optional dependencies for python-pillow
    freetype2: for the ImageFont module [installed]
    libraqm: for complex text scripts [installed]
    libwebp: for webp images [installed]
    tk: for the ImageTK module
    python-olefile: OLE2 file support
    python-pyqt5: for the ImageQt module [installed]
(429/695) installing python-reportlab                       [################################] 100%
(430/695) installing splix                                  [################################] 100%
Installation of the color profile for color printers:
-----------------------------------------------------

	Color printers need color profile files to get better results. These
files are provided by your printer manufacturer and you have to install them
manually. To do that, download the official linux drivers and locate the "cms"
directory. Install the contents to "/usr/share/cups/profiles/$MANUFACTURER".

	Samsung color profile files are available at:
		(Then use MANUFACTURER=samsung)
		http://splix.ap2c.org/samsung_cms.tar.bz2
(431/695) installing libieee1284                            [################################] 100%
Optional dependencies for libieee1284
    python: for python module [installed]
(432/695) installing poppler-glib                           [################################] 100%
(433/695) installing sane                                   [################################] 100%
(434/695) installing colord-sane                            [################################] 100%
(435/695) installing manjaro-printer                        [################################] 100%
Optional dependencies for manjaro-printer
    system-config-printer: A gtk cups printer configuration tool and status applet
    print-manager: A kde tool for managing print jobs and printers
    xsane: gtk2 frontend for scanner
    simple-scan: gtk3 frontend for scanner
    skanlite: Image Scanning Application for KDE
(436/695) installing libxklavier                            [################################] 100%
(437/695) installing lightdm                                [################################] 100%
Optional dependencies for lightdm
    accountsservice: Enhanced user accounts handling [pending]
    lightdm-gtk-greeter: GTK greeter [pending]
    xorg-server-xephyr: LightDM test mode
(438/695) installing lightdm-gtk-greeter                    [################################] 100%
(439/695) installing lightdm-gtk-greeter-settings           [################################] 100%
(440/695) installing accountsservice                        [################################] 100%
(441/695) installing python-cairo                           [################################] 100%
(442/695) installing blueman                                [################################] 100%
Optional dependencies for blueman
    dnsmasq: Network Access Point (NAP) support [installed]
    iproute2: Network Access Point (NAP) support [installed]
    networkmanager: Dial Up Networking (DUN) and Personal Area Networking (PAN) support [installed]
    pulseaudio-bluetooth: audio devices support [installed]
(443/695) installing ffmpegthumbnailer                      [################################] 100%
Optional dependencies for ffmpegthumbnailer
    gvfs: support for gio uris [installed]
(444/695) installing zensu                                  [################################] 100%
Optional dependencies for zensu
    spacefm: Used for displaying dialog window
    kdialog: Alternative for displaying dialog window
    yad: Alternative for displaying dialog window
    zenity: Alternative for displaying dialog window
(445/695) installing libgsf                                 [################################] 100%
(446/695) installing libopenraw                             [################################] 100%
(447/695) installing libxss                                 [################################] 100%
(448/695) installing light-locker                           [################################] 100%
Optional dependencies for light-locker
    upower: Power management support
(449/695) installing libnma                                 [################################] 100%
(450/695) installing nm-connection-editor                   [################################] 100%
(451/695) installing libdbusmenu-glib                       [################################] 100%
(452/695) installing libdbusmenu-gtk3                       [################################] 100%
(453/695) installing libindicator-gtk3                      [################################] 100%
(454/695) installing libappindicator-gtk3                   [################################] 100%
(455/695) installing network-manager-applet                 [################################] 100%
(456/695) installing gnome-menus                            [################################] 100%
(457/695) installing gtksourceview3                         [################################] 100%
(458/695) installing python-psutil                          [################################] 100%
(459/695) installing python-pyxdg                           [################################] 100%
(460/695) installing menulibre                              [################################] 100%
(461/695) installing libcanberra-pulse                      [################################] 100%
(462/695) installing libsigc++                              [################################] 100%
(463/695) installing glibmm                                 [################################] 100%
(464/695) installing cairomm                                [################################] 100%
(465/695) installing pangomm                                [################################] 100%
(466/695) installing atkmm                                  [################################] 100%
(467/695) installing gtkmm3                                 [################################] 100%
(468/695) installing pavucontrol                            [################################] 100%
Optional dependencies for pavucontrol
    pulseaudio: Audio backend [installed]
(469/695) installing gnome-themes-extra                     [################################] 100%
Optional dependencies for gnome-themes-extra
    gtk-engines: HighContrast GTK2 theme [pending]
(470/695) installing grub-theme-manjaro                     [################################] 100%
/usr/bin/grub-probe: error: failed to get canonical path of `overlay'.

==> Installation: Theme is added to your /etc/default/grub: GRUB_THEME="/usr/share/grub/themes/manjaro/theme.txt"
==> Default resolution "auto"
==> If an OS icon doesn't appear probably not present in /usr/share/grub/themes/manjaro/icons simply add it or merge in
==> https://gitlab.manjaro.org/artwork/branding/grub-theme

(471/695) installing gtk2                                   [################################] 100%
Optional dependencies for gtk2
    gnome-themes-standard: Default widget theme [installed]
    adwaita-icon-theme: Default icon theme [installed]
    python2: gtk-builder-convert
(472/695) installing gtk-engine-murrine                     [################################] 100%
(473/695) installing gtk-engines                            [################################] 100%
(474/695) installing matcha-gtk-theme                       [################################] 100%
(475/695) installing gtk-theme-breath                       [################################] 100%
(476/695) installing gnome-icon-theme-symbolic              [################################] 100%
(477/695) installing gnome-icon-theme                       [################################] 100%
(478/695) installing breath2-icon-themes                    [################################] 100%
(479/695) installing manjaro-icons                          [################################] 100%
(480/695) installing xcursor-simpleandsoft                  [################################] 100%
(481/695) installing xcursor-vanilla-dmz-aa                 [################################] 100%
(482/695) installing xcursor-maia                           [################################] 100%
(483/695) installing capitaine-cursors                      [################################] 100%
(484/695) installing python-ptyprocess                      [################################] 100%
(485/695) installing python-pexpect                         [################################] 100%
(486/695) installing libxfce4util                           [################################] 100%
(487/695) installing xfconf                                 [################################] 100%
(488/695) installing catfish                                [################################] 100%
One of the optional file search engines must be installed for catfish to work.
Optional dependencies for catfish
    mlocate: filename search [pending]
(489/695) installing dmidecode                              [################################] 100%
(490/695) installing unzip                                  [################################] 100%
(491/695) installing zip                                    [################################] 100%
(492/695) installing engrampa                               [################################] 100%
Optional dependencies for engrampa
    caja: Caja support
    p7zip: 7Z and ARJ archive support [pending]
    unace: ACE archive support [pending]
    unrar: RAR archive support [pending]
    brotli: Brotli compression support [installed]
(493/695) installing mailcap                                [################################] 100%
(494/695) installing firefox                                [################################] 100%
Optional dependencies for firefox
    networkmanager: Location detection via available WiFi networks [installed]
    libnotify: Notification integration [installed]
    pulseaudio: Audio support [installed]
    speech-dispatcher: Text-to-Speech
    hunspell-en_US: Spell checking, American English
(495/695) installing galculator-gtk2                        [################################] 100%
(496/695) installing gcolor2                                [################################] 100%
(497/695) installing babl                                   [################################] 100%
(498/695) installing libspiro                               [################################] 100%
(499/695) installing exiv2                                  [################################] 100%
(500/695) installing libgexiv2                              [################################] 100%
(501/695) installing jasper                                 [################################] 100%
Optional dependencies for jasper
    jasper-doc: documentation
    freeglut: jiv support [pending]
    glu: jiv support [pending]
(502/695) installing libraw                                 [################################] 100%
(503/695) installing luajit                                 [################################] 100%
(504/695) installing lensfun                                [################################] 100%
Optional dependencies for lensfun
    python: for lensfun-update-data and lensfun-add-adapter [installed]
(505/695) installing metis                                  [################################] 100%
(506/695) installing blas                                   [################################] 100%
(507/695) installing lapack                                 [################################] 100%
(508/695) installing tbb                                    [################################] 100%
(509/695) installing suitesparse                            [################################] 100%
(510/695) installing gegl                                   [################################] 100%
Optional dependencies for gegl
    ffmpeg: ffmpeg plugin [installed]
    graphviz: for gegl-introspect [pending]
(511/695) installing libmng                                 [################################] 100%
(512/695) installing libmypaint                             [################################] 100%
(513/695) installing libwmf                                 [################################] 100%
Optional dependencies for libwmf
    gdk-pixbuf2: for pixbuf loader [installed]
(514/695) installing mypaint-brushes1                       [################################] 100%
(515/695) installing poppler-data                           [################################] 100%
(516/695) installing libxaw                                 [################################] 100%
(517/695) installing netpbm                                 [################################] 100%
(518/695) installing gts                                    [################################] 100%
(519/695) installing graphviz                               [################################] 100%
Optional dependencies for graphviz
    mono: sharp bindings
    guile: guile bindings
    lua: lua bindings
    ocaml: ocaml bindings
    perl: perl bindings [installed]
    python: python bindings [installed]
    r: r bindings
    tcl: tcl bindings [installed]
    qt5-base: gvedit [installed]
    gtk2: gtk output plugin [installed]
    xterm: vimdot
(520/695) installing gimp                                   [################################] 100%
Optional dependencies for gimp
    gutenprint: for sophisticated printing only as gimp has built-in cups print support [installed]
    poppler-glib: for pdf support [installed]
    alsa-lib: for MIDI event controller module [installed]
    curl: for URI support [installed]
    ghostscript: for postscript support [installed]
(521/695) installing gparted                                [################################] 100%
Optional dependencies for gparted
    dosfstools: for FAT16 and FAT32 partitions [installed]
    jfsutils: for jfs partitions [installed]
    f2fs-tools: for Flash-Friendly File System [installed]
    btrfs-progs: for btrfs partitions [installed]
    exfatprogs: for exFAT partitions
    ntfs-3g: for ntfs partitions [installed]
    reiserfsprogs: for reiser partitions [installed]
    udftools: for UDF file system support
    xfsprogs: for xfs partitions [installed]
    nilfs-utils: for nilfs2 support
    polkit: to run gparted from application menu [installed]
    gpart: for recovering corrupt partition tables
    mtools: utilities to access MS-DOS disks
(522/695) installing ufw                                    [################################] 100%
(523/695) installing harfbuzz-icu                           [################################] 100%
(524/695) installing libwpe                                 [################################] 100%
(525/695) installing wpebackend-fdo                         [################################] 100%
(526/695) installing enchant                                [################################] 100%
Optional dependencies for enchant
    aspell: for aspell based spell checking support
    hunspell: for hunspell based spell checking support [pending]
    libvoikko: for libvoikko based spell checking support
    hspell: for hspell based spell checking support
    nuspell: for nuspell based spell checking support
(527/695) installing hyphen                                 [################################] 100%
(528/695) installing woff2                                  [################################] 100%
(529/695) installing webkit2gtk                             [################################] 100%
Optional dependencies for webkit2gtk
    geoclue: Geolocation support [installed]
    gst-plugins-good: media decoding [installed]
    gst-plugins-bad: media decoding [installed]
    gst-libav: nonfree media decoding [installed]
(530/695) installing gufw                                   [################################] 100%
(531/695) installing qt5-svg                                [################################] 100%
(532/695) installing libguess                               [################################] 100%
(533/695) installing libcue                                 [################################] 100%
(534/695) installing libsidplayfp                           [################################] 100%
Optional dependencies for libsidplayfp
    vice: better SID support
(535/695) installing confuse                                [################################] 100%
(536/695) installing libftdi                                [################################] 100%
Optional dependencies for libftdi
    python: library bindings [installed]
(537/695) installing libusb-compat                          [################################] 100%
(538/695) installing lirc                                   [################################] 100%
Optional dependencies for lirc
    python: for lirc-setup, irdb-get and pronto2lirc [installed]
(539/695) installing qt5-x11extras                          [################################] 100%
(540/695) installing audacious-plugins                      [################################] 100%
(541/695) installing audacious                              [################################] 100%
Optional dependencies for audacious
    unzip: zipped skins support [installed]
(542/695) installing htop                                   [################################] 100%
Optional dependencies for htop
    lm_sensors: show cpu temperatures [installed]
    lsof: show files opened by a process
    strace: attach to a running process
(543/695) installing libsynctex                             [################################] 100%
(544/695) installing poppler-qt5                            [################################] 100%
(545/695) installing qpdfview                               [################################] 100%
Optional dependencies for qpdfview
    libspectre: for PostScript support
    djvulibre: for DjVu support
(546/695) installing inxi                                   [################################] 100%
Optional dependencies for inxi
    bind-tools: dig: -i wlan IP
    bluez-tools: bt-adapter: -E bluetooth data (if no hciconfig)
    curl: -i (if no dig); -w,-W; -U [installed]
    dmidecode: -M if no sys machine data; -m [installed]
    file: -o unmounted file system (if no lsblk) [installed]
    hddtemp: -Dx show hdd temp, if no drivetemp module
    iproute2: ip: -i ip LAN [installed]
    kmod: modinfo: Ax; -Nx module version [installed]
    ipmitool: -s IPMI sensors (servers)
    lm_sensors: sensors: -s sensors output [installed]
    mesa-demos: glxinfo: -G glx info [pending]
    net-tools: ifconfig: -i ip LAN (deprecated)
    perl-io-socket-ssl: -U; -w,-W; -i (if dig not installed)
    perl-json-xs: --output json - required for export (legacy)
    smartmontools: smartctl: -Da advanced data
    systemd-sysvcompat: runlevel: -I fallback to Perl [installed]
    sudo: -Dx hddtemp-user; -o file-user [installed]
    tree: --debugger 20,21 /sys tree
    upower: -sx attached device battery info
    usbutils: lsusb: -A usb audio; -J (optional); -N usb networking [installed]
    wget: -i (if no dig); -w,-W; -U [installed]
    wmctrl: -S active window manager (fallback)
    xorg-xdpyinfo: xdpyinfo: -G multi screen resolution [pending]
    xorg-xprop: xprop: -S desktop data [installed]
    xorg-xrandr: xrandr: -G single screen resolution
(547/695) installing web-installer-url-handler              [################################] 100%
touch: cannot touch '/root/.config/mimeapps.list': No such file or directory
/usr/bin/xdg-mime: line 873: /root/.config/mimeapps.list.new: No such file or directory
Optional dependencies for web-installer-url-handler
    snap: snap support
    flatpak: flatpak support [installed]
(548/695) installing manjaro-hello                          [################################] 100%
Optional dependencies for manjaro-hello
    calamares: universal installer framework
    manjaro-application-utility: GUI for selecting common applications for installation or removal
    [pending]
    gnome-layout-switcher: convenient GUI to select Gnome layout and more
(549/695) installing manjaro-application-utility            [################################] 100%
Optional dependencies for manjaro-application-utility
    manjaro-hello [installed]
(550/695) installing kitemmodels                            [################################] 100%
Optional dependencies for kitemmodels
    python-pyqt5: for the Python bindings [installed]
    qt5-declarative: QML bindings
(551/695) installing kcoreaddons                            [################################] 100%
Optional dependencies for kcoreaddons
    python-pyqt5: for the Python bindings [installed]
(552/695) installing polkit-qt5                             [################################] 100%
(553/695) installing kauth                                  [################################] 100%
Optional dependencies for kauth
    python-pyqt5: for the Python bindings [installed]
(554/695) installing ckbcomp                                [################################] 100%
(555/695) installing manjaro-settings-manager               [################################] 100%
Optional dependencies for manjaro-settings-manager
    manjaro-settings-manager-notifier: qt-based [pending]
    manjaro-settings-manager-knotifier: knotifications-based
(556/695) installing manjaro-settings-manager-notifier      [################################] 100%
Optional dependencies for manjaro-settings-manager-notifier
    manjaro-settings-manager-notifier: qt-based [installed]
    manjaro-settings-manager-knotifier: knotifications-based
(557/695) installing mlocate                                [################################] 100%
(558/695) installing mousepad                               [################################] 100%
(559/695) installing intltool                               [################################] 100%
(560/695) installing python-distutils-extra                 [################################] 100%
(561/695) installing mugshot                                [################################] 100%
Optional dependencies for mugshot
    cheese: webcam support
(562/695) installing farstream                              [################################] 100%
Optional dependencies for farstream
    gst-plugins-good [installed]
    gst-plugins-bad [installed]
(563/695) installing protobuf-c                             [################################] 100%
(564/695) installing libgadu                                [################################] 100%
(565/695) installing libpurple                              [################################] 100%
Optional dependencies for libpurple
    avahi: Bonjour protocol support [installed]
    ca-certificates: SSL CA certificates [installed]
    python-dbus: for purple-remote and purple-url-handler [installed]
    tk: Tcl/Tk scripting support
(566/695) installing startup-notification                   [################################] 100%
(567/695) installing gtkspell                               [################################] 100%
(568/695) installing pidgin                                 [################################] 100%
Optional dependencies for pidgin
    aspell: for spelling correction
(569/695) installing powertop                               [################################] 100%
Optional dependencies for powertop
    xorg-xset: for the --calibrate function [installed]
(570/695) installing xorg-xdpyinfo                          [################################] 100%
(571/695) installing screenfetch                            [################################] 100%
Optional dependencies for screenfetch
    scrot: to take screenshot
(572/695) installing hunspell                               [################################] 100%
Optional dependencies for hunspell
    perl: for ispellaff2myspell [installed]
(573/695) installing botan                                  [################################] 100%
Optional dependencies for botan
    python: for using botan2.py [installed]
    boost-libs: for the botan executable [installed]
(574/695) installing thunderbird                            [################################] 100%
Optional dependencies for thunderbird
    libcanberra: sound support [installed]
(575/695) installing libdvbpsi                              [################################] 100%
(576/695) installing lua52                                  [################################] 100%
(577/695) installing libebml                                [################################] 100%
(578/695) installing libmatroska                            [################################] 100%
(579/695) installing libtar                                 [################################] 100%
(580/695) installing libupnp                                [################################] 100%
(581/695) installing glslang                                [################################] 100%
(582/695) installing spirv-tools                            [################################] 100%
(583/695) installing shaderc                                [################################] 100%
(584/695) installing libplacebo                             [################################] 100%
(585/695) installing vlc                                    [################################] 100%
Optional dependencies for vlc
    avahi: service discovery using bonjour protocol [installed]
    aom: AOM AV1 codec [installed]
    gst-plugins-base-libs: for libgst plugins [installed]
    dav1d: dav1d AV1 decoder [installed]
    libdvdcss: decoding encrypted DVDs [installed]
    libavc1394: devices using the 1394ta AV/C [installed]
    libdc1394: IEEE 1394 access plugin [installed]
    kwallet: kwallet keystore
    libva-vdpau-driver: vdpau backend nvidia [pending]
    libva-intel-driver: video backend intel [pending]
    libbluray: Blu-Ray video input [installed]
    flac: Free Lossless Audio Codec plugin [installed]
    twolame: TwoLAME mpeg2 encoder plugin [installed]
    libgme: Game Music Emu plugin [installed]
    vcdimager: navigate VCD with libvcdinfo
    libmtp: MTP devices discovery [installed]
    systemd-libs: udev services discovery [installed]
    smbclient: SMB access plugin [installed]
    libcdio: audio CD playback [installed]
    gnu-free-fonts: subtitle font
    ttf-dejavu: subtitle font [installed]
    libssh2: sftp access [installed]
    libnfs: NFS access [installed]
    mpg123: mpg123 codec [installed]
    protobuf: chromecast streaming [installed]
    libmicrodns: mDNS services discovery (chromecast etc) [installed]
    lua52-socket: http interface
    live-media: RTSP input
    libdvdread: DVD input module [installed]
    libdvdnav: DVD with navigation input module [installed]
    libogg: Ogg and OggSpots codec [installed]
    libshout: shoutcast/icecast output plugin [installed]
    libmodplug: MOD output plugin [installed]
    libvpx: VP8 and VP9 codec [installed]
    libvorbis: Vorbis decoder/encoder [installed]
    speex: Speex codec [installed]
    opus: opus codec [installed]
    libtheora: theora codec [installed]
    libpng: PNG support [installed]
    libjpeg-turbo: JPEG support [installed]
    librsvg: SVG plugin [installed]
    x264: H264 encoding [installed]
    x265: HEVC/H.265 encoder [installed]
    zvbi: VBI/Teletext/webcam/v4l2 capture/decoding [installed]
    libass: Subtitle support [installed]
    libkate: Kate codec [installed]
    libtiger: Tiger rendering for Kate streams
    sdl_image: SDL image support
    srt: SRT input/output plugin [installed]
    aalib: ASCII art video output [installed]
    libcaca: colored ASCII art video output [installed]
    libpulse: PulseAudio audio output [installed]
    alsa-lib: ALSA audio output [installed]
    jack: jack audio server [installed]
    libsamplerate: audio Resampler [installed]
    libsoxr: SoX audio Resampler [installed]
    chromaprint: Chromaprint audio fingerprinter [installed]
    lirc: lirc control [installed]
    libgoom2: Goom visualization
    projectm: ProjectM visualisation
    ncurses: ncurses interface [installed]
    libnotify: notification plugin [installed]
    gtk3: notification plugin [installed]
    aribb24: aribsub support
    aribb25: aribcam support
    pcsclite: aribcam support [installed]
(586/695) installing viewnior                               [################################] 100%
(587/695) installing libburn                                [################################] 100%
(588/695) installing libisofs                               [################################] 100%
(589/695) installing libgtop                                [################################] 100%
(590/695) installing libxfce4ui                             [################################] 100%
(591/695) installing exo                                    [################################] 100%
(592/695) installing xfburn                                 [################################] 100%
(593/695) installing yelp-xsl                               [################################] 100%
(594/695) installing yelp                                   [################################] 100%
(595/695) installing p7zip                                  [################################] 100%
(596/695) installing unace                                  [################################] 100%

==> unace post-install message:
==> to use this software, you have to accept the Public UnAce Licence,
==> ( it's in /usr/share/licenses/unace/license )
==> otherwise, remove this package!

(597/695) installing unrar                                  [################################] 100%
(598/695) installing manjaro-documentation-en               [################################] 100%
(599/695) installing xf86-input-elographics                 [################################] 100%
(600/695) installing xf86-input-evdev                       [################################] 100%
(601/695) installing xf86-input-libinput                    [################################] 100%
(602/695) installing xf86-input-void                        [################################] 100%
(603/695) installing lib32-glibc                            [################################] 100%
Optional dependencies for lib32-glibc
    perl: for mtrace [installed]
(604/695) installing lib32-libpciaccess                     [################################] 100%
(605/695) installing lib32-libdrm                           [################################] 100%
(606/695) installing lib32-libxdmcp                         [################################] 100%
(607/695) installing lib32-libxau                           [################################] 100%
(608/695) installing lib32-libxcb                           [################################] 100%
(609/695) installing lib32-libx11                           [################################] 100%
(610/695) installing lib32-libxext                          [################################] 100%
(611/695) installing lib32-libffi                           [################################] 100%
(612/695) installing lib32-expat                            [################################] 100%
(613/695) installing lib32-zlib                             [################################] 100%
(614/695) installing lib32-ncurses                          [################################] 100%
(615/695) installing lib32-readline                         [################################] 100%
(616/695) installing lib32-xz                               [################################] 100%
(617/695) installing lib32-gcc-libs                         [################################] 100%
(618/695) installing lib32-icu                              [################################] 100%
(619/695) installing lib32-libxml2                          [################################] 100%
(620/695) installing lib32-wayland                          [################################] 100%
(621/695) installing lib32-libxxf86vm                       [################################] 100%
(622/695) installing lib32-libxfixes                        [################################] 100%
(623/695) installing lib32-libxdamage                       [################################] 100%
(624/695) installing lib32-libxshmfence                     [################################] 100%
(625/695) installing lib32-libunistring                     [################################] 100%
(626/695) installing lib32-libidn2                          [################################] 100%
(627/695) installing lib32-openssl                          [################################] 100%
Optional dependencies for lib32-openssl
    ca-certificates [installed]
(628/695) installing lib32-libssh2                          [################################] 100%
(629/695) installing lib32-e2fsprogs                        [################################] 100%
(630/695) installing lib32-libxcrypt                        [################################] 100%
(631/695) installing lib32-libldap                          [################################] 100%
(632/695) installing lib32-keyutils                         [################################] 100%
(633/695) installing lib32-krb5                             [################################] 100%
(634/695) installing lib32-libpsl                           [################################] 100%
(635/695) installing lib32-zstd                             [################################] 100%
(636/695) installing lib32-curl                             [################################] 100%
(637/695) installing lib32-bzip2                            [################################] 100%
(638/695) installing lib32-libelf                           [################################] 100%
(639/695) installing lib32-libunwind                        [################################] 100%
(640/695) installing lib32-llvm-libs                        [################################] 100%
(641/695) installing lib32-lm_sensors                       [################################] 100%
(642/695) installing lib32-mesa                             [################################] 100%
Optional dependencies for lib32-mesa
    opengl-man-pages: for the OpenGL API man pages
    lib32-mesa-vdpau: for accelerated video playback
    lib32-libva-mesa-driver: for accelerated video playback [pending]
(643/695) installing lib32-libglvnd                         [################################] 100%
(644/695) installing lib32-libva                            [################################] 100%
Optional dependencies for lib32-libva
    lib32-libva-vdpau-driver: vdpau back-end for nvidia [pending]
    lib32-libva-intel-driver: back-end for intel cards [pending]
(645/695) installing lib32-libva-intel-driver               [################################] 100%
(646/695) installing lib32-libva-mesa-driver                [################################] 100%
(647/695) installing lib32-libvdpau                         [################################] 100%
(648/695) installing lib32-libva-vdpau-driver               [################################] 100%
(649/695) installing libva-intel-driver                     [################################] 100%
(650/695) installing libva-mesa-driver                      [################################] 100%
(651/695) installing libva-vdpau-driver                     [################################] 100%
(652/695) installing glu                                    [################################] 100%
(653/695) installing glew                                   [################################] 100%
(654/695) installing freeglut                               [################################] 100%
(655/695) installing mesa-demos                             [################################] 100%
(656/695) installing lib32-libice                           [################################] 100%
(657/695) installing lib32-util-linux                       [################################] 100%
(658/695) installing lib32-libsm                            [################################] 100%
(659/695) installing lib32-libxt                            [################################] 100%
(660/695) installing lib32-libxmu                           [################################] 100%
(661/695) installing lib32-libxi                            [################################] 100%
(662/695) installing lib32-glu                              [################################] 100%
(663/695) installing lib32-glew                             [################################] 100%
(664/695) installing lib32-mesa-demos                       [################################] 100%
(665/695) installing numlockx                               [################################] 100%
(666/695) installing xdg-user-dirs                          [################################] 100%
Created symlink /etc/systemd/user/default.target.wants/xdg-user-dirs-update.service → /usr/lib/systemd/user/xdg-user-dirs-update.service.
(667/695) installing xorg-fonts-encodings                   [################################] 100%
(668/695) installing libfontenc                             [################################] 100%
(669/695) installing libxfont2                              [################################] 100%
(670/695) installing xorg-xkbcomp                           [################################] 100%
(671/695) installing xorg-setxkbmap                         [################################] 100%
(672/695) installing xorg-server-common                     [################################] 100%
(673/695) installing xorg-server                            [################################] 100%
>>> xorg-server has now the ability to run without root rights with
    the help of systemd-logind. xserver will fail to run if not launched
    from the same virtual terminal as was used to log in.
    Without root rights, log files will be in ~/.local/share/xorg/ directory.

    Old behavior can be restored through Xorg.wrap config file.
    See Xorg.wrap man page (man xorg.wrap).
(674/695) installing xorg-twm                               [################################] 100%
(675/695) installing xorg-xauth                             [################################] 100%
(676/695) installing xorg-xrdb                              [################################] 100%
Optional dependencies for xorg-xrdb
    gcc: for preprocessing
    mcpp: a lightweight alternative for preprocessing
(677/695) installing xorg-xmodmap                           [################################] 100%
(678/695) installing xorg-xinit                             [################################] 100%
Optional dependencies for xorg-xinit
    xorg-twm [installed]
    xterm
(679/695) installing xorg-xkill                             [################################] 100%
(680/695) installing xorg-mkfontscale                       [################################] 100%
Creating X fontdir indices... done.
(681/695) installing perl-ipc-system-simple                 [################################] 100%
(682/695) installing perl-file-basedir                      [################################] 100%
(683/695) installing perl-file-desktopentry                 [################################] 100%
(684/695) installing perl-file-mimeinfo                     [################################] 100%
(685/695) installing libgee                                 [################################] 100%
(686/695) installing libgnomekbd                            [################################] 100%
(687/695) installing xapp                                   [################################] 100%
Optional dependencies for xapp
    python: for mate-desktop status applet [installed]
(688/695) installing xorg-xhost                             [################################] 100%
(689/695) installing timeshift                              [################################] 100%
==> Attempting to enable Cronie services...Running in chroot, ignoring command 'is-active'
(690/695) installing kernel-alive                           [################################] 100%
(691/695) installing gtkhash                                [################################] 100%
(692/695) installing thunar                                 [################################] 100%
Optional dependencies for thunar
    gvfs: trash support, mounting with udisk and remote filesystems [installed]
    tumbler: thumbnail previews
    thunar-volman: removable device management
    thunar-archive-plugin: archive creation and extraction
    thunar-media-tags-plugin: view/edit ID3/OGG tags
(693/695) installing gtkhash-thunar                         [################################] 100%
(694/695) installing manjaro-hotfixes                       [################################] 100%
(695/695) installing xiccd                                  [################################] 100%
:: Running post-transaction hooks...
( 1/25) Creating system user accounts...
Creating group adbusers with gid 975.
Creating group locate with gid 21.
Creating group ntp with gid 87.
Creating group avahi with gid 974.
Creating user avahi (Avahi mDNS/DNS-SD daemon) with uid 974 and gid 974.
Creating group colord with gid 973.
Creating user colord (Color management daemon) with uid 973 and gid 973.
Creating group cups with gid 209.
Creating user cups (cups helper user) with uid 209 and gid 209.
Creating group flatpak with gid 972.
Creating user flatpak (Flatpak system helper) with uid 972 and gid 972.
Creating group geoclue with gid 971.
Creating user geoclue (Geoinformation service) with uid 971 and gid 971.
Creating group git with gid 970.
Creating user git (git daemon user) with uid 970 and gid 970.
Creating group lightdm with gid 969.
Creating user lightdm (Light Display Manager) with uid 969 and gid 969.
Creating group nm-openconnect with gid 968.
Creating user nm-openconnect (NetworkManager OpenConnect) with uid 968 and gid 968.
Creating group nm-openvpn with gid 967.
Creating user nm-openvpn (NetworkManager OpenVPN) with uid 967 and gid 967.
Creating user ntp (Network Time Protocol) with uid 87 and gid 87.
Creating group openvpn with gid 966.
Creating user openvpn (OpenVPN) with uid 966 and gid 966.
Creating group polkitd with gid 102.
Creating user polkitd (PolicyKit daemon) with uid 102 and gid 102.
Creating group rtkit with gid 133.
Creating user rtkit (RealtimeKit) with uid 133 and gid 133.
Creating group saned with gid 965.
Creating user saned (SANE daemon user) with uid 965 and gid 965.
Creating group usbmux with gid 140.
Creating user usbmux (usbmux user) with uid 140 and gid 140.
( 2/25) Reloading system manager configuration...
  Skipped: Current root is not booted.
( 3/25) Updating udev hardware database...
( 4/25) Creating temporary files...
( 5/25) Reloading device manager configuration...
  Skipped: Device manager is not running.
( 6/25) Arming ConditionNeedsUpdate...
( 7/25) Updating linux initcpios...
==> Building image from preset: /etc/mkinitcpio.d/linux54.preset: 'default'
  -> -k /boot/vmlinuz-5.4-x86_64 -c /etc/mkinitcpio.conf -g /boot/initramfs-5.4-x86_64.img
==> Starting build: 5.4.105-1-MANJARO
  -> Running build hook: [base]
  -> Running build hook: [udev]
  -> Running build hook: [autodetect]
  -> Running build hook: [modconf]
  -> Running build hook: [block]
  -> Running build hook: [filesystems]
  -> Running build hook: [keyboard]
  -> Running build hook: [fsck]
==> Generating module dependencies
==> Creating gzip-compressed initcpio image: /boot/initramfs-5.4-x86_64.img
bsdtar: Failed to set default locale
bsdtar: Failed to set default locale
==> Image generation successful
==> Building image from preset: /etc/mkinitcpio.d/linux54.preset: 'fallback'
  -> -k /boot/vmlinuz-5.4-x86_64 -c /etc/mkinitcpio.conf -g /boot/initramfs-5.4-x86_64-fallback.img -S autodetect
==> Starting build: 5.4.105-1-MANJARO
  -> Running build hook: [base]
  -> Running build hook: [udev]
  -> Running build hook: [modconf]
  -> Running build hook: [block]
  -> Running build hook: [filesystems]
  -> Running build hook: [keyboard]
  -> Running build hook: [fsck]
==> Generating module dependencies
==> Creating gzip-compressed initcpio image: /boot/initramfs-5.4-x86_64-fallback.img
bsdtar: Failed to set default locale
bsdtar: Failed to set default locale
==> Image generation successful
( 8/25) Reloading system bus configuration...
  Skipped: Current root is not booted.
( 9/25) Warn about old perl modules
perl: warning: Setting locale failed.
perl: warning: Please check that your locale settings:
	LANGUAGE = (unset),
	LC_ALL = (unset),
	LC_ADDRESS = "en_IN",
	LC_MONETARY = "en_IN",
	LC_PAPER = "en_IN",
	LC_MESSAGES = "C",
	LC_MEASUREMENT = "en_IN",
	LC_TIME = "en_IN",
	LANG = "C"
    are supported and installed on your system.
perl: warning: Falling back to the standard locale ("C").
(10/25) Updating fontconfig cache...
(11/25) Probing GDK-Pixbuf loader modules...
(12/25) Updating GIO module cache...
(13/25) Compiling GSettings XML schema files...
(14/25) Probing GTK2 input method modules...
(15/25) Probing GTK3 input method modules...
(16/25) Updating icon theme caches...
(17/25) Reloading GVFS config...
(18/25) Install systemd service to restore linux kernel modules
Created symlink /etc/systemd/system/basic.target.wants/linux-module-cleanup.service → /etc/systemd/system/linux-module-cleanup.service.
(19/25) Hiding redundant menu entries
(20/25) Changing NetworkManager Connectivity-Ping to manjaro.org
(21/25) Updating the info directory file...
(22/25) Updating the desktop file MIME type cache...
(23/25) Updating the MIME type database...
(24/25) Updating the vlc plugin cache...
(25/25) Updating X fontdir indices...
  -> Copying [desktop-overlay] ...
 --> Restoring [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/desktopfs/etc/pacman.conf] ...
  -> No snaps found in profile. Skipping adding snaps
 --> overlayfs umount: [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/desktopfs]
  -> Cleaning [desktopfs]
==> Done [Desktop installation] (desktopfs)
 --> Loading Packages: [Packages-Live] ...
==> Prepare [Live installation] (livefs)
 --> overlayfs mount: [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/livefs]
 --> mirror: https://manjaro.moson.eu/stable/$repo/$arch
==> Creating install root at /var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/livefs
  -> Installing packages to /var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/livefs
:: Synchronizing package databases...
 core                           164.9 KiB  40.5 KiB/s 00:04 [################################] 100%
 extra                         2000.5 KiB   231 KiB/s 00:09 [################################] 100%
 community                        6.6 MiB   478 KiB/s 00:14 [################################] 100%
 multilib                       180.5 KiB   112 KiB/s 00:02 [################################] 100%
resolving dependencies...
looking for conflicting packages...

Packages (78) appstream-0.14.3-1  appstream-qt-0.14.3-1  attica-5.80.0-1  cracklib-2.9.7-2
              drbl-2.30.5-1  grub-theme-live-common-20.2-12  kactivities-5.80.0-1
              karchive-5.80.0-1  kbookmarks-5.80.0-1  kcodecs-5.80.0-1  kcompletion-5.80.0-1
              kconfig-5.80.0-1  kconfigwidgets-5.80.0-1  kcrash-5.80.0-1  kdbusaddons-5.80.0-1
              kdeclarative-5.80.0-1  kded-5.80.0-1  kglobalaccel-5.80.0-1  kguiaddons-5.80.0-1
              ki18n-5.80.0-1  kiconthemes-5.80.0-2  kio-5.80.1-1  kirigami2-5.80.0-1
              kitemviews-5.80.0-1  kjobwidgets-5.80.0-1  knotifications-5.80.0-1
              kpackage-5.80.0-1  kpmcore-20.12.3-1  kservice-5.80.0-1  ktextwidgets-5.80.0-1
              kwallet-5.80.0-1  kwayland-5.80.0-1  kwidgetsaddons-5.80.0-1  kwindowsystem-5.80.0-1
              kxmlgui-5.80.0-1  lbzip2-2.5-5  libdbusmenu-qt5-0.9.3+16.04.20160218-5
              libpwquality-1.4.4-3  lrzip-0.640-1  lzop-1.04-3  manjaro-live-base-20190901-1
              manjaro-tools-base-0.15.12-1  media-player-info-24-2  nilfs-utils-2.2.8-2
              partclone-0.3.17-1  partimage-0.6.9-13  pbzip2-1.1.13-3  pigz-2.6-1  pixz-1.0.7-2
              plasma-framework-5.80.0-1  progsreiserfs-0.3.0.5-10  qca-2.3.2-1
              qt5-declarative-5.15.2-1  qt5-graphicaleffects-5.15.2-1  qt5-multimedia-5.15.2-1
              qt5-quickcontrols-5.15.2-1  qt5-quickcontrols2-5.15.2-1  qt5-speech-5.15.2-1
              qt5-wayland-5.15.2-1  qt5-xmlpatterns-5.15.2-1  screen-4.8.0-3  smartmontools-7.2-1
              solid-5.80.0-1  sonnet-5.80.0-1  sshfs-3.7.1-1  upower-0.99.11-3
              xorg-xmessage-1.0.5-2  yaml-cpp-0.6.3-2  calamares-3.2.37-1  clonezilla-3.35.2-3
              grub-theme-live-manjaro-20.2-12  gsmartcontrol-1.1.3-3  linux54-zfs-2.0.4-2
              manjaro-live-skel-20180509-1  manjaro-live-systemd-20170621-1
              mkinitcpio-nfs-utils-0.3-7  nbd-3.21-1  zfs-utils-2.0.4-1

Total Installed Size:  235.86 MiB

:: Proceed with installation? [Y/n]
(78/78) checking keys in keyring                            [################################] 100%
(78/78) checking package integrity                          [################################] 100%
(78/78) loading package files                               [################################] 100%
(78/78) checking for file conflicts                         [################################] 100%
:: Processing package changes...
( 1/78) installing kconfig                                  [################################] 100%
Optional dependencies for kconfig
    python-pyqt5: for the Python bindings [installed]
( 2/78) installing kcodecs                                  [################################] 100%
Optional dependencies for kcodecs
    python-pyqt5: for the Python bindings [installed]
( 3/78) installing qt5-declarative                          [################################] 100%
( 4/78) installing qt5-wayland                              [################################] 100%
( 5/78) installing kguiaddons                               [################################] 100%
Optional dependencies for kguiaddons
    python-pyqt5: for the Python bindings [installed]
( 6/78) installing ki18n                                    [################################] 100%
Optional dependencies for ki18n
    python-pyqt5: for the Python bindings [installed]
    python: to compile .ts files [installed]
( 7/78) installing kwidgetsaddons                           [################################] 100%
Optional dependencies for kwidgetsaddons
    python-pyqt5: for the Python bindings [installed]
( 8/78) installing kconfigwidgets                           [################################] 100%
Optional dependencies for kconfigwidgets
    python-pyqt5: for the Python bindings [installed]
    perl: for preparetips5 [installed]
( 9/78) installing kitemviews                               [################################] 100%
Optional dependencies for kitemviews
    python-pyqt5: for the Python bindings [installed]
(10/78) installing karchive                                 [################################] 100%
(11/78) installing kiconthemes                              [################################] 100%
Optional dependencies for kiconthemes
    breeze-icons: fallback icon theme
(12/78) installing media-player-info                        [################################] 100%
(13/78) installing upower                                   [################################] 100%
(14/78) installing solid                                    [################################] 100%
Optional dependencies for solid
    qt5-declarative: QML bindings [installed]
(15/78) installing kjobwidgets                              [################################] 100%
Optional dependencies for kjobwidgets
    python-pyqt5: for the Python bindings [installed]
(16/78) installing kdbusaddons                              [################################] 100%
Optional dependencies for kdbusaddons
    python-pyqt5: for the Python bindings [installed]
(17/78) installing kwindowsystem                            [################################] 100%
(18/78) installing kcrash                                   [################################] 100%
Optional dependencies for kcrash
    drkonqi: KDE crash handler application
(19/78) installing kglobalaccel                             [################################] 100%
(20/78) installing attica                                   [################################] 100%
(21/78) installing kxmlgui                                  [################################] 100%
(22/78) installing kbookmarks                               [################################] 100%
(23/78) installing libdbusmenu-qt5                          [################################] 100%
(24/78) installing qt5-multimedia                           [################################] 100%
Optional dependencies for qt5-multimedia
    qt5-declarative: QML bindings [installed]
    gst-plugins-good: camera support, additional plugins [installed]
    gst-plugins-bad: camera support, additional plugins [installed]
    gst-plugins-ugly: additional plugins [installed]
    gst-libav: ffmpeg plugin [installed]
(25/78) installing qt5-speech                               [################################] 100%
Optional dependencies for qt5-speech
    flite: flite TTS backend
    speech-dispatcher: speech-dispatcher TTS backend
(26/78) installing knotifications                           [################################] 100%
(27/78) installing kservice                                 [################################] 100%
(28/78) installing kwallet                                  [################################] 100%
Optional dependencies for kwallet
    kwalletmanager: Configuration GUI
(29/78) installing kcompletion                              [################################] 100%
Optional dependencies for kcompletion
    python-pyqt5: for the Python bindings [installed]
(30/78) installing sonnet                                   [################################] 100%
Optional dependencies for sonnet
    hunspell: spell checking via hunspell [installed]
    aspell: spell checking via aspell
    hspell: spell checking for Hebrew
    libvoikko: Finnish support via Voikko
(31/78) installing ktextwidgets                             [################################] 100%
(32/78) installing kded                                     [################################] 100%
(33/78) installing kio                                      [################################] 100%
Optional dependencies for kio
    kio-extras: extra protocols support (sftp, fish and more)
    kdoctools: for the help kioslave
    kio-fuse: to mount remote filesystems via FUSE
(34/78) installing yaml-cpp                                 [################################] 100%
(35/78) installing smartmontools                            [################################] 100%
Optional dependencies for smartmontools
    s-nail: to get mail alerts to work [installed]
(36/78) installing qca                                      [################################] 100%
Optional dependencies for qca
    pkcs11-helper: PKCS-11 plugin [installed]
    botan: botan plugin [installed]
(37/78) installing kpmcore                                  [################################] 100%
Optional dependencies for kpmcore
    e2fsprogs: ext2/3/4 support [installed]
    xfsprogs: XFS support [installed]
    jfsutils: JFS support [installed]
    reiserfsprogs: Reiser support [installed]
    ntfs-3g: NTFS support [installed]
    dosfstools: FAT32 support [installed]
    fatresize: FAT resize support
    f2fs-tools: F2FS support [installed]
    exfat-utils: exFAT support [installed]
    nilfs-utils: nilfs support [pending]
    udftools: UDF support
(38/78) installing kactivities                              [################################] 100%
Optional dependencies for kactivities
    qt5-declarative: QML bindings [installed]
(39/78) installing kpackage                                 [################################] 100%
(40/78) installing kdeclarative                             [################################] 100%
(41/78) installing kwayland                                 [################################] 100%
(42/78) installing qt5-quickcontrols                        [################################] 100%
(43/78) installing qt5-quickcontrols2                       [################################] 100%
Optional dependencies for qt5-quickcontrols2
    qt5-graphicaleffects: for the Material style [pending]
(44/78) installing qt5-graphicaleffects                     [################################] 100%
(45/78) installing kirigami2                                [################################] 100%
(46/78) installing plasma-framework                         [################################] 100%
(47/78) installing qt5-xmlpatterns                          [################################] 100%
Optional dependencies for qt5-xmlpatterns
    qt5-declarative: QML bindings [installed]
(48/78) installing cracklib                                 [################################] 100%
(49/78) installing libpwquality                             [################################] 100%
Optional dependencies for libpwquality
    python: Python bindings [installed]
(50/78) installing appstream                                [################################] 100%
(51/78) installing appstream-qt                             [################################] 100%
(52/78) installing calamares                                [################################] 100%
(53/78) installing grub-theme-live-common                   [################################] 100%
(54/78) installing grub-theme-live-manjaro                  [################################] 100%
(55/78) installing xorg-xmessage                            [################################] 100%
(56/78) installing gsmartcontrol                            [################################] 100%
Optional dependencies for gsmartcontrol
    polkit: to run gsmartcontrol directly from menu [installed]
    xterm: to update the drive database
(57/78) installing manjaro-live-skel                        [################################] 100%
(58/78) installing manjaro-tools-base                       [################################] 100%
Optional dependencies for manjaro-tools-base
    manjaro-tools-pkg: Manjaro Linux package tools
    manjaro-tools-iso: Manjaro Linux iso tools
    manjaro-tools-yaml: Manjaro Linux yaml tools
(59/78) installing manjaro-live-base                        [################################] 100%
(60/78) installing manjaro-live-systemd                     [################################] 100%
(61/78) installing mkinitcpio-nfs-utils                     [################################] 100%
(62/78) installing nbd                                      [################################] 100%
(63/78) installing drbl                                     [################################] 100%
(64/78) installing progsreiserfs                            [################################] 100%
(65/78) installing nilfs-utils                              [################################] 100%
(66/78) installing partclone                                [################################] 100%
(67/78) installing partimage                                [################################] 100%
(68/78) installing pigz                                     [################################] 100%
(69/78) installing sshfs                                    [################################] 100%
(70/78) installing pbzip2                                   [################################] 100%
(71/78) installing lbzip2                                   [################################] 100%
(72/78) installing lrzip                                    [################################] 100%
(73/78) installing pixz                                     [################################] 100%
(74/78) installing lzop                                     [################################] 100%
(75/78) installing screen                                   [################################] 100%
(76/78) installing clonezilla                               [################################] 100%
(77/78) installing zfs-utils                                [################################] 100%
Optional dependencies for zfs-utils
    python: pyzfs and extra utilities, [installed]
    python-cffi: pyzfs [installed]
(78/78) installing linux54-zfs                              [################################] 100%
:: Running post-transaction hooks...
( 1/13) Creating system user accounts...
Creating group nbd with gid 964.
Creating user nbd (Network Block Device) with uid 964 and gid 964.
Creating group partimag with gid 110.
Creating user partimag (Partimage user) with uid 110 and gid 110.
( 2/13) Reloading system manager configuration...
  Skipped: Current root is not booted.
( 3/13) Updating udev hardware database...
( 4/13) Creating temporary files...
( 5/13) Reloading device manager configuration...
  Skipped: Device manager is not running.
( 6/13) Arming ConditionNeedsUpdate...
( 7/13) Updating module dependencies...
( 8/13) Updating linux initcpios...
==> Building image from preset: /etc/mkinitcpio.d/linux54.preset: 'default'
  -> -k /boot/vmlinuz-5.4-x86_64 -c /etc/mkinitcpio.conf -g /boot/initramfs-5.4-x86_64.img
==> Starting build: 5.4.105-1-MANJARO
  -> Running build hook: [base]
  -> Running build hook: [udev]
  -> Running build hook: [autodetect]
  -> Running build hook: [modconf]
  -> Running build hook: [block]
  -> Running build hook: [filesystems]
  -> Running build hook: [keyboard]
  -> Running build hook: [fsck]
==> Generating module dependencies
==> Creating gzip-compressed initcpio image: /boot/initramfs-5.4-x86_64.img
bsdtar: Failed to set default locale
bsdtar: Failed to set default locale
==> Image generation successful
==> Building image from preset: /etc/mkinitcpio.d/linux54.preset: 'fallback'
  -> -k /boot/vmlinuz-5.4-x86_64 -c /etc/mkinitcpio.conf -g /boot/initramfs-5.4-x86_64-fallback.img -S autodetect
==> Starting build: 5.4.105-1-MANJARO
  -> Running build hook: [base]
  -> Running build hook: [udev]
  -> Running build hook: [modconf]
  -> Running build hook: [block]
  -> Running build hook: [filesystems]
  -> Running build hook: [keyboard]
  -> Running build hook: [fsck]
==> Generating module dependencies
==> Creating gzip-compressed initcpio image: /boot/initramfs-5.4-x86_64-fallback.img
bsdtar: Failed to set default locale
bsdtar: Failed to set default locale
==> Image generation successful
( 9/13) Reloading system bus configuration...
  Skipped: Current root is not booted.
(10/13) Updating icon theme caches...
(11/13) Updating the info directory file...
(12/13) Updating the appstream cache...
AppStream cache update completed successfully.
(13/13) Updating the desktop file MIME type cache...
  -> Copying [live-overlay] ...
==> Configuring [livefs]
  -> Configuring logind ...
  -> Configuring journald ...
  -> Disable systemd-gpt-auto-generator
 --> Configuring services
  -> Setting manjaro-live ...
  -> Setting mhwd-live ...
  -> Setting pacman-init ...
  -> Setting mirrors-live ...
  -> Setting avahi-daemon ...
  -> Setting bluetooth ...
  -> Setting cronie ...
  -> Setting ModemManager ...
  -> Setting NetworkManager ...
  -> Setting cups ...
  -> Setting haveged ...
  -> Setting ufw ...
  -> Setting apparmor ...
  -> Setting snapd.apparmor ...
  -> Setting snapd ...
  -> Setting fstrim.timer ...
  -> Setting lightdm ...
  -> Enable apparmor kernel parameters
 --> Done configuring services
 --> Configuring [Calamares]
  -> Writing settings.conf ...
  -> Writing welcome.conf ...
  -> Writing locale.conf ...
  -> Writing users.conf ...
  -> Skipping enabling PackageChooser module.
  -> Writing unpackfs.conf ...
  -> Writing machineid.conf ...
  -> Writing initcpio.conf ...
  -> Writing displaymanager.conf ...
  -> Writing mhwdcfg.conf ...
  -> Writing services.conf ...
  -> Writing bootloader.conf ...
  -> Writing postcfg.conf ...
  -> Writing finished.conf ...
 --> Done configuring [Calamares]
  -> Writing live.conf
==> Done configuring [livefs]
  -> Configuring branding
==> Done [Distribution: Release 21.0 Codename Ornara]
  -> Configuring polkit user rules
 --> Restoring [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/livefs/etc/pacman.conf] ...
 --> overlayfs umount: [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/livefs]
  -> Cleaning [livefs]
==> Done [Live installation] (livefs)
 --> Loading Packages: [Packages-Mhwd] ...
==> Prepare [drivers repository] (mhwdfs)
 --> overlayfs mount: [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/mhwdfs]
 --> Restoring [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/mhwdfs/etc/pacman.conf] ...
Root      : /
Conf File : /etc/pacman.conf
DB Path   : /var/lib/pacman/
Cache Dirs: /var/cache/pacman/pkg/
Hook Dirs : /usr/share/libalpm/hooks/  /etc/pacman.d/hooks/
Lock File : /var/lib/pacman/db.lck
Log File  : /var/log/pacman.log
GPG Dir   : /etc/pacman.d/gnupg/
Targets   : linux54-bbswitch  linux54-broadcom-wl  linux54-zfs  zfs-utils  linux54-virtualbox-guest-modules  linux54-headers  zfs-dkms  libva-intel-driver  libva-mesa-driver  libva-vdpau-driver  lib32-libva-vdpau-driver  libxaw  libxpm  libxvmc  mesa-vdpau  lib32-mesa-vdpau  lib32-libxvmc  lib32-primus  linux54-nvidia-390xx  linux54-nvidia  nvidia-390xx-utils  nvidia-utils  lib32-nvidia-utils  lib32-nvidia-390xx-utils  opencl-mesa  open-vm-tools  primus  bumblebee  nvidia-prime  gtkmm3  spice-vdagent  virtualbox-guest-utils  vulkan-radeon  lib32-vulkan-radeon  vulkan-intel  lib32-vulkan-intel  xf86-input-vmmouse  xf86-video-amdgpu  xf86-video-ati  xf86-video-dummy  xf86-video-fbdev  xf86-video-intel  xf86-video-nouveau  xf86-video-openchrome  xf86-video-sisusb  xf86-video-vesa  xf86-video-vmware  xf86-video-voodoo
:: Synchronizing package databases...
 core                           164.9 KiB   341 KiB/s 00:00 [################################] 100%
 extra                         2000.5 KiB   387 KiB/s 00:05 [################################] 100%
 community                        6.6 MiB  1258 KiB/s 00:05 [################################] 100%
 multilib                       180.5 KiB  1504 KiB/s 00:00 [################################] 100%
resolving dependencies...
warning: dependency cycle detected:
warning: primus will be installed before its bumblebee dependency

Packages (69) binutils-2.36.1-2  clang-11.1.0-1  compiler-rt-11.1.0-1  dkms-2.8.4-1
              egl-wayland-1.1.6-1  eglexternalplatform-1.1-2  elfutils-0.183-3  gc-8.0.4-4
              gcc-10.2.0-6  gdk-pixbuf-xlib-2.40.2-1  guile-2.2.6-2  lib32-libxv-1.0.11-2
              libclc-11.1.0-1  libdnet-1.12-13  libmicrohttpd-0.9.72-1  libmpc-1.2.1-1
              libmspack-1:0.10.1alpha-3  make-4.3-3  patch-2.7.6-8  uriparser-0.9.4-1
              xorg-xrandr-1.5.1-2  bumblebee-3.2.1-22  gtkmm3-3.24.4-1
              lib32-libva-vdpau-driver-0.7.4-6  lib32-libxvmc-1.0.12-1  lib32-mesa-vdpau-20.3.4-3
              lib32-nvidia-390xx-utils-390.141-1  lib32-nvidia-utils-460.56-1
              lib32-primus-20151110-4  lib32-vulkan-intel-20.3.4-3  lib32-vulkan-radeon-20.3.4-3
              libva-intel-driver-2.4.1-1  libva-mesa-driver-20.3.4-3  libva-vdpau-driver-0.7.4-5
              libxaw-1.0.13-3  libxpm-3.5.13-2  libxvmc-1.0.12-3  linux54-bbswitch-0.8-113
              linux54-broadcom-wl-6.30.223.271-113  linux54-headers-5.4.105-1
              linux54-nvidia-460.56-6  linux54-nvidia-390xx-390.141-19
              linux54-virtualbox-guest-modules-6.1.18-15  linux54-zfs-2.0.4-2  mesa-vdpau-20.3.4-3
              nvidia-390xx-utils-390.141-2  nvidia-prime-1.0-4  nvidia-utils-460.56-2
              open-vm-tools-6:11.2.5-2  opencl-mesa-20.3.4-3  primus-20151110-9
              spice-vdagent-0.21.0-1  virtualbox-guest-utils-6.1.18-2  vulkan-intel-20.3.4-3
              vulkan-radeon-20.3.4-3  xf86-input-vmmouse-13.1.0-5  xf86-video-amdgpu-19.1.0-2
              xf86-video-ati-1:19.1.0-2  xf86-video-dummy-0.3.8-4  xf86-video-fbdev-0.5.0-2
              xf86-video-intel-1:2.99.917+916+g31486f40-1  xf86-video-nouveau-1.0.17-1
              xf86-video-openchrome-0.6.0-4  xf86-video-sisusb-0.9.7-3  xf86-video-vesa-2.5.0-1
              xf86-video-vmware-13.3.0-2  xf86-video-voodoo-1.2.5-11  zfs-dkms-2.0.4-1
              zfs-utils-2.0.4-1

Total Download Size:  325.80 MiB

:: Proceed with download? [Y/n]
error: Partition /var/cache/pacman/pkg too full: 88555 blocks needed, 0 blocks free
error: failed to commit transaction (not enough free disk space)
Errors occurred, no packages were upgraded.
  -> Copying mhwd package cache ...
building file list ...
rsync: [sender] link_stat "/var/cache/pacman/pkg/linux54-bbswitch-0.8-113-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/linux54-broadcom-wl-6.30.223.271-113-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/linux54-virtualbox-guest-modules-6.1.18-15-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/linux54-headers-5.4.105-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/libmicrohttpd-0.9.72-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/elfutils-0.183-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/binutils-2.36.1-2-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/libmpc-1.2.1-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/gcc-10.2.0-6-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/gc-8.0.4-4-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/guile-2.2.6-2-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/make-4.3-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/patch-2.7.6-8-x86_64.pkg.tar.xz" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/dkms-2.8.4-1-any.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/zfs-dkms-2.0.4-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/eglexternalplatform-1.1-2-any.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/egl-wayland-1.1.6-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/nvidia-390xx-utils-390.141-2-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/nvidia-utils-460.56-2-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/lib32-nvidia-utils-460.56-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/lib32-nvidia-390xx-utils-390.141-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/libxvmc-1.0.12-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/mesa-vdpau-20.3.4-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/lib32-mesa-vdpau-20.3.4-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/lib32-libxv-1.0.11-2-x86_64.pkg.tar.xz" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/lib32-libxvmc-1.0.12-1-x86_64.pkg.tar.xz" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/primus-20151110-9-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/bumblebee-3.2.1-22-x86_64.pkg.tar.xz" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/lib32-primus-20151110-4-x86_64.pkg.tar.xz" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/linux54-nvidia-390xx-390.141-19-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/linux54-nvidia-460.56-6-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/libclc-11.1.0-1-any.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/compiler-rt-11.1.0-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/clang-11.1.0-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/opencl-mesa-20.3.4-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/libdnet-1.12-13-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/libmspack-1:0.10.1alpha-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/uriparser-0.9.4-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/gdk-pixbuf-xlib-2.40.2-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/open-vm-tools-6:11.2.5-2-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/nvidia-prime-1.0-4-any.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/spice-vdagent-0.21.0-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-vmware-13.3.0-2-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/virtualbox-guest-utils-6.1.18-2-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/vulkan-radeon-20.3.4-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/lib32-vulkan-radeon-20.3.4-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/vulkan-intel-20.3.4-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/lib32-vulkan-intel-20.3.4-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-input-vmmouse-13.1.0-5-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-amdgpu-19.1.0-2-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-ati-1:19.1.0-2-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-dummy-0.3.8-4-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-fbdev-0.5.0-2-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-intel-1:2.99.917+916+g31486f40-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-nouveau-1.0.17-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-openchrome-0.6.0-4-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-sisusb-0.9.7-3-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-vesa-2.5.0-1-x86_64.pkg.tar.zst" failed: No such file or directory (2)
rsync: [sender] link_stat "/var/cache/pacman/pkg/xf86-video-voodoo-1.2.5-11-x86_64.pkg.tar.zst" failed: No such file or directory (2)
done
gtkmm3-3.24.4-1-x86_64.pkg.tar.zst
lib32-libva-vdpau-driver-0.7.4-6-x86_64.pkg.tar.xz
libva-intel-driver-2.4.1-1-x86_64.pkg.tar.zst
libva-mesa-driver-20.3.4-3-x86_64.pkg.tar.zst
libva-vdpau-driver-0.7.4-5-x86_64.pkg.tar.zst
libxaw-1.0.13-3-x86_64.pkg.tar.zst
libxpm-3.5.13-2-x86_64.pkg.tar.zst
linux54-zfs-2.0.4-2-x86_64.pkg.tar.zst
xorg-xrandr-1.5.1-2-x86_64.pkg.tar.zst
zfs-utils-2.0.4-1-x86_64.pkg.tar.zst

sent 15,792,023 bytes  received 206 bytes  31,584,458.00 bytes/sec
total size is 15,787,281  speedup is 1.00
rsync error: some files/attrs were not transferred (see previous errors) (code 23) at main.c(1330) [sender=v3.2.3]
==> ERROR: A failure occurred in make_image_mhwd().
    Aborting...
 --> overlayfs umount: [/var/lib/manjaro-tools/buildiso/LODE-alpha/x86_64/mhwdfs]
